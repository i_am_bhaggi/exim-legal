<?php

/**
 * :: Constant File ::
 * To hold all constants used throughout.
 *
 **/

return [

    'UPLOADS' => '/uploads/',
    'BANNER_UPLOAD' => '/uploads/banner/',
    'FORM_UPLOAD' => '/uploads/forms/',
    'TEAM_UPLOAD' => '/uploads/team/'
];
