<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    protected $table = "contact_us";
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'contact',
        'message',
        'status'
    ];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    // /**
    //  * Method is used to save/update resource.
    //  *
    //  * @param   array $input
    //  * @param   int $id
    //  * @return  \Response
    //  */
    // public function store($input, $id = null)
    // {
    //     if ($id) {
    //         // save role
    //         return $this->find($id)->update($input);
    //     } else {
    //         return $this->create($input)->id;
    //     }
    // }

    /**
     * Method is used to search detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getContacts($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        // default filter if no search
        $filter = 1;

        $fields = [
            'contact_us.id',
            'contact_us.name',
            'contact_us.email',
            'contact_us.contact',
            'contact_us.message',
            'contact_us.status',
            'contact_us.created_at',
        ];

        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('keyword', $search)) ? " AND (contact_us.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' OR contact_us.email LIKE '%" .
                addslashes(trim($search['keyword'])) . "%')" : "";
        }

        if (array_key_exists('total', $search)) {
            return $this->whereRaw($filter)->count();
        }

        return $this->whereRaw($filter)->orderBy('contact_us.id', 'DESC')
            ->skip($skip)->take($take)->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalContacts($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('keyword', $search)) ? " AND (contact_us.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' OR contact_us.email LIKE '%" .
                addslashes(trim($search['keyword'])) . "%')" : "";
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->first();
    }
}
