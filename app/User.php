<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'name',
        'email',
        'mobile',
        'username',
        'password',
        'forgot_otp',
        'status',
        'last_seen',
        'last_login',
        'login_attempts',
        'created_by',
        'updated_by',
        'deleted_by',
        'id_type',
        'id_number'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at', 'last_login'];

    /**
     * Scope a query to only include active users.
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to validate the authentication.
     * @param array $inputs
     * @return \Illuminate\Contracts\Validation\Factory|\Illuminate\Contracts\Validation\Validator
     */
    public function validateAuthentication($inputs = [])
    {
        $rules = [
            'email'     => 'required',
            'password'  => 'required',
        ];
        return validator($inputs, $rules);
    }

    /**
     * @param $inputs
     * @return \Illuminate\Validation\Validator
     */
    public function validateResetPassword($inputs)
    {
        $rules['email'] = 'required|email';
        return validator($inputs, $rules);
    }

    /**
     * @param $inputs
     * @param bool $password
     * @return \Illuminate\Validation\Validator
     */
    public function validatePassword($inputs, $password = true)
    {
        if ($password) {
            $rules['password']          = 'required';
        }
        $rules['new_password']      = 'required|same:confirm_password';
        $rules['confirm_password']  = 'required';
        return validator($inputs, $rules);
    }

    /**
     * @param $password
     * @return mixed
     */
    public function updatePassword($password)
    {
        return $this->where('id', authUserId())->update(['password' => $password]);
    }

    /**
     * @param $token
     * @return mixed
     */
    public function getHashCustomer($token)
    {
        return $this->where(\DB::raw('md5(customer_id)'), $token)->first();
    }

    /**
     * @param $token
     * @param $password
     * @return mixed
     */
    public function updateHashPassword($token, $password)
    {
        return $this->where(\DB::raw('md5(customer_id)'), $token)
            ->update(['password' => $password, 'forgot_otp' => ""]);
    }

    /**
     * @return mixed
     */
    public function updateLastLogin()
    {
        $loginTiming = [
            'last_login'        => new \DateTime,
            'login_attempts'    => 0,
        ];
        return $this->find(authUserId())->update($loginTiming);
    }

    /**
     * @param string $username
     *
     * @return mixed
     */
    public function updateFailedAttempts($username)
    {
        $user = $this->where('id', '!=', 1)
            ->where(function ($query) use ($username) {
                $query->where('username', $username)
                    ->orWhere('email', $username);
            })
            ->first();

        if ($user) {
            $user->increment('login_attempts', 1);
        }
    }

    /**
     * @param array $inputs
     *
     * @param null $id
     * @return \Illuminate\Validation\Validator
     */
    public function validateUser($inputs, $id = null)
    {
        $rules = [
            'name'          => 'required',
            'role'          => 'required',
            'state'          => 'required',
            'status'        => 'in:0,1'
        ];

        if ($id) {
            $rules['username'] = 'required|unique:users,username,' . $id . ',id,deleted_at,NULL';
            $rules['email'] = 'required|email|unique:users,email,' . $id . ',id,deleted_at,NULL';
            $rules['mobile'] = 'required|unique:users,username,' . $id . ',id,deleted_at,NULL';
            $rules['password'] = 'min:5';
        } else {
            $rules['username'] = 'required|unique:users,username,NULL,id,deleted_at,NULL';
            $rules['email'] = 'required|email|unique:users,email,NULL,id,deleted_at,NULL';
            $rules['mobile'] = 'required|unique:users,username,NULL,id,deleted_at,NULL';
            $rules['password'] = 'required|min:5';
        }
        return validator($inputs, $rules);
    }

    /**
     * @param array $inputs
     * @param int $id
     *
     * @return mixed
     */
    public function store($inputs, $id = null)
    {
        if ($id) {
            $this->find($id)->update($inputs);
            return $id;
        } else {
            return $this->create($inputs)->id;
        }
    }

    /**
     * Method is used to search results.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getUsers($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        $filter = 1; // default filter if no search

        $fields = [
            'users.id',
            'users.username',
            'users.name',
            'users.email',
            'users.mobile',
            'role.name as role',
            'users.status',
        ];

        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('keyword', $search) && $search['keyword'] != "") ? " AND username LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
        }

        return $this->leftJoin('role', 'role.id', '=', 'users.role_id')
            ->whereRaw($filter)
            ->orderBy('users.id', 'ASC')
            ->skip($skip)->take($take)
            ->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalUser($search = null)
    {
        $filter = 1; // default filter if no search

        // when search
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('keyword', $search)  && $search['keyword'] != "") ? " AND username LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->get()->first();
    }


    /**
     * @param array $search
     * @return mixed
     */
    public function getUsersService($search = [])
    {
        $filter = 1;
        // when search
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('u_type', $search)  && $search['u_type'] != 1) ? " AND role_id IN (2,3)" : "";
        }

        $result = $this->active()
            ->where('id', '>', 1)
            ->whereRaw($filter)
            ->lists('name', 'id')->toArray();
        return ['' => '-Select Users-'] + $result;
    }
}
