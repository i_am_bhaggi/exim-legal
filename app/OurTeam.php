<?php

namespace App;

/**
 * :: OurTeam Model ::
 * To manage OurTeam CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OurTeam extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'our_teams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'image-pic',
        'designation',
        'professional',
        'area_of_practice',
        'description',
        'status',
        '_order',
        'frontpage',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to validate
     *
     * @param int $id
     * @return \Response
     **/
    public function validateOurTeam($inputs, $id = null)
    {
        // validation rule
        if ($id) {
            $rules['name'] = 'required';
            $rules['designation'] = 'required';
            // $rules['image-pic'] = 'required|mimes:jpeg,bmp,png';
        } else {
            $rules['name'] = 'required';
            $rules['designation'] = 'required';
            $rules['image-pic'] = 'required|mimes:jpeg,bmp,png';
        }
        //$rules['image'] = 'mimes:jpeg,bmp,png';
        return validator($inputs, $rules);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  \Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            // save role
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }
    }

    /**
     * Method is used to search detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getOurTeam($search = null, $skip = 0, $perPage = 0)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        // default filter if no search
        $filter = 1;

        $fields = [
            'our_teams.id',
            'our_teams.name',
            'our_teams.designation',
            'our_teams.image-pic',
            'our_teams.professional',
            'our_teams.area_of_practice',
            'our_teams.description',
            'our_teams.status',
            'our_teams._order',
            'our_teams.frontpage',
            'our_teams.created_by',
            'our_teams.updated_by',
            'our_teams.created_at',
            'our_teams.updated_at',
            'our_teams.deleted_at',
        ];

        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search)) ? " AND our_teams.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $f1;
        }

        return $this->whereRaw($filter)
            ->orderBy('our_teams._order', 'DESC')
            ->orderBy('our_teams.id', 'DESC')
            ->skip($skip)->take($take)->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalOurTeam($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search)) ? " AND name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $f1;
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->first();
    }

    // /**
    //  * @param bool $parent
    //  * @return mixed
    //  */
    // public function getOurTeam($parent = false)
    // {
    //     $data = $this->active()->orderBy('name', 'ASC');
    //     $data = $data->lists('name', 'id')->toArray();
    //     $result = [];
    //     foreach ($data as $id => $name) {
    //         $result[$id] = $name;
    //     }
    //     return ['' => '-Select Doctor-'] + $result;
    // }

    /**
     * @param $id
     *
     * @param array $search
     * @return array
     */
    public function getFilterOurTeam($id = null, $search = [])
    {
        $filter = 1;
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('frontpage', $search)) ? " AND our_teams.frontpage = " .
                addslashes(trim($search['frontpage'])) : "";
        }
        $result = $this->active()->whereRaw($filter)
            ->orderBy('our_teams._order', 'DESC')
            ->orderBy('our_teams.id', 'DESC');
        return $result->get(['id', 'name', 'image', 'short_intro', 'url_slug']);
    }
}
