<?php

namespace App;

/**
 * :: Testimonial Model ::
 * To manage Testimonial CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Testimonial extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'testimonial_master';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'image',
        'url_slug',
        '_order',
        'short_intro',
        'full_detail',
        'keywords',
        'description',
        'status',
        'frontpage',
        'show_menu',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to validate
     *
     * @param int $id
     * @return \Response
     **/
    public function validateTestimonial($inputs, $id = null)
    {
        // validation rule
        if ($id) {
            $rules['name'] = 'required';
        } else {
            $rules['name'] = 'required';
        }
        //$rules['image'] = 'mimes:jpeg,bmp,png';
        return validator($inputs, $rules);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  \Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            // save role
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }
    }

    /**
     * Method is used to search detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getTestimonial($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        // default filter if no search
        $filter = 1;

        $fields = [
            'testimonial_master.id',
            'testimonial_master.name',
            'testimonial_master.short_intro',
            'testimonial_master.image',
            'testimonial_master.status',
            'testimonial_master.frontpage',
        ];

        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search)) ? " AND testimonial_master.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $f1;
        }

        return $this->whereRaw($filter)
            ->orderBy('testimonial_master._order', 'DESC')
            ->orderBy('testimonial_master.id', 'DESC')
            ->skip($skip)->take($take)->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalTestimonial($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search)) ? " AND name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $f1;
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->first();
    }

    /**
     * @param bool $parent
     * @return mixed
     */
    public function getTestimonialService($parent = false)
    {
        $data = $this->active()->orderBy('name', 'ASC');
        $data = $data->lists('name', 'id')->toArray();
        $result = [];
        foreach ($data as $id => $name) {
            $result[$id] = $name;
        }
        return ['' => '-Select Doctor-'] + $result;
    }

    /**
     * @param $id
     *
     * @param array $search
     * @return array
     */
    public function getFilterTestimonial($id = null, $search = [])
    {
        $filter = 1;
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('frontpage', $search)) ? " AND testimonial_master.frontpage = " .
                addslashes(trim($search['frontpage'])) : "";
        }
        $result = $this->active()->whereRaw($filter)
            ->orderBy('testimonial_master._order', 'DESC')
            ->orderBy('testimonial_master.id', 'DESC');
        return $result->get(['id', 'name', 'image', 'short_intro', 'url_slug']);
    }
}
