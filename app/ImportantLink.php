<?php

namespace App;

/**
 * :: ImportantLinks Model ::
 * To manage ImportantLinks CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ImportantLink extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'important_links';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        '_order',
        'link',
        'status',
        'frontpage',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to validate
     *
     * @param int $id
     * @return \Response
     **/
    public function validateImportantLinks($inputs, $id = null)
    {
        // validation rule
        if ($id) {
            $rules['title'] = 'required';
            $rules['link'] = 'required';
        } else {
            $rules['title'] = 'required';
            $rules['link'] = 'required';
        }
        //$rules['image'] = 'mimes:jpeg,bmp,png';
        return validator($inputs, $rules);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  \Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            // save role
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }
    }

    /**
     * Method is used to search detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getImportantLinks($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        // default filter if no search
        $filter = 1;

        $fields = [
            'important_links.id',
            'important_links.title',
            'important_links.link',
            'important_links.status',
            'important_links.frontpage',
        ];

        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search)) ? " AND important_links.title LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $f1;
        }

        return $this->whereRaw($filter)
            ->orderBy('important_links._order', 'DESC')
            ->orderBy('important_links.id', 'DESC')
            ->skip($skip)->take($take)->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalImportantLinks($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search)) ? " AND title LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $f1;
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->first();
    }

    // /**
    //  * @param bool $parent
    //  * @return mixed
    //  */
    // public function getImportantLinks($parent = false)
    // {
    //     $data = $this->active()->orderBy('title', 'ASC');
    //     $data = $data->lists('title', 'id')->toArray();
    //     $result = [];
    //     foreach ($data as $id => $title) {
    //         $result[$id] = $title;
    //     }
    //     return ['' => '-Select Doctor-'] + $result;
    // }

    /**
     * @param $id
     *
     * @param array $search
     * @return array
     */
    public function getFilterImportantLinks($id = null, $search = [])
    {
        $filter = 1;
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('frontpage', $search)) ? " AND important_links.frontpage = " .
                addslashes(trim($search['frontpage'])) : "";
        }
        $result = $this->active()->whereRaw($filter)
            ->orderBy('important_links._order', 'DESC')
            ->orderBy('important_links.id', 'DESC');
        return $result->get(['id', 'title', 'image', 'short_intro', 'url_slug']);
    }
}
