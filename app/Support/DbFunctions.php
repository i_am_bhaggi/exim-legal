<?php

use App\Customer;
use App\Cart;
use App\CartProducts;
use App\OurTeam;
use App\Pages;
use App\Services;
use App\SettingMaster;
use App\Testimonial;

/**
 * :: Db Functions File ::
 * USed for manage all kind database related helper functions.
 *
 **/

/**
 * @return null
 */
function loggedInCompanyId()
{
	return 1;
}

/**
 * @param array $filter
 * @param int $take
 * @param int $skip
 * @return array
 */
function getServices($filter = [], $take = 0, $skip = 0)
{
	return (new Services)->getFilterServices($filter, $take, $skip);
}

function getOurTeams($filter = [], $take = 0, $skip = 0)
{
	return (new OurTeam)->getOurTeam($filter, $take, $skip);
}

/**
 * @param $id
 * @return null
 */
function getPage($id)
{
	return (new Pages)->getPageDetail(['id' => $id]);
}

/**
 * @return mixed
 */
function webSetting()
{
	return (new SettingMaster)->getSetting();
}

function getTestimonials($all = 1)
{
	// all -> 1: yes, 0: only frontend page
	if ($all) {
		$data = Testimonial::where('status', 1)->get();
	} else {
		$data = Testimonial::where(['status' => 1, 'frontpage' => 1])->get();
	}
	return $data;
}
