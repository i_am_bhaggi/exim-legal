<?php

use App\ImportantLink;
use App\WebsiteUser;
use Illuminate\Http\Request;

function p($data, $exit = 1)
{
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($exit == 1)
        die;
}
/**
 * @param $e
 * @return string
 */
function exception($e)
{
    return $e->getMessage() . $e->getFile() . $e->getLine();
}

/**
 * Method is used to get current route name.
 *
 * @return mixed
 */
function currentRoute()
{
    return \Route::currentRouteName();
}

/**
 * @return bool
 */
function isSystemAdmin()
{
    if (!authUser()) {
        return false;
    }
    return (\Auth::user()->id == 1) ? true : false;
}

/**
 * @return bool
 */
function isAdmin()
{
    if (!authUser()) {
        return false;
    }
    return (\Auth::user()->role_id == 1) ? true : false;
}

/**
 * @return bool
 */
function roleId()
{
    if (!authUser()) {
        return false;
    }
    return \Auth::user()->role_id;
}

/**
 * Method is used to get current logged-in user id.
 * @return null
 */
function authUserId()
{
    $id = null;
    if (\Auth::check()) {
        $id = \Auth::user()->id;
    }
    return $id;
}

/**
 * method is used to get current logged-in user object.
 * @return null
 */
function authUser()
{
    $user = null;
    if (\Auth::check()) {
        $user = \Auth::user();
    }
    return $user;
}

/**
 * Conversion to utc time based on country specific time zone.
 *
 * @param string $localDate
 * @param string $timezone
 * @param string $format
 *
 * @return bool|string
 */
function convertToUtc($localDate = null, $format = null, $timezone = 'Asia/Kolkata')
{
    $format = ($format == "") ? 'Y-m-d H:i:s' : $format;
    $localDate = ($localDate == "") ? date('Y-m-d H:i:s') : $localDate;
    $date = new \DateTime($localDate, new DateTimeZone($timezone));
    $date->setTimezone(new DateTimeZone('UTC'));
    return $date->format($format);
}

/**
 * conversion utc time to country specific time zone depending upon which country user is belong to
 *
 * @param $utcDate
 * @param string $timezone
 * @param string $format
 * @return bool|string
 */
function convertToLocal($utcDate, $format = null, $timezone = 'Asia/Kolkata')
{
    $dateFormat = ($format != "") ? $format : 'Y-m-d H:i:s';
    $date = new \DateTime($utcDate, new DateTimeZone('UTC'));
    $date->setTimezone(new DateTimeZone($timezone));
    return $date->format($dateFormat);
}

/**
 * Method is used to convert date to
 * specified format
 *
 * @param string $date
 * @param string $format
 *
 * @return bool|null|string
 */
function dateFormat($date, $format = 'Y-m-d')
{
    if (trim($date) != '') {
        if (trim($date) == '0000-00-00' || trim($date) == '0000-00-00 00:00') {
            return null;
        } else {
            return date($format, strtotime($date));
        }
    }
    return $date;
}

/**
 * @param bool $withTime
 * @return bool|string
 */
function currentDate($withTime = false)
{
    $date = date('Y-m-d H:i:s');
    if (!$withTime) {
        $date = date('Y-m-d');
    }
    return $date;
}

/**
 * Method is used to maintain multi language working.
 *
 * @param null $path
 * @param null $string
 * @return null
 */
function lang($path = null, $string = null)
{
    $lang = $path;
    if (trim($path) != '' && trim($string) == '') {
        $lang = \Lang::get($path);
    } elseif (trim($path) != '' && trim($string) != '') {
        $lang = \Lang::get($path, ['attribute' => $string]);
    }
    return $lang;
}



/**
 * Method is used to return string in lower, upper or ucfirst.
 *
 * @param string $string
 * @param string $type L -> lower, U -> upper, UC -> upper character first
 * @return Response
 */
function string_manip($string = null, $type = 'L')
{
    switch ($type) {
        case 'U':
            return strtoupper($string);
            break;
        case 'UC':
            return ucfirst(strtolower($string));
            break;
        case 'UCW':
            return ucwords(strtolower($string));
            break;
        default:
            return strtolower($string);
            break;
    }
}

/**
 * @param bool $status
 * @param int $statusCode
 * @param string $message
 * @param array $result
 *
 * @return \Illuminate\Http\JsonResponse
 */
function apiResponse($status, $statusCode, $message, $errors = [], $result = [])
{
    $response = ['success' => $status, 'status' => $statusCode];

    if ($message != "") {
        $response['message'] = $message;
    }

    if (count($errors) > 0) {
        $response['errors'] = $errors;
    }

    if (count($result) > 0) {
        $response['result'] = $result;
    }
    return response()->json($response, $statusCode);
}

/**
 * @param bool $status
 * @param int $statusCode
 * @param string $message
 * @param string $url
 * @param array $errors
 * @param array $data
 * @return \Illuminate\Http\JsonResponse
 */
function validationResponse($status, $statusCode, $message = null, $url = null, $errors = [], $data = [])
{
    $response = ['success' => $status, 'status' => $statusCode];

    if ($message != "") {
        $response['message'] = $message;
    }

    if ($url != "") {
        $response['url'] = $url;
    }

    if (count($errors) > 0) {
        $response['errors'] = errorMessages($errors);
    }

    if (count($data) > 0) {
        $response['data'] = $data;
    }
    return response()->json($response, $statusCode);
}

/**
 * @param array $errors
 * @return array
 */
function errorMessages($errors = [])
{
    $error = [];
    foreach ($errors->toArray() as $key => $value) {
        $error[$key] = $value[0];
    }
    return $error;
}

/**
 * @param $file
 * @param $id
 * @param null $oldImage
 * @param null $path
 * @return string
 */
function fileUploader($file, $id, $oldImage = null, $path = null)
{
    $folder = ($path != "") ? $path : ROOT . \Config::get('constants.UPLOADS');
    $fileName = $_FILES[$file]['name'];
    $newFile = str_replace(' ', '_', str_random(6) . " " . $fileName);
    $fileTempName = $_FILES[$file]['tmp_name'];

    if ($oldImage != "" && file_exists($folder . $oldImage)) {
        unlink($folder . $oldImage);
    }

    if (move_uploaded_file($fileTempName, $folder . $id  . '__' . string_manip($newFile))) {
        return $id  . '__' . string_manip($newFile);
    }
}

/**
 * @param $image
 * @param int $width
 * @param bool $url
 * @return string
 */
function renderImage($image, $width = 150, $url = false)
{
    $folder = ROOT . \Config::get('constants.UPLOADS');
    if (file_exists($folder . $image) && $image != "") {
        $file = asset(\Config::get('constants.UPLOADS')) . '/' . $image;
        if ($url) {
            return $file;
        } else {
            return \HTML::image($file, 'No Image', ['width' => $width, 'class' => 'img-circle']);
        }
    } else {
        $file = asset('assets/images/noimage.png');
        if ($url) {
            return $file;
        } else {
            return \HTML::image(asset('assets/images/noimage.png'), 'No dd Image', ['width' => $width, 'class' => 'img-circle']);
        }
    }
}

/**
 * undocumented function
 *
 * @param    string $loc
 * @param    int $id
 * @param    string $date
 * @return \Response
 */
function dropZoneUploader($id, $date = null)
{
    $path = \Config::get('constants.UPLOADS');
    $folderPath = ROOT . $path;
    $fileParamName = 'file'; //dropzone uploading file name

    $rand = str_random(5);
    $folder =  $folderPath . '/';
    $prefix =     $id . '__' . $rand . '.';

    if (!is_dir($folder)) {
        mkdir($folder, 0775);
    }

    $tf = $folder . 'thumbs/';
    if (!is_dir($tf)) {
        mkdir($tf, 0775);
    }

    $mf = $folder . 'medium/';
    if (!is_dir($mf)) {
        mkdir($mf, 0775);
    }

    $fileName = $_FILES[$fileParamName]['name'];

    //	retrieve uploaded file path (temporary stored by php engine)
    $sourceFilePath = $_FILES[$fileParamName]['tmp_name'];
    //	construct target file path (desired location of uploaded file)
    $targetFilePath = strtolower($folder . $prefix . $fileName);
    $thumbFile = strtolower($tf . $prefix . $fileName);
    $mediumFile = strtolower($mf . $prefix . $fileName);

    //	move uploaded file
    if (move_uploaded_file($sourceFilePath, $targetFilePath)) {
        createThumb($targetFilePath, $thumbFile, 300, 300);
        createThumb($targetFilePath, $mediumFile, 600, 600);
    }
    return $fileName;
}

/**
 * Method is used to create thumbnails.
 *
 * @return 	Response
 **/
function createThumb($name, $filename, $new_w, $new_h)
{
    $found = 0;
    $system = explode('.', $name);
    $echeck = strtolower(end($system));

    if (preg_match('/jpg|jpeg/', $echeck)) {
        $src_img = imagecreatefromjpeg($name);
        $found = 1;
    }

    if (preg_match('/png/', $echeck)) {
        $src_img = imagecreatefrompng($name);
        $found = 1;
    }

    if (preg_match('/gif/', $echeck)) {
        $src_img = imagecreatefromgif($name);
        $found = 1;
    }

    if ($found) {
        $old_x = imagesx($src_img);
        $old_y = imagesy($src_img);
        $ar = $old_x / $old_y;


        if ($new_w > $new_h) {
            $thumb_w = $new_w;
            $thumb_h = $thumb_w / $ar;
        }
        if ($new_w < $new_h) {
            $thumb_h = $new_h;
            $thumb_w = $thumb_h * $ar;
        }
        if ($new_w == $new_h) {
            $thumb_w = $new_w;
            $thumb_h = $new_h;
        }

        $dst_img = imagecreatetruecolor($thumb_w, $thumb_h);
        imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y);

        if (preg_match("/png/", $echeck)) {
            imagepng($dst_img, $filename);
        } else if (preg_match('/jpg|jpeg/', $echeck)) {
            imagejpeg($dst_img, $filename);
        } else if (preg_match("/gif/", $echeck)) {
            imagegif($dst_img, $filename);
        }

        imagedestroy($dst_img);
    }
    imagedestroy($src_img);
}

/**
 * Method is used to delete images
 *
 * @param 	string 	$file
 * @return 	Response
 **/
function deleteUploadedPicture($id, $file)
{
    $path = \Config::get('constants.UPLOADS');
    $folderPath = ROOT . $path . '/';

    if (file_exists($folderPath . $file)) {
        @unlink($folderPath . $file);
        @unlink($folderPath . "medium/" . $file);
        @unlink($folderPath . "thumbs/" . $file);
    }
}

/**
 * @param $index
 * @param $page
 * @param $perPage
 * @return mixed
 */
function pageIndex($index, $page, $perPage)
{
    return (($page - 1) * $perPage) + $index;
}

/**
 * Method is used to create pagination controls
 *
 * @param int $page
 * @param int $total
 * @param int $perPage
 *
 * @return string
 */
function paginationControls($page, $total, $perPage = 20)
{
    $paginates = '';
    $curPage = $page;
    $page -= 1;
    $previousButton = true;
    $next_btn = true;
    $first_btn = false;
    $last_btn = false;
    $noOfPaginations = ceil($total / $perPage);

    /* ---------------Calculating the starting and ending values for the loop----------------------------------- */
    if ($curPage >= 10) {
        $start_loop = $curPage - 5;
        if ($noOfPaginations > $curPage + 5) {
            $end_loop = $curPage + 5;
        } elseif ($curPage <= $noOfPaginations && $curPage > $noOfPaginations - 9) {
            $start_loop = $noOfPaginations - 9;
            $end_loop = $noOfPaginations;
        } else {
            $end_loop = $noOfPaginations;
        }
    } else {
        $start_loop = 1;
        if ($noOfPaginations > 10)
            $end_loop = 10;
        else
            $end_loop = $noOfPaginations;
    }

    $paginates .= '<div class="col-sm-5 padding0 pull-left">' .
        lang('common.jump_to') .
        '<input type="text" class="goto" size="1" />
					<button type="button" id="go_btn" class="go_button"> <span class="fa fa-arrow-right"> </span> </button> ' .
        lang('common.pages') . ' ' .  $curPage . ' of <span class="_total">' . $noOfPaginations . '</span> | ' . lang('common.total_records', $total) .
        '</div> <ul class="pagination pagination-sm margin0 pull-right">';

    // FOR ENABLING THE FIRST BUTTON
    if ($first_btn && $curPage > 1) {
        $paginates .= '<li p="1" class="disabled">
	    					<a href="javascript:void(0);">' .
            lang('common.first')
            . '</a>
	    			   </li>';
    } elseif ($first_btn) {
        $paginates .= '<li p="1" class="disabled">
	    					<a href="javascript:void(0);">' .
            lang('common.first')
            . '</a>
	    			   </li>';
    }

    // FOR ENABLING THE PREVIOUS BUTTON
    if ($previousButton && $curPage > 1) {
        $pre = $curPage - 1;
        $paginates .= '<li p="' . $pre . '" class="_paginate">
	    					<a href="javascript:void(0);" aria-label="Previous">
					        	<span aria-hidden="true">&laquo;</span>
				      		</a>
	    			   </li>';
    } elseif ($previousButton) {
        $paginates .= '<li class="disabled">
	    					<a href="javascript:void(0);" aria-label="Previous">
					        	<span aria-hidden="true">&laquo;</span>
				      		</a>
	    			   </li>';
    }

    for ($i = $start_loop; $i <= $end_loop; $i++) {
        if ($curPage == $i)
            $paginates .= '<li p="' . $i . '" class="active"><a href="javascript:void(0);">' . $i . '</a></li>';
        else
            $paginates .= '<li p="' . $i . '" class="_paginate"><a href="javascript:void(0);">' . $i . '</a></li>';
    }

    // TO ENABLE THE NEXT BUTTON
    if ($next_btn && $curPage < $noOfPaginations) {
        $nex = $curPage + 1;
        $paginates .= '<li p="' . $nex . '" class="_paginate">
	    					<a href="javascript:void(0);" aria-label="Next">
					        	<span aria-hidden="true">&raquo;</span>
					      	</a>
	    			   </li>';
    } elseif ($next_btn) {
        $paginates .= '<li class="disabled">
	    					<a href="javascript:void(0);" aria-label="Next">
					        	<span aria-hidden="true">&raquo;</span>
					      	</a>
	    			   </li>';
    }

    // TO ENABLE THE END BUTTON
    if ($last_btn && $curPage < $noOfPaginations) {
        $paginates .= '<li p="' . $noOfPaginations . '" class="_paginate">
	    					<a href="javascript:void(0);">' .
            lang('common.last')
            . '</a>
	    			   </li>';
    } elseif ($last_btn) {
        $paginates .= '<li p="' . $noOfPaginations . '" class="disabled">
	    					<a href="javascript:void(0);">' .
            lang('common.last')
            . '</a>
			   		   </li>';
    }

    $paginates .= '</ul>';

    return $paginates;
}


/**
 * Method is used for string padding with 0 based on prefix string.
 *
 * @param int $count
 * @param string $prefix
 * @param int $length
 * @return string
 */
function srNo($count = 0, $prefix = '', $length = 4)
{
    return $prefix . str_pad($count, $length, 0, STR_PAD_LEFT);
}


function getLoggedInUser()
{
    return WebsiteUser::find(session()->get('user_id'));
}


function importantLinks()
{
    return ImportantLink::where('status', 1)->orderBy('_order', 'asc')->get();
}
