<?php
namespace App;
/**
 * :: Appointments Model ::
 * To manage Appointments CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointments extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'appointment_master';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'appointment_type',
        'name',
        'mobile',
        'email',
        'appointment_date',
        'appointment_utc_date',
        'appointment_time_slot',
        'message',
        'status',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  \Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            // save role
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }        
    }

    /**
     * Method is used to search detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getAppointments($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        // default filter if no search
        $filter = 1;

        $fields = [
            'appointment_master.id',
            'appointment_master.appointment_type',
            'appointment_master.name',
            'appointment_master.email',
            'appointment_master.mobile',
            'appointment_master.appointment_date',
            'appointment_master.appointment_utc_date',
            'appointment_master.appointment_time_slot',
            'appointment_master.message',
            'appointment_master.status',
        ];

        if (is_array($search) && count($search) > 0)
        {
            $filter .= (array_key_exists('keyword', $search)) ? " AND (appointment_master.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' OR appointment_master.email LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' OR appointment_master.mobile LIKE '%" .
                addslashes(trim($search['keyword'])) . "%')" : "";

        }

        return $this->whereRaw($filter)->orderBy('appointment_master.id', 'DESC')
            ->skip($skip)->take($take)->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalAppointments($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0)
        {
            $filter .= (array_key_exists('keyword', $search)) ? " AND appointment_master.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
        }
        return $this->select(\DB::raw('count(*) as total'))
                ->whereRaw($filter)->first();
    }

    /**
     * @param array $search
     * @return int
     */
    public function checkAppointmentSlotBooked($search = [])
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0)
        {
            $filter .= (array_key_exists('appointment_date', $search)) ? " AND appointment_master.appointment_date = '" .
                addslashes(trim($search['appointment_date'])) . "' " : "";
            
            $filter .= (array_key_exists('time_slot', $search)) ? " AND appointment_master.appointment_time_slot = '" .
                addslashes(trim($search['time_slot'])) . "' " : "";

            return $this->whereRaw($filter)->count();
        }
        return 0;
    }
}
