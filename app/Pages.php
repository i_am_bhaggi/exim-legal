<?php
namespace App;
/**
 * :: Pages Model ::
 * To manage Pages CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pages extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'title',
        'byline',
        'url_slug',
        'short_intro',
        'full_detail',
        'p_order',
        'image',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'hits',
        'status',
        'created_by',
        'updated_by',

    ];

    /**
     * Scope a query to only include active users.
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }
     
    /**
     * Method is used to validate roles 
     * @param $inputs
     * @param int $id
     * @return \Response
     */
    public function validatePages($inputs, $id = null)
    {
        // validation rule
        $rules = [
            'title' => 'required|min:2|max:255',
            'byline' => 'required|min:2|max:255',
            //'menu' => 'required_without:category',
            //'date' => 'required',
            //'keywords' => 'required',
            //'short_intro' => 'required'
        ];

        $message = [
            'byline.required' => 'The page name field is required.',
        ];
        return \Validator::make($inputs, $rules, $message);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  \Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }        
    }

    /**
     * Method is used to search news detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     * @return mixed
     */
    public function getPages($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        $filter = 1; // default filter if no search

        $fields = [
            'pages.id',
            'pages.title',
            'pages.byline',
            'pages.status',
            'pages.p_order',
        ];
        
        $orderEntity = 'pages.p_order';
        $orderAction = 'DESC';

        if (is_array($search) && count($search) > 0)
        {
            $filter .= (array_key_exists('keyword', $search)) ?
                " AND (pages.title LIKE '%".addslashes(trim($search['keyword'])) . "%')" : "";
        }

        return $this->whereRaw($filter)
                ->orderBy($orderEntity, $orderAction)
                ->skip($skip)->take($take)->get($fields);
    }

    /**
     * Method is used to get total results.
     * @param array $search
     * @return mixed
     */
    public function totalPages($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0)
        {
            $filter .= (array_key_exists('keyword', $search)) ?
                " AND (pages.title LIKE '%".addslashes(trim($search['keyword'])) . "%')" : "";
        }
        return $this->select(\DB::raw('count(*) as total'))
                    ->whereRaw($filter)
                    ->first();
    }

    /**
     * @param $id
     */
    public function drop($id)
    {
        $this->find($id)->delete();
    }

    /**
     * @param array $search
     * @return null
     */
    public function getPageDetail($search = [])
    {
        $result = null;
        if(is_array($search) && count($search) > 0)
        {
            $fields = [
                'id',
                'title',
                'url_slug',
                'byline',
                'short_intro',
                'meta_title',
                'meta_keywords',
                'meta_description',
                'full_detail',
                'status',
            ];
            $filter = 1;
            $filter .= (array_key_exists('id', $search) && $search['id'] != "") ?
                " AND (pages.id = '".addslashes(trim($search['id'])). "')" : "";

            $result = $this->whereRaw($filter)->first($fields);
        }
        return $result;
    }

    /**
     * @param array $search
     * @return null
     */
    public function getPageByFilter($search = [])
    {
        $result = null;
        if(is_array($search) && count($search) > 0)
        {
            $fields = [
                'id',
                'title',
                'url_slug',
                'byline',
                'short_intro',
                'meta_title',
                'meta_keywords',
                'meta_description',
                'status',
            ];
            $filter = 1;
            $result = $this->active()
                ->whereRaw($filter)
                ->orderBy('p_order', 'DESC')
                ->get($fields);
        }
        return $result;
    }

    /**
     * @return mixed
     */
    public function getPageService()
    {
        $data = $this->get(['title', 'id']);
        $result = [];
        foreach($data as $detail) {
            $result[$detail->id] = $detail->title;
        }
        return ['' => '-Select Page-'] + $result;
    }
}
