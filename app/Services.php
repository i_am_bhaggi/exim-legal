<?php

namespace App;

/**
 * :: Services Model ::
 * To manage Services CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'service_master';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'inner_text',
        'code',
        'image',
        'url_slug',
        '_order',
        'short_intro',
        'full_detail',
        'meta_title',
        'keywords',
        'description',
        'status',
        'frontpage',
        'show_menu',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to validate
     *
     * @param int $id
     * @return Response
     **/
    public function validateServices($inputs, $id = null)
    {
        // validation rule
        if ($id) {
            $rules['name'] = 'required|unique:service_master,name,' . $id . ',id,deleted_at,NULL';
        } else {
            $rules['name'] = 'required|unique:service_master,name,NULL,id,deleted_at,NULL';
        }
        //$rules['image'] = 'mimes:jpeg,bmp,png';
        return \Validator::make($inputs, $rules);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            // save role
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }
    }

    /**
     * Method is used to search detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getServices($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        // default filter if no search
        $filter = 1;

        $fields = [
            'service_master.id',
            'service_master.name',
            'service_master.status',
            'service_master.frontpage',
        ];

        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search)) ? " AND service_master.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $f1;
        }

        return $this->whereRaw($filter)
            ->orderBy('service_master._order', 'DESC')
            ->skip($skip)->take($take)->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalServices($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0) {
            $f1 = (array_key_exists('keyword', $search)) ? " AND name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
            $filter .= $f1;
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->first();
    }

    /**
     * @param bool $parent
     * @return mixed
     */
    public function getServicesService($parent = false)
    {
        $data = $this->active()->orderBy('name', 'ASC');
        $data = $data->lists('name', 'id')->toArray();
        $result = [];
        foreach ($data as $id => $name) {
            $result[$id] = $name;
        }
        return ['' => '-Select Service-'] + $result;
    }

    /**
     * @return array
     */
    public function getNavigation()
    {
        $data = $this->where('service_master.status', 1)
            ->orderBy('service_master._order', 'DESC')
            ->get(
                [
                    'service_master.id',
                    'service_master.name'
                ]
            );
        $result = $data->toArray();
        return $this->prepareNavigation($result);
    }

    /**
     * @param $data
     * @param null $parent
     *
     * @return array
     */
    private function prepareNavigation($data, $parent = null)
    {
        $nav = array();
        $x = 0;
        foreach ($data as $d) {
            if ($d['parent_id'] == $parent) {
                $child = $this->prepareNavigation($data, $d['id']);
                // set a trivial key
                if (!empty($child)) {
                    $d['child'] = $child;
                } else {
                    $d['child'] = [];
                }
                $nav[$x]['id'] = $d['id'];
                $nav[$x]['name'] = $d['name'];

                if (array_key_exists('child', $d)) {
                    $nav[$x]['child'] =  $d['child'];
                }
                $x++;
            }
        }
        return $nav;
    }

    /**
     * @param array $search
     * @param int $take
     * @param int $skip
     * @return array
     */
    public function getFilterServices($search = [], $take = 0, $skip = 0)
    {
        $filter = 1;
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('frontpage', $search)) ? " AND service_master.frontpage = " .
                addslashes(trim($search['frontpage'])) : "";

            $filter .= (array_key_exists('id', $search)) ? " AND service_master.id = " .
                addslashes(trim($search['id'])) : "";

            $filter .= (array_key_exists('show_menu', $search)) ? " AND service_master.show_menu = 1" : "";

            $filter .= (array_key_exists('url_slug', $search)) ? " AND service_master.url_slug = '" .
                addslashes(trim($search['url_slug'])) . "'" : "";
        }
        //dd($filter);
        $result = $this->active()->whereRaw($filter)->orderBy('service_master._order', 'DESC');

        if ($skip != 0) {
            $result = $result->skip($skip);
        }

        if ($take != 0) {
            $result = $result->take($take);
        }

        if (array_key_exists('id', $search) || array_key_exists('url_slug', $search)) {
            return $result->first(
                [
                    'id', 'name',
                    'image', 'short_intro',
                    'full_detail', 'url_slug',
                    'url_slug', 'meta_title',
                    'keywords', 'description',
                    'inner_text'
                ]
            );
        }
        return $result->get(
            [
                'id', 'name',
                'image', 'short_intro',
                'full_detail', 'url_slug',
                'url_slug', 'meta_title',
                'keywords', 'description',
                'inner_text'
            ]
        );
    }
}
