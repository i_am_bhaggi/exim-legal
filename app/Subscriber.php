<?php

namespace App;

/**
 * :: Subscribe Model ::
 * To manage Subscribe CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscriber_master';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'subscribe_ip',
        'subscribe_date',
        'unsubscribe_ip',
        'unsubscribe_date'
    ];

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  \Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            // save role
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }
    }

    /**
     * Method is used to search detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getSubscriber($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        // default filter if no search
        $filter = 1;

        $fields = [
            'subscriber_master.id',
            'subscriber_master.name',
            'subscriber_master.email',
            'subscriber_master.subscribe_date',
            'subscriber_master.unsubscribe_date',
        ];

        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('keyword', $search)) ? " AND (subscriber_master.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' OR subscriber_master.email LIKE '%" .
                addslashes(trim($search['keyword'])) . "%')" : "";
        }

        if (array_key_exists('total', $search)) {
            return $this->whereRaw($filter)->count();
        }

        return $this->whereRaw($filter)->orderBy('subscriber_master.id', 'DESC')
            ->skip($skip)->take($take)->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalSubscriber($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('keyword', $search)) ? " AND (subscriber_master.name LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' OR subscriber_master.email LIKE '%" .
                addslashes(trim($search['keyword'])) . "%')" : "";
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->first();
    }
}
