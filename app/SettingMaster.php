<?php
namespace App;
/**
 * :: Setting Master Model ::
 * To manage Setting Master CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;

class SettingMaster extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'setting_master';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_name',
        'site_logo',
        'address',
        'other_text',
        'mobile',
        'phone',
        'email',
        'website',
        'meta_title',
        'meta_keyword',
        'meta_description',
        'all_rights_reserved',
        'facebook_url',
        'twitter_url',
        'googleplus_url',
        'instagram_url',
        'youtube_url',
    ];

    public function validateWebSetting($inputs)
    {
        $message = [];
        $rules['site_name'] = 'required';
        $rules['email'] = 'email';
        //$rules['mobile'] = 'digits_between:7,11';
        //$rules['phone'] = 'digits_between:7,11';
        //$rules['website'] = 'alphanumeric';
        $rules['address'] = 'required';
        //$rules['all_rights_reserved'] = 'required';

        $weekDays = $inputs['weekday'];
        foreach ($weekDays as $key => $day)
        {
            $rules['weekday.'.$key] = 'required';
            $rules['timing.'.$key] = 'required';

            $message['weekday.'.$key .'.required'] = '* Required.';
            $message['timing.'.$key .'.required'] = '* Required.';
        }

        return validator($inputs, $rules, $message);
    }
    
    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  \Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            // save role
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }        
    }

    /**
     * Method is used to search detail.
     *
     * @return mixed
     */
    public function getSetting()
    {
        return $this->where('id', 1)->first();
    }
}
