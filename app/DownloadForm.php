<?php

namespace App;

/**
 * :: Slider Banner Model ::
 * To manage Slider Banner CRUD operations
 *
 **/

use Illuminate\Database\Eloquent\Model;

class DownloadForm extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'download_forms';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'file',
        '_order',
        'status',
        'created_at',
        'updated_at'
    ];

    /**
     * Scope a query to only include active users.
     *
     * @param $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('download_forms.status', 1);
    }

    /**
     * Method is used to validate roles
     *
     * @param $inputs
     * @param int $id
     * @return \Response
     */
    public function validateDownloadForm($inputs, $id = null)
    {
        // validation rule
        $rules['pdf-file'] = 'nullable|mimes:pdf';
        if (!$id) {
            $rules['pdf-file'] = 'required|mimes:pdf';
        }
        $message = [
            'pdf-file.required' => '* Required',
            'pdf-file.mimes' => 'File must be pdf file format.',
        ];
        return \Validator::make($inputs, $rules, $message);
    }

    /**
     * Method is used to save/update resource.
     *
     * @param   array $input
     * @param   int $id
     * @return  \Response
     */
    public function store($input, $id = null)
    {
        if ($id) {
            return $this->find($id)->update($input);
        } else {
            return $this->create($input)->id;
        }
    }

    /**
     * Method is used to search news detail.
     *
     * @param array $search
     * @param int $skip
     * @param int $perPage
     *
     * @return mixed
     */
    public function getDownloadForms($search = null, $skip, $perPage)
    {
        $take = ((int)$perPage > 0) ? $perPage : 20;
        $filter = 1; // default filter if no search

        $fields = [
            'id',
            'title',
            'file',
            '_order',
            'status',
            'created_at'
        ];

        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('keyword', $search) && $search['keyword'] != "") ? " AND title LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";

            $filter .= (array_key_exists('active', $search) && $search['active'] != "") ? " AND status = '" .
                addslashes(trim($search['active'])) . "' " : "";
        }

        return $this->whereRaw($filter)->orderBy('_order', 'DESC')
            ->skip($skip)->take($take)
            ->get($fields);
    }

    /**
     * Method is used to get total results.
     *
     * @param array $search
     *
     * @return mixed
     */
    public function totalDownloadForms($search = null)
    {
        $filter = 1; // if no search add where

        // when search
        if (is_array($search) && count($search) > 0) {
            $filter .= (array_key_exists('keyword', $search) && $search['keyword'] != "") ? " AND title LIKE '%" .
                addslashes(trim($search['keyword'])) . "%' " : "";
        }
        return $this->select(\DB::raw('count(*) as total'))
            ->whereRaw($filter)->first();
    }
}
