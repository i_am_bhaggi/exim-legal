<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteUser extends Model
{
    protected $table = "web_users";
    protected $primaryKey = "id";
}
