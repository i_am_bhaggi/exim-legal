<?php 
namespace App\Http\Controllers;
/**
 * :: Appointments Controller ::
 * To manage Appointments.
 *
 **/

use App\Http\Controllers\Controller;
use App\Appointments;
use Illuminate\Http\Request;

class AppointmentsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
		return view('appointments.index');
	}

	/**
	 * Used to update appointments active status.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function appointmentToggle($id = null)
	{
		if (!\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
            // get the appointments w.r.t id
            $result = Appointments::find($id);
        } catch (\Exception $exception) {
            return lang('messages.invalid_id', string_manip(lang('appointments.appointment')));
        }

		$result->update(['status' => !$result->status]);
        $response = ['status' => 1, 'data' => (int)!$result->status . '.gif'];
        // return json response
        return json_encode($response);
	}

	/**
	 * Used to load more records and render to view.
	 *
	 * @param Request $request
	 * @return \Response
	 * @internal param int $pageNumber
	 *
	 */
	public function appointmentPaginate(Request $request)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) { //
			return lang('messages.server_error');
		}

		$inputs = $request->all();
		$page = 1;
		if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
			$page = $inputs['page'];
		}

		$perPage = 20;
		if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
			$perPage = $inputs['perpage'];
		}

		$start = ($page - 1) * $perPage;
		if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
			$inputs = array_filter($inputs);
			unset($inputs['_token']);

			$data = (new Appointments)->getAppointments($inputs, $start, $perPage);
			$totalAppointments = (new Appointments)->totalAppointments($inputs);
			$total = $totalAppointments->total;
		} else {

			$data = (new Appointments)->getAppointments($inputs, $start, $perPage);
			$totalAppointments = (new Appointments)->totalAppointments($inputs);
			$total = $totalAppointments->total;
		}

		return view('appointments.load_data', compact('data', 'total', 'page', 'perPage'));
	}
}