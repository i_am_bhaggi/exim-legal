<?php

namespace App\Http\Controllers;

/**
 * :: Testimonial Controller ::
 * To manage important-link.
 *
 **/

use App\Http\Controllers\Controller;
use App\ImportantLink;
use App\Testimonial;
use Illuminate\Http\Request;

class ImportantLinkController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Response
     */
    public function index()
    {
        return view('important-link.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Response
     */
    public function create()
    {
        return view('important-link.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Response
     */
    public function store(Request $request)
    {

        $inputs = $request->all();
        $validator = (new ImportantLink)->validateImportantLinks($inputs);
        if ($validator->fails()) {
            return validationResponse(false, 206, "", "", $validator->messages());
        }
        $url = str_slug($inputs['title']);

        try {
            \DB::beginTransaction();
            $inputs = $inputs + [
                'status'           => (!isset($inputs['status']) ? 0 : 1),
                'created_by'     => authUserId()
            ];
            $id = (new ImportantLink)->store($inputs);
            \DB::commit();
            return validationResponse(true, 201, lang(
                'messages.created',
                lang('important-link.testimonial')
            ), route('important-link.index'));
        } catch (\Exception $exception) {
            \DB::rollBack();
            return validationResponse(false, 207, lang('messages.server_error'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Response
     */
    public function edit($id = null)
    {
        $result = ImportantLink::find($id);
        if (!$result) {
            abort(404);
        }

        return view('important-link.edit', compact('result'));
    }


    /**
     * Method is used to sort news.
     *
     * @param Request $request
     * @return \Response
     */
    public function sortingImportantLink(Request $request)
    {
        $inputAll = $request->all();
        $order = $inputAll['order'];
        try {
            if (count($order) > 0) {
                $index = count($order);
                foreach ($order as $key => $value) {
                    ImportantLink::find($value)->update(['_order' => $index--]);
                }
            }
            // return 1 for successfully sorted news.
            echo '1';
        } catch (\Exception $e) {
            // else return 0
            echo '0';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Response
     */
    public function update(Request $request, $id = null)
    {
        $result = ImportantLink::find($id);
        if (!$result) {
            return validationResponse(false, 207, lang(
                'messages.invalid_id',
                string_manip(lang('important-link.testimonial'))
            ));
        }
        $inputs = $request->all();
        $validator = (new ImportantLink)->validateImportantLinks($inputs, $id);
        if ($validator->fails()) {
            return validationResponse(false, 206, "", "", $validator->messages());
        }
        try {
            \DB::beginTransaction();
            // $inputs = $inputs + [
            //     'status'       => (!isset($inputs['status']) ? 0 : 1),
            //     'updated_by' => authUserId()
            // ];
            (new ImportantLink)->store($inputs, $id);
            \DB::commit();
            return validationResponse(true, 201, lang(
                'messages.updated',
                lang('important-link.testimonial')
            ), route('important-link.index'));
        } catch (\Exception $exception) {
            \DB::rollBack();
            return validationResponse(false, 207, lang('messages.server_error'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Response
     */
    public function drop($id)
    {
        return "In Progress";
    }

    /**
     * Used to update testimonial active status.
     *
     * @param int $id
     * @return \Response
     */
    public function importantLinkToggle($id = null)
    {
        if (!\Request::ajax()) {
            return lang('messages.server_error');
        }

        try {
            // get the testimonial w.r.t id
            $result = ImportantLink::find($id);
        } catch (\Exception $exception) {
            return lang('messages.invalid_id', string_manip(lang('important-link.testimonial')));
        }

        $result->update(['status' => !$result->status]);
        $response = ['status' => 1, 'data' => (int)$result->status . '.gif'];
        // return json response
        return json_encode($response);
    }

    /**
     * Used to load more records and render to view.
     *
     * @param Request $request
     * @return \Response
     * @internal param int $pageNumber
     *
     */
    public function importantLinkPaginate(Request $request)
    {
        if (!\Request::isMethod('post') && !\Request::ajax()) { //
            return lang('messages.server_error');
        }

        $inputs = $request->all();
        $page = 1;
        if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
            $page = $inputs['page'];
        }

        $perPage = 20;
        if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
            $perPage = $inputs['perpage'];
        }

        $start = ($page - 1) * $perPage;
        if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
            $inputs = array_filter($inputs);
            unset($inputs['_token']);

            $data = (new ImportantLink)->getImportantLinks($inputs, $start, $perPage);
            $totalImportantLinks = (new ImportantLink)->totalImportantLinks($inputs);
            $total = $totalImportantLinks->total;
        } else {

            $data = (new ImportantLink)->getImportantLinks($inputs, $start, $perPage);
            $totalImportantLinks = (new ImportantLink)->totalImportantLinks($inputs);
            $total = $totalImportantLinks->total;
        }
        return view('important-link.load_data', compact('data', 'total', 'page', 'perPage'));
    }

    /**
     * Used to update product category active status.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function markedFeatured($id)
    {
        if (!\Request::isMethod('post') && !\Request::ajax()) {
            return lang('messages.server_error');
        }

        try {
            $result = ImportantLink::find($id);
        } catch (\Exception $exception) {
            return lang('messages.invalid_id', string_manip(lang('important-link.testimonial')));
        }

        $result->update(['frontpage' => !$result->frontpage]);
        $response = ['status' => 1, 'data' => (int)$result->frontpage . '.png'];
        // return json response
        return json_encode($response);
    }
}
