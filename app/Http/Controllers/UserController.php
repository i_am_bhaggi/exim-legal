<?php

namespace App\Http\Controllers;

/**
 * :: User Controller ::
 * To manage users.
 *
 **/

use App\Menu;
use App\Http\Controllers\Controller;
use App\Role;
use App\State;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\UserPermissions;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $role = (new Role)->getRoleService();
        $state = (new State)->getStateService();
        unset($role['2']);
        unset($role['3']);
        //$tree = (new Menu)->getMenuNavigation();
        return view('user.create', compact('role', 'state'));
    }

    public function changePassword()
    {
        return view('changepassword');
    }

    public function updatePassword(Request $request)
    {

        if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error", "Your current password does not matches with the password you provided. Please try again.");
        }

        if (strcmp($request->get('old_password'), $request->get('new_password')) == 0) {
            //Current password and new password are same
            return redirect()->back()->with("error", "New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|string|min:6',
            'confirm_password' => 'required|same:new_password'
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect()->back()->with("success", "Password changed successfully !");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $inputs = \Input::all();
        $inputs['username'] = $inputs['mobile'];
        $validator = (new User)->validateUser($inputs);
        if ($validator->fails()) {
            return validationResponse(false, 206, "", "", $validator->messages());
        }

        try {
            $password = $inputs['password'];
            $role = $inputs['role'];
            $state = $inputs['state'];
            $dob = $inputs['dob'];
            unset($inputs['password']);
            unset($inputs['role']);
            unset($inputs['dob']);
            \DB::beginTransaction();
            $inputs = $inputs + [
                'role_id'       => $role,
                'state_id'       => $state,
                'dob'       => ($dob != "") ? dateFormat('Y-m-d', $dob) : null,
                'status'       => (!isset($inputs['status']) ? 0 : 1),
                'password'      => \Hash::make($password),
                'created_by'    => authUserId()
            ];

            (new User)->store($inputs);

            \DB::commit();
            return validationResponse(true, 201, lang('messages.created', lang('user.user')), route('user.index'));
        } catch (\Exception $exception) {
            \DB::rollBack();
            return validationResponse(false, 207, lang('messages.server_error'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->route('user.index')
                ->with('error', lang('messages.invalid_id', string_manip(lang('user.user'))));
        }

        if ($user->id == 1) {
            if ($user->id != authUserId()) {
                return redirect()->route('user.index')
                    ->with('error', lang('messages.permission_denied'));
            }
        }

        if (in_array($user->role_id, [2, 3])) {
            return redirect()->route('user.index')
                ->with('error', lang('messages.permission_denied'));
        }

        $role = (new Role)->getRoleService();
        $state = (new State)->getStateService();
        unset($role['2']);
        unset($role['3']);
        //dd($userPermissions->toArray());
        return view('user.edit', compact('user', 'role', 'state'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function customerEdit($id = null)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->route('user.index')
                ->with('error', lang('messages.invalid_id', string_manip(lang('user.user'))));
        }

        if ($user->id == 1) {
            if ($user->id != authUserId()) {
                return redirect()->route('user.index')
                    ->with('error', lang('messages.permission_denied'));
            }
        }

        if (in_array($user->role_id, [1, 3])) {
            return redirect()->route('user.index')
                ->with('error', lang('messages.permission_denied'));
        }

        $userType = 2;
        $role = (new Role)->getRoleService();
        $state = (new State)->getStateService();
        unset($role['2']);
        unset($role['3']);
        //dd($userPermissions->toArray());
        return view('user.edit', compact('user', 'role', 'state', 'userType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function dealerEdit($id = null)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect()->route('user.index')
                ->with('error', lang('messages.invalid_id', string_manip(lang('user.user'))));
        }

        if ($user->id == 1) {
            if ($user->id != authUserId()) {
                return redirect()->route('user.index')
                    ->with('error', lang('messages.permission_denied'));
            }
        }

        if (in_array($user->role_id, [1, 2])) {
            return redirect()->route('user.index')
                ->with('error', lang('messages.permission_denied'));
        }

        $userType = 2;
        $role = (new Role)->getRoleService();
        $state = (new State)->getStateService();
        unset($role['2']);
        unset($role['3']);
        //dd($userPermissions->toArray());
        return view('user.edit', compact('user', 'role', 'state', 'userType'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $user = User::find($id);
        if (!$user) {
            return validationResponse(false, 207, lang('messages.invalid_id', string_manip(lang('user.user'))));
        }

        if ($user->id == 1) {
            if ($user->id != authUserId()) {
                return validationResponse(false, 207, lang('messages.permission_denied'));
            }
        }

        $inputs = \Input::except('section');
        $validator = (new User)->validateUser($inputs, $id);
        if ($validator->fails()) {
            return validationResponse(false, 206, "", "", $validator->messages());
        }

        $role = $inputs['role'];
        if ($role == 3) {
            $state = (new State)->getStateById($inputs['state']);
            $gst = substr($inputs['gst_number'], 0, 2);
            if ($gst != $state->state_digit_code) {
                return validationResponse(false, 207, "Invalid GST Number.");
            }
        }

        try {
            \DB::beginTransaction();
            $state = $inputs['state'];
            $dob = $inputs['dob'];
            unset($inputs['password']);
            unset($inputs['role']);
            unset($inputs['dob']);
            $inputs = $inputs + [
                'role_id'   => $role,
                'state_id'  => $state,
                'dob'       => ($dob != "") ? dateFormat('Y-m-d', $dob) : null,
                'status' => (!isset($inputs['status']) ? 0 : 1),
                'updated_by' => authUserId()
            ];
            (new User)->store($inputs, $id);
            \DB::commit();
            $route = route('user.index');
            $lang = lang('messages.updated', lang('user.user'));

            if ($role == 2) {
                $route = route('customer.index');
                $lang = lang('messages.updated', lang('customer.customer'));
            } elseif ($role == 3) {
                $route = route('dealers.index');
                $lang = lang('messages.updated', lang('customer.dealer'));
            }
            return validationResponse(true, 201, $lang, $route);
        } catch (\Exception $exception) {
            \DB::rollBack();
            return validationResponse(false, 207, lang('messages.server_error'));
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function drop($id)
    {
        return "In Progress";
    }

    /**
     * Used to update User active status.
     *
     * @param  int $id
     * @return string
     */
    public function userToggle($id = null)
    {
        if (!\Request::isMethod('post') && !\Request::ajax()) {
            return lang('messages.server_error');
        }

        try {
            // get the User w.r.t id
            $user = User::find($id);
        } catch (\Exception $exception) {
            return lang('messages.invalid_id', string_manip(lang('user.user')));
        }

        $user->update(['status' => !$user->status]);
        $response = ['status' => 1, 'data' => (int)$user->status . '.gif'];
        // return json response
        return json_encode($response);
    }

    /**
     * Used to load more records and render to view.
     *
     * @param int $pageNumber
     * @return mixed
     */
    public function userPaginate($pageNumber = null)
    {
        if (!\Request::isMethod('post') && !\Request::ajax()) {
            return lang('messages.server_error');
        }

        $inputs = \Input::all();
        $page = 1;
        if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
            $page = $inputs['page'];
        }

        $perPage = 20;
        if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
            $perPage = $inputs['perpage'];
        }

        $start = ($page - 1) * $perPage;
        $filter = '';

        if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
            $inputs = array_filter($inputs);
            unset($inputs['_token']);

            $data = (new User)->getUsers($inputs, $start, $perPage);
            $totalUser = (new User)->totalUser($inputs);
            $total = $totalUser->total;
        } else {
            $data = (new User)->getUsers($filter, $start, $perPage);

            $totalUser = (new User)->totalUser($inputs);
            $total = $totalUser->total;
        }

        return view('user.load_data', compact('data', 'total', 'page', 'perPage'));
    }

    /**
     * Method is used to update status of user enable/disable
     *
     * @return Response
     */
    public function userAction()
    {
        $inputs = \Input::all();
        if (!isset($inputs['tick']) || count($inputs['tick']) < 1) {
            return redirect()->route('user.index')
                ->with('error', lang('messages.atleast_one', string_manip(lang('user.user'))));
        }

        $ids = '';
        foreach ($inputs['tick'] as $key => $value) {
            $ids .= $value . ',';
        }
        $ids = rtrim($ids, ',');
        $status = 0;
        if (isset($inputs['active'])) {
            $status = '1';
        }

        User::whereRaw('id IN (' . $ids . ')')->update(['status' => $status]);
        return redirect()->route('user.index')
            ->with('success', lang('messages.updated', lang('user.user_status')));
    }
}
