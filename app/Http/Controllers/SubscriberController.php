<?php

namespace App\Http\Controllers;

/**
 * :: Subscriber Controller ::
 * To manage Subscriber.
 *
 **/

use App\Http\Controllers\Controller;
use App\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
		return view('subscriber.index');
	}

	/**
	 * Used to update subscriber active status.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function subscriberToggle($id = null)
	{
		if (!\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			// get the subscriber w.r.t id
			$result = Subscriber::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('subscriber.subscriber')));
		}

		$result->update(['status' => !$result->status]);
		$response = ['status' => 1, 'data' => (int)!$result->status . '.gif'];
		// return json response
		return json_encode($response);
	}

	/**
	 * Used to load more records and render to view.
	 *
	 * @param Request $request
	 * @return \Response
	 * @internal param int $pageNumber
	 *
	 */
	public function subscriberPaginate(Request $request)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) { //
			return lang('messages.server_error');
		}

		$inputs = $request->all();
		$page = 1;
		if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
			$page = $inputs['page'];
		}

		$perPage = 20;
		if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
			$perPage = $inputs['perpage'];
		}

		$start = ($page - 1) * $perPage;
		if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
			$inputs = array_filter($inputs);
			unset($inputs['_token']);

			$data = (new Subscriber)->getSubscriber($inputs, $start, $perPage);
			$totalSubscriber = (new Subscriber)->totalSubscriber($inputs);
			$total = $totalSubscriber->total;
		} else {
			$data = (new Subscriber)->getSubscriber($inputs, $start, $perPage);
			$totalSubscriber = (new Subscriber)->totalSubscriber($inputs);
			$total = $totalSubscriber->total;
		}

		return view('subscriber.load_data', compact('data', 'total', 'page', 'perPage'));
	}
}
