<?php
namespace App\Http\Controllers;

/**
 * :: Dashboard Controller ::
 * To manage dashboard settings.
 *
 **/

use App\Http\Controllers\Controller;
use App\User;
use App\Voucher;
use App\Transaction;
use App\VoucherEntry;

class DashboardController extends Controller
{
    /**
     * Display a dashboard view.
     *
     * @return Response
     */
    public function index()
    {
        if (\Auth::user()->id > 1) {
            return view('dashboard');
        } else {
            return view('system_admin');
        }
    }

    /**
     * @return \Illuminate\View\View
     */
    public function changePasswordForm()
    {
        return view('changepassword');
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function changePassword()
    {
        $inputs = \Input::all();
        $password = \Auth::user()->password;
        if(!(\Hash::check($inputs['old_password'], $password))){
            return redirect()->route('changepassword')
                ->with("error", "Incorrect Old Password.");
        }

        $validator = (new user)->validatePassword($inputs);

        if ($validator->fails()) {
            return redirect()->route('changepassword')
                ->withErrors($validator);
        }

        if ((new user)->updatePassword(\Hash::make($inputs['new_password']))) {
            return redirect()->route('changepassword')
                ->with("success", "Password Successfully Updated.");
        } else {
            return redirect()->route('changepassword')
                ->withErrors("Internal Server Error");
        }
    }
}