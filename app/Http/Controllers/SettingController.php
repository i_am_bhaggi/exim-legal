<?php

namespace App\Http\Controllers;

use App\Laboratory;
use App\LaboratoryProfile;
use App\SettingMaster;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function myAccount(Request $request)
    {
        $inputs = $request->all();
        if (count($inputs) > 0) {
            $validator = (new User)->validatePassword($inputs);
            if ($validator->fails()) {
                return validationResponse(false, 206, "", "", $validator->messages());
            }

            $password = \Auth::user()->password;
            if(!(\Hash::check($inputs['password'], $password))){
                return validationResponse(false, 207, lang('messages.invalid_password'));
            }

            (new User)->updatePassword(\Hash::make($inputs['new_password']));
            return validationResponse(true, 201, lang('messages.password_updated'), route('setting.manage-account'));
        }
        return view('setting.account');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function webSetting(Request $request)
    {
        $inputs = $request->all();
        if (count($inputs) > 0)
        {
            $validator = (new SettingMaster)->validateWebSetting($inputs);
            if ($validator->fails()) {
                return validationResponse(false, 206, "", "", $validator->messages());
            }

            $timings = [];
            $weekDays = array_filter($inputs['weekday']);
            foreach ($weekDays as $key => $day) {
                $time = $inputs['timing'][$key];
                $timings[strtolower($day)] = $time;
            }
            $inputs['other_text'] = json_encode(['timings' => $timings]);
            //dd($inputs);
            (new SettingMaster)->store($inputs, 1);
            return validationResponse(true, 201, lang('messages.updated', lang('setting.web_setting')),
                route('setting.web-setting'));
        }
        $setting = (new SettingMaster)->getSetting();
        return view('setting.web-setting', compact('setting'));
    }
}
