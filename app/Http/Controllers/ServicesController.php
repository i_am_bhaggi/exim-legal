<?php

namespace App\Http\Controllers;

/**
 * :: Services Controller ::
 * To manage services.
 *
 **/

use App\Http\Controllers\Controller;
use App\Services;
use Illuminate\Http\Request;

class ServicesController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
		return view('services.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Response
	 */
	public function create()
	{
		return view('services.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return \Response
	 */
	public function store(Request $request)
	{
		$inputs = $request->all();
		$validator = (new Services)->validateServices($inputs);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		if ($inputs['image'] != "") {
			list($width, $height) = getimagesize($_FILES['image']['tmp_name']);
			if ($width != 300 && $height != 300) {
				return validationResponse(false, 207, lang('messages.invalid_file_size'));
			}
		}

		$url = str_slug($inputs['name']);

		try {
			\DB::beginTransaction();
			$inputs = $inputs + [
				'url_slug' 		=> $url,
				'status'       	=> (!isset($inputs['status']) ? 0 : 1),
				'created_by' 	=> authUserId()
			];
			$id = (new Services)->store($inputs);
			if ($inputs['image'] != "") {
				$fileName = fileUploader('image', $id);
				$fileImage = [
					'image' => $fileName
				];
				(new Services)->store($fileImage, $id);
			}
			\DB::commit();
			return validationResponse(true, 201, lang(
				'messages.created',
				lang('services.service')
			), route('services.index'));
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, lang('messages.server_error'));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function edit($id = null)
	{
		$result = Services::find($id);
		if (!$result) {
			abort(404);
		}

		return view('services.edit', compact('result'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return \Response
	 */
	public function update(Request $request, $id = null)
	{
		$result = Services::find($id);
		if (!$result) {
			return validationResponse(false, 207, lang(
				'messages.invalid_id',
				string_manip(lang('services.service'))
			));
		}

		$inputs = $request->all();
		$validator = (new Services)->validateServices($inputs, $id);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		if ($inputs['image'] != "") {
			list($width, $height) = getimagesize($_FILES['image']['tmp_name']);
			if ($width != 300 && $height != 300) {
				return validationResponse(false, 207, lang('messages.invalid_file_size'));
			}
		}

		try {
			\DB::beginTransaction();

			$url = str_slug($inputs['name']);

			$inputs = $inputs + [
				'url_slug' 		=> $url,
				'status'       => (!isset($inputs['status']) ? 0 : 1),
				'updated_by' => authUserId()
			];

			if ($inputs['image'] != "") {
				$fileName = fileUploader('image', $id, $inputs['old_image']);
				$inputs = [
					'image' => $fileName
				];
			} else {
				unset($inputs['image']);
			}
			(new Services)->store($inputs, $id);
			\DB::commit();
			return validationResponse(true, 201, lang(
				'messages.updated',
				lang('services.service')
			), route('services.index'));
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, lang('messages.server_error'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function drop($id)
	{
		return "In Progress";
	}

	/**
	 * Used to update services active status.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function serviceToggle($id = null)
	{
		if (!\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			// get the services w.r.t id
			$result = Services::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('services.service')));
		}

		$result->update(['status' => !$result->status]);
		$response = ['status' => 1, 'data' => (int)$result->status . '.gif'];
		// return json response
		return json_encode($response);
	}

	/**
	 * Used to load more records and render to view.
	 *
	 * @param Request $request
	 * @return \Response
	 */
	public function servicePaginate(Request $request)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) { //
			return lang('messages.server_error');
		}

		$inputs = $request->all();
		$page = 1;
		if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
			$page = $inputs['page'];
		}

		$perPage = 100;
		if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
			$perPage = $inputs['perpage'];
		}

		$start = ($page - 1) * $perPage;
		if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
			$inputs = array_filter($inputs);
			unset($inputs['_token']);

			$data = (new Services)->getServices($inputs, $start, $perPage);
			$totalServices = (new Services)->totalServices($inputs);
			$total = $totalServices->total;
		} else {

			$data = (new Services)->getServices($inputs, $start, $perPage);
			$totalServices = (new Services)->totalServices($inputs);
			$total = $totalServices->total;
		}

		return view('services.load_data', compact('data', 'total', 'page', 'perPage'));
	}

	/**
	 * Used to update product category active status.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function markedFeatured($id)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			$result = Services::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('services.service')));
		}

		$result->update(['frontpage' => !$result->frontpage]);
		$response = ['status' => 1, 'data' => (int)$result->frontpage . '.png'];
		// return json response
		return json_encode($response);
	}

	/**
	 * Method is used to sort news.
	 *
	 * @param Request $request
	 * @return \Response
	 */
	public function sortingService(Request $request)
	{
		$inputAll = $request->all();
		$order = $inputAll['order'];
		try {
			if (count($order) > 0) {
				$index = count($order);
				foreach ($order as $key => $value) {
					Services::find($value)->update(['_order' => $index--]);
				}
			}
			// return 1 for successfully sorted news.
			echo '1';
		} catch (\Exception $e) {
			// else return 0
			echo '0';
		}
	}

	/**
	 * Used to upload news pictures.
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function pictureUpload($id = null)
	{
		if (!is_null($id)) {
			fileUploader('file', $id);
		} else {
			return lang('messages.server_error');
		}
	}

	/**
	 * Used to delete news pictures.
	 *
	 * @param  int  $id
	 * @param  string 	$fileName
	 * @return \Response
	 */
	public function pictureDelete($id = null, $fileName = null)
	{
		$result = Services::find($id);
		// get the result w.r.t id
		if (!$result) {
			return redirect()->route('services.index')
				->with('error', lang('messages.invalid_id', string_manip(lang('services.service'))));
		}

		try {
			deleteUploadedPicture($id, base64_decode($fileName));
			return json_encode(['status' => 1]);
		} catch (\Exception $exception) {
			return json_encode(['status' => 0]);
		}
	}
}
