<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    /**
     * @return mixed
     */
    public function redirectTo()
    {
        return route('home');
    }

    /**
     * Method is used to render login form for admin
     * @return mixed
     */
    public function login()
    {
        return view('layouts.login');
    }

    /**
     * Method is used to perform login authentication
     *
     * @param Request $request
     * @return $this
     */
    public function doLogin(Request $request)
    {
        $redirectTo = 'admin.login';

        $inputs = $request->only('email', 'password');
        $validation = (new User)->validateAuthentication($inputs);
        if ($validation->fails()) {
            return redirect()->route($redirectTo)->with('error', lang('auth.failed_login'));
        }

        $credentials = [
            'username' => $inputs['email'],
            'password' => $inputs['password'],
            'status' => 1
        ];

        if (\Auth::attempt($credentials)) {
            $user = \Auth::user();
            if ($user->role_id == 1) {
                return redirect()->intended('eadmin/dashboard');
            } else {
                return redirect()->intended('/');
            }
        }
        return redirect()->route($redirectTo)->withInput()->with('error', lang('auth.failed_login'));
    }

    /**
     * Log the party out of the application.
     */
    public function authLogout()
    {
        \Auth::logout();
        \Session::flush();
        return redirect()->route('admin.login');
    }
}
