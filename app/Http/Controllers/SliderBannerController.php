<?php

namespace App\Http\Controllers;

/**
 * :: Slider Banner Controller ::
 * To manage slider banner.
 *
 **/

use App\Http\Controllers\Controller;
use App\SliderBanner;
use Illuminate\Http\Request;

class SliderBannerController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
		return view('slider-banner.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Response
	 */
	public function create()
	{
		return view('slider-banner.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return \Response
	 */
	public function store(Request $request)
	{
		$inputs = $request->all();
		$validator = (new SliderBanner)->validateSliderBanner($inputs);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		if ($inputs['image-pic'] != "") {
			list($width, $height) = getimagesize($_FILES['image-pic']['tmp_name']);
			if ($width <= 1000 || $height <= 300) {
				return validationResponse(false, 207, lang('messages.invalid_banner_size'));
			}
		}

		try {
			\DB::beginTransaction();
			$inputs = $inputs + [
				'status' => (!isset($inputs['status']) ? 0 : 1),
				'login_required' => (!isset($inputs['login_required']) ? 0 : 1),
				'created_by' => authUserId(),
			];
			$id = (new SliderBanner)->store($inputs);

			if ($inputs['image-pic'] != "") {
				$path = ROOT . \Config::get('constants.BANNER_UPLOAD');
				$fileName = fileUploader('image-pic', $id, null, $path);
				$fileImage = [
					'image' => $fileName
				];
				(new SliderBanner)->store($fileImage, $id);
			}
			\DB::commit();
			return validationResponse(true, 201, lang(
				'messages.created',
				lang('slider_banner.slider_banner')
			), route('slider-banner.index'));
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, exception($exception) . lang('messages.server_error'));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function edit($id = null)
	{
		$result = SliderBanner::find($id);
		if (!$result) {
			abort(404);
		}
		return view('slider-banner.edit', compact('result'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return \Response
	 */
	public function update(Request $request, $id = null)
	{
		$result = SliderBanner::find($id);
		if (!$result) {
			return validationResponse(false, 207, lang('messages.invalid_id', lang('slider_banner.slider_banner')));
		}

		$inputs = $request->all();
		$validator = (new SliderBanner)->validateSliderBanner($inputs, $id);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		if ($inputs['image-pic'] != "") {
			list($width, $height) = getimagesize($_FILES['image-pic']['tmp_name']);
			if ($width <= 1000 || $height <= 300) {
				return validationResponse(false, 207, lang('messages.invalid_banner_size'));
			}
		}

		try {
			\DB::beginTransaction();
			$inputs = $inputs + [
				'status' => (!isset($inputs['status']) ? 0 : 1),
				'login_required' => (!isset($inputs['login_required']) ? 0 : 1),
				'updated_by' => authUserId()
			];

			if ($inputs['image-pic'] != "") {
				$path = ROOT . \Config::get('constants.BANNER_UPLOAD');
				$fileName = fileUploader('image-pic', $id, $inputs['old_image'], $path);
				$inputs['image'] = $fileName;
			} else {
				unset($inputs['image-pic']);
			}
			(new SliderBanner)->store($inputs, $id);
			\DB::commit();
			return validationResponse(true, 201, lang(
				'messages.updated',
				lang('slider_banner.slider_banner')
			), route('slider-banner.index'));
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, lang('messages.server_error'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function drop($id)
	{
		return "In Progress";
	}

	/**
	 * Used to update slider_banner active status.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function sliderBannerToggle($id = null)
	{
		if (!\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			// get the slider_banner w.r.t id
			$result = SliderBanner::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('slider_banner.slider_banner')));
		}

		$result->update(['status' => !$result->status]);
		$response = ['status' => 1, 'data' => (int)$result->status . '.gif'];
		// return json response
		return json_encode($response);
	}

	/**
	 * Used to load more records and render to view.
	 *
	 * @param Request $request
	 * @return \Response
	 * @internal param int $pageNumber
	 *
	 */
	public function sliderBannerPaginate(Request $request)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) { //
			return lang('messages.server_error');
		}

		$inputs = $request->all();
		$page = 1;
		if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
			$page = $inputs['page'];
		}

		$perPage = 20;
		if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
			$perPage = $inputs['perpage'];
		}

		$start = ($page - 1) * $perPage;
		if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
			$inputs = array_filter($inputs);
			unset($inputs['_token']);

			$data = (new SliderBanner)->getSliderBanners($inputs, $start, $perPage);
			$totalSliderBanner = (new SliderBanner)->totalSliderBanners($inputs);
			$total = $totalSliderBanner->total;
		} else {

			$data = (new SliderBanner)->getSliderBanners($inputs, $start, $perPage);
			$totalSliderBanner = (new SliderBanner)->totalSliderBanners($inputs);
			$total = $totalSliderBanner->total;
		}
		return view('slider-banner.load_data', compact('data', 'total', 'page', 'perPage'));
	}

	/**
	 * Used to update product category active status.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function markedFeatured($id)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			$result = SliderBanner::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('slider_banner.slider_banner')));
		}

		$result->update(['is_featured' => !$result->is_featured]);
		$response = ['status' => 1, 'data' => (int)$result->is_featured . '.png'];
		// return json response
		return json_encode($response);
	}

	/**
	 * Method is used to sort news.
	 *
	 * @param Request $request
	 * @return \Response
	 */
	public function sortingSliderBanner(Request $request)
	{
		$inputAll = $request->all();
		$order = $inputAll['order'];
		try {
			if (count($order) > 0) {
				$index = count($order);
				foreach ($order as $key => $value) {
					SliderBanner::find($value)->update(['_order' => $index--]);
				}
			}
			// return 1 for successfully sorted news.
			echo '1';
		} catch (\Exception $e) {
			// else return 0
			echo '0';
		}
	}
}
