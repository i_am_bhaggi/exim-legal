<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WebsiteUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class WebsiteUserController extends Controller
{
    public function profile()
    {
        return view('front.my-profile');
    }

    public function updateProfile(Request $request)
    {
        $uId = session()->get('user_id');
        DB::beginTransaction();
        try {
            $user = WebsiteUser::find($uId);
            $user->name = $request['name'];
            $user->email = $request['email'];
            $user->mobile_number = $request['contact'];
            $user->id_number = $request['id_number'];
            $user->address = $request['address'];
            $user->city = $request['city'];
            $user->state = $request['state'];
            $user->country = $request['country'];
            $user->zip_code = $request['zipcode'];
            $user->save();
            DB::commit();
            return redirect()->back()->with('success', 'Profile updated successfully');
        } catch (\Exception $e) {
            DB::rollBack();
            return redirect()->back()->withErrors('Unable to update profile, please try again later');
        }
    }
    private function checkUserExist($email)
    {
        $user = null;
        $user = WebsiteUser::where(['email' => $email])->first();
        return $user;
    }

    public function showLogin()
    {
        return view('front.login');
    }

    public function showRegiter()
    {
        return view('front.register');
    }

    public function login(Request $request)
    {
        $email = $request['email'];
        $password = md5($request['password']);
        $user = $this->checkUserExist($email);
        if (is_null($user)) {
            $response['status'] = 0;
            $response['message']  = "You might have entered an invalid email";
        } else {
            if ($user->user_status == 1) {
                if ($user->password == $password) {
                    $response['status'] = 1;
                    $response['message']  = "Login successful, Redirecting you in 5 seconds";
                    session()->put('user_id', $user->id);
                    session()->put('user_name', $user->name);
                } else {
                    $response['status'] = 0;
                    $response['message']  = "You have entered a wrong password";
                }
            } else {
                $response['status'] = 0;
                $response['message']  = "Your account is temporary inactive, please contact us at the admin.";
            }
        }
        $response['sigin_requested_from'] = "";
        if (session()->has('sigin_requested_from')) {
            $response['sigin_requested_from'] = session()->get('sigin_requested_from');
            session()->forget('sigin_requested_from');
        }
        return response()->json($response);
    }

    public function register(Request $request)
    {
        if ($request['password'] == $request['confirm_password']) {
            $name = $request['name'];
            $email = $request['email'];
            $password = md5($request['password']);
            $contact = $request['contact'];
            $idType = $request['id_type'];
            $idNumber = $request['id_number'];
            $user = $this->checkUserExist($email);
            if (!is_null($user)) {
                $response['status'] = 0;
                $response['message']  = "User with this email address already exists";
            } else {
                DB::beginTransaction();
                try {
                    $newUser = new WebsiteUser;
                    $newUser->name = $name;
                    $newUser->email = $email;
                    $newUser->password = $password;
                    $newUser->mobile_number = $contact;
                    $newUser->id_type = $idType;
                    $newUser->id_number = $idNumber;
                    $newUser->save();
                    $code = md5($newUser->email . $newUser->password);
                    $url = url('/verify-email') . '/' . $code;
                    $msg = "Hey $newUser->name, Thank you for your registration with " . ucwords(Config::get('app.appname')) . " Please click on the below link to verify your email \n $url";
                    // Mail::raw($msg, function ($message) use ($newUser) {
                    //     $message->to($newUser->email, $newUser->name);
                    //     $message->subject('Email Confirmation');
                    // });
                    $response['status'] = 1;
                    $response['message']  = "Account created successfully, Please login once";
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                    $response['status'] = 0;
                    // $response['message'] = $e->getMessage();
                    $response['message']  = "Unable to create the account, please try agin later.";
                }
            }
        } else {
            $response['status'] = 0;
            $response['message']  = "Password and Confirm Password does not match";
        }
        return response()->json($response);
    }

    public function logout()
    {
        session()->forget(['user_name', 'user_id']);
        return redirect()->back();
    }

    public function googleLogin(Request $request)
    {
        $name = $request['name'];
        $email = $request['email'];
        $user = $this->checkUserExist($email);
        if (is_null($user)) {
            DB::beginTransaction();
            try {
                $newUser = new WebsiteUser;
                $newUser->name = $name;
                $newUser->email = $email;
                $newUser->save();
                DB::commit();
                session()->put('user_id', $newUser->id);
                session()->put('user_name', $newUser->name);
                if (session()->has('sigin_requested_from')) {
                    $sigin_requested_from = session()->get('sigin_requested_from');
                    session()->forget('sigin_requested_from');
                    return redirect($sigin_requested_from);
                }
                return redirect('/');
            } catch (\Exception $e) {
                return redirect('/sign-in')->with('error', 'Unable to login using Google, Please try again');
                DB::rollBack();
            }
        } else {
            session()->put('user_id', $user->id);
            session()->put('user_name', $user->name);
            if (session()->has('sigin_requested_from')) {
                $sigin_requested_from = session()->get('sigin_requested_from');
                session()->forget('sigin_requested_from');
                return redirect($sigin_requested_from);
            }
            return redirect('/');
        }
    }

    public function sendResetLinkEmail(Request $request)
    {
        $user = $this->checkUserExist($request['email']);
        if (is_null($user)) {
            return redirect()->back()->withErrors(['email' => 'You have entered an invalid email']);
        } else {
            $tempPass = md5(Str::random(15));
            $user->password = $tempPass;
            $user->save();
            $actionUrl = url('/password/reset') . "/$tempPass/$user->email";
            $data = [
                'user' => $user->toArray(),
                'actionUrl' => $actionUrl
            ];
            try {
                Mail::send('emails.forgot_mail', $data, function ($message) use ($data) {
                    $message->from('info@kerig.com', 'Kerig Krafts');
                    $message->to($data['user']['email'], $data['user']['name']);
                    $message->subject('Forgot Password');
                });
                return redirect()->back()->with('status', 'Reset password link sent to your email.');
            } catch (\Exception $e) {
                return redirect()->back()->withErrors([$e->getMessage()]);
            }
        }
    }

    public function showResetPasswordPage($temp_pass, $email)
    {
        $user = $this->checkUserExist($email);
        if (is_null($user)) {
            return view('front.reset-password')->withErrors(['Invalid email provided']);
        } else {
            if ($user->password == $temp_pass) {
                $data = [
                    'email' => $user->email
                ];
                return view('front.reset-password')->with($data);
            } else {
                return view('front.reset-password')->withErrors(['The reset password key does not match.']);
            }
        }
    }

    public function changePassword(Request $request)
    {
        $password = $request['password'];
        $password_confirmation = $request['password_confirmation'];
        $email = $request['email'] ?? "";
        $userId = session('user_id') ?? "";
        if ($email != "") {
            // for forgot change password
            $user = $this->checkUserExist($email);
            if (is_null($user)) {
                return redirect()->back()->withErrors('Invalid email provided');
            } else {
                if ($password == $password_confirmation) {
                    DB::beginTransaction();
                    try {
                        $user->password = md5($password);
                        $user->save();
                        DB::commit();
                        return redirect()->back()->with(['status' => __('messages.password_updated')]);
                    } catch (\Exception $e) {
                        DB::rollback();
                        return redirect()->back()->withErrors([__('messages.email_address_not_exists')]);
                    }
                } else {
                    return redirect()->back()->withErrors([__('messages.password_confirm_not_match')]);
                }
            }
        } elseif ($userId != "") {
            // for profile change password
        } else {
            return redirect()->back();
        }
    }
}
