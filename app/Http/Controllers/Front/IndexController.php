<?php

namespace App\Http\Controllers\Front;

use App\Appointments;
use App\Blogs;
use App\Pages;
use App\Services;
use App\SliderBanner;
use App\Subscriber;
use App\Testimonial;
use Illuminate\Http\Request;
use App\ContactUs;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use App\DownloadForm;
use App\OurTeam;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class IndexController extends Controller
{
    private $thisData = array();

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = (new Services)->getFilterServices(['frontpage' => 1]);
        $testimonials = (new Testimonial)->getFilterTestimonial(['frontpage' => 1]);
        $banners = (new SliderBanner)->getSliderBanners(['active' => 1], 0, 20);
        $about = (new Pages)->getPageDetail(['id' => 1]);
        //dd($services->toArray());
        return view('front.index', compact('services', 'testimonials', 'banners', 'about'));
    }

    /**
     * Display Terms Of Use.
     *
     * @param null $name
     * @param null $id
     * @return \Response
     */
    public function pageDetail($name = null, $id = null)
    {
        if ((int)$id < 1) {
            return redirect()->route('/');
        }
        $detail = (new Pages)->getPageDetail(['id' => $id]);
        if (!$detail) {
            return redirect()->route('/');
        }
        return view('front.page-detail', compact('detail'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function about()
    {
        $about = (new Pages)->getPageDetail(['id' => 1]);
        return view('front.about', compact('about'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function services()
    {
        return view('front.services');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function freeConsultation()
    {
        return view('front.free-consultation');
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function consultationStore(Request $request)
    {
        $inputs = $request->all();
        $inputs = array_filter($inputs);
        if (count($inputs) > 0) {
            try {
                \DB::beginTransaction();

                $email = $inputs['email'];
                $name = $inputs['name'];

                $emailTo = "deepi.kamboj@gmail.com";
                $emailTo = "haryshgupta@gmail.com";

                Mail::send('emails.consultation_form', ['inputs' => $inputs, 'user' => false], function ($m) use ($emailTo, $email, $name, $inputs) {
                    $m->from("websoft.mailing@gmail.com", "Exim Legal Consultants");
                    $m->replyTo($email, $name);
                    $m->to($emailTo, "Exim Legal Consultants")
                        ->subject('New Consultation Query From : ' . $name);
                });

                /*Mail::send('emails.contact_form', ['inputs' => $inputs, 'user' => false], function ($m) use ($emailTo, $email, $name, $inputs) {
                    $m->from("websoft.mailing@gmail.com", "Exim Legal Consultants");
                    $m->replyTo($emailTo, "Exim Legal Consultants");
                    $m->to($email, $name)
                        ->subject('Your contact query has been received, we will contact you soon.');
                });*/
                \DB::commit();
                return validationResponse(true, 200, "Thanks for contacting us, we will contact you soon.");
            } catch (\Exception $e) {
                \DB::rollBack();
                return validationResponse(false, 201, " Opps! There is some server error.");
            }
        }
        return validationResponse(false, 201, "Opps! There is some error, While sending contact query.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contact()
    {
        return view('front.contact');
    }

    public function ourTeam()
    {
        return view('front.our-team');
    }

    /**
     * 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadform()
    {
        $forms = DownloadForm::where('status', 1)->get();
        $data = compact('forms');
        return view('front.download-forms')->with($data);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function enquiry()
    {
        return view('front.enquiry');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function testimonial()
    {
        return view('front.testmonial');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function contactStore(Request $request)
    {
        $messages = [
            'g-recaptcha-response.required' => 'You must check the reCAPTCHA.',
            'g-recaptcha-response.captcha' => 'Captcha error! try again later or contact site admin.',
        ];

        $validator = Validator::make($request->all(), [
            'g-recaptcha-response' => 'required|captcha'
        ], $messages);

        if ($validator->fails()) {
            return validationResponse(false, 200, "You must check the reCAPTCHA.");
        }
        $inputs = $request->all();
        $inputs = array_filter($inputs);
        if (count($inputs) > 0) {
            try {
                \DB::beginTransaction();

                $email = $inputs['email'];
                $name = $inputs['name'];

                $contact = new ContactUs;
                $contact->name = $name;
                $contact->email = $email;
                $contact->contact = $inputs['mobile'];
                $contact->message = $inputs['message'];;
                $contact->save();

                $emailTo = "deepi.kamboj@gmail.com";
                $emailTo = "haryshgupta@gmail.com";

                // Mail::send('emails.contact_form', ['inputs' => $inputs, 'user' => false], function ($m) use ($emailTo, $email, $name, $inputs) {
                //     $m->from("websoft.mailing@gmail.com", "Exim Legal Consultants");
                //     $m->replyTo($email, $name);
                //     $m->to($emailTo, "Exim Legal Consultants")
                //         ->subject('New Contact Query From : ' . $name);
                // });

                /*Mail::send('emails.contact_form', ['inputs' => $inputs, 'user' => false], function ($m) use ($emailTo, $email, $name, $inputs) {
                    $m->from("websoft.mailing@gmail.com", "Exim Legal Consultants");
                    $m->replyTo($emailTo, "Exim Legal Consultants");
                    $m->to($email, $name)
                        ->subject('Your contact query has been received, we will contact you soon.');
                });*/
                \DB::commit();
                return validationResponse(true, 200, $inputs['success-message']);
            } catch (\Exception $e) {
                \DB::rollBack();
                return validationResponse(false, 200, " Opps! There is some server error.");
            }
        }
        return validationResponse(false, 201, "Opps! There is some error, While sending contact query.");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function termCondition()
    {
        return view('front.term-condition');
    }

    /**
     * @param string $urlSlug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function serviceDetail($urlSlug = '')
    {
        $service = (new Services)->getFilterServices(['url_slug' => $urlSlug]);
        return view('front.service-detail', compact('service'));
    }

    public function teamDetail($id)
    {
        $teamDetail = (new OurTeam)->getOurTeam(['id' => $id]);
        $teamDetail = $teamDetail->first();
        return view('front.team-detail', compact('teamDetail'));
    }

    /**
     * @return mixed
     */
    public function subscribeStore()
    {
        $inputs = Input::all();
        if (count($inputs) > 0) {
            try {
                \DB::beginTransaction();
                $email = $inputs['news_email'];
                $total = (new Subscriber)->getSubscriber(['keyword' => $email, 'total' => 1], 0, 20);
                if ($total > 0) {
                    return validationResponse(false, 201, "Sorry you have already subscribed with us.");
                }
                $save = [
                    'email' => $email,
                    'subscribe_ip' => request()->ip(),
                    'subscribe_date' => convertToLocal(convertToUtc(), 'Y-m-d')
                ];
                (new Subscriber)->store($save);
                \DB::commit();
                return validationResponse(true, 200, "Thanks for subscribing with us, we will contact you soon.");
            } catch (\Exception $e) {
                \DB::rollBack();
                return validationResponse(false, 201, "Opps! There is some server error.");
            }
        }
        return validationResponse(false, 201, "Opps! There is some error, while saving subscription.");
    }
}
