<?php

namespace App\Http\Controllers;

/**
 * :: Pages Controller ::
 * To manage page.
 *
 **/

use App\Http\Controllers\Controller;
use App\Menu;
use App\Pages;
use App\Category;
use Illuminate\Http\Request;

class PageController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
		return view('pages.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Response
	 */
	public function create()
	{
		return view('pages.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return \Response
	 */
	public function store(Request $request)
	{
		$inputs = $request->all();
		$validator = (new Pages)->validatePages($inputs);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		try {
			\DB::beginTransaction();

			$url = str_slug($inputs['byline']);
			$inputs = $inputs + [
				'url_slug'	=> $url,
				'status' 	=> (!isset($inputs['status']) ? 0 : 1),
				'created_by' => authUserId(),
			];
			//dd($inputs);
			(new Pages)->store($inputs);
			\DB::commit();
			$route = route('pages.index');
			$lang = lang('messages.created', lang('pages.page'));
			return validationResponse(true, 201, $lang, $route);
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, lang('messages.server_error'));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 * @param int $id
	 * @return \Response
	 */
	public function edit($id = null)
	{
		$result = Pages::find($id);
		if (!$result) {
			return redirect()->route('pages.index')
				->with('error', lang('messages.invalid_id', string_manip(lang('pages.page'))));
		}

		return view('pages.edit', compact('result'));
	}

	/**
	 * Update the specified resource in storage.
	 * @param Request $request
	 * @param int $id
	 * @return \Response
	 */
	public function update(Request $request, $id = null)
	{
		$result = Pages::find($id);
		if (!$result) {
			return redirect()->route('pages.index')
				->with('error', lang('messages.invalid_id', string_manip(lang('pages.page'))));
		}

		$inputs = $request->all();
		$validator = (new Pages)->validatePages($inputs, $id);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		try {
			\DB::beginTransaction();

			$url = str_slug($inputs['byline']);
			$inputs = $inputs + [
				'url_slug'	=> $url,
				'status' 	=> (!isset($inputs['status']) ? 0 : 1),
				'updated_by' => authUserId(),
			];
			(new Pages)->store($inputs, $id);
			\DB::commit();
			$route = route('pages.index');
			$lang = lang('messages.updated', lang('pages.page'));
			return validationResponse(true, 201, $lang, $route);
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, lang('messages.server_error'));
		}
	}

	/**
	 * Remove the specified resource from storage. 
	 * @param int $id
	 * @return \Response
	 */
	public function drop($id)
	{
		if (!\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			(new Pages)->drop($id);
			$response = ['status' => 1, 'message' => lang('messages.deleted', lang('pages.page'))];
		} catch (\Exception $exception) {
			$response = ['status' => 0, 'message' => lang('messages.server_error')];
		}
		return json_encode($response);
	}

	/**
	 * Used to update page active status. 
	 * @param int $id
	 * @return \Response
	 */
	public function pageToggle($id = null)
	{
		if (!\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			$result = Pages::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('pages.page')));
		}

		$result->update(['status' => !$result->status]);
		$response = ['status' => 1, 'data' => (int)$result->status . '.gif'];
		return json_encode($response);
	}

	/**
	 * Used to load more records and render to view.
	 * @param Request $request
	 * @return \Response
	 * @internal param int $pageNumber
	 */
	public function pagePaginate(Request $request)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) { //
			return lang('messages.server_error');
		}

		$inputs = $request->all();
		$page = 1;
		if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
			$page = $inputs['page'];
		}

		$perPage = 20;
		if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
			$perPage = $inputs['perpage'];
		}

		$start = ($page - 1) * $perPage;
		if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
			$inputs = array_filter($inputs);
			unset($inputs['_token']);

			$data = (new Pages)->getPages($inputs, $start, $perPage);
			$totalPages = (new Pages)->totalPages($inputs);
			$total = $totalPages->total;
		} else {
			$data = (new Pages)->getPages($inputs, $start, $perPage);
			$totalPages = (new Pages)->totalPages();
			$total = $totalPages->total;
		}

		return view('pages.load_data', compact('data', 'total', 'page', 'perPage', 'inputs'));
	}

	/**
	 * Method is used to sort data.
	 *
	 * @return \Response
	 */
	public function sorter()
	{
		$inputAll = \Input::all();
		$order = $inputAll['order'];

		try {
			if (count($order) > 0) {
				$index = count($order);
				//for ($i=1; $i<=$index; $i++) {
				foreach ($order as $key => $value) {
					Pages::where('id', $value)
						->update(['p_order' => $index--]);
				}
			}
			// return 1 for successfully sorted news.
			echo '1';
		} catch (\Exception $e) {
			// else return 0
			echo '0';
		}
	}

	/**
	 * Used to upload news pictures.
	 *
	 * @param  int  $id
	 * @return \Response
	 */
	public function pictureUpload($id = null)
	{
		if (!is_null($id)) {
			dropZoneUploader(1, $id);
		} else {
			return lang('messages.server_error');
		}
	}

	/**
	 * Used to delete news pictures.
	 *
	 * @param  int  $id
	 * @param  string 	$fileName
	 * @return \Response
	 */
	public function pictureDelete($id = null, $fileName = null)
	{
		$result = Pages::find($id);
		// get the result w.r.t id
		if (!$result) {
			return redirect()->route('pages.index')
				->with('error', lang('messages.invalid_id', string_manip(lang('pages.page'))));
		}

		try {
			deleteUploadedPicture($id, base64_decode($fileName));
			return json_encode(['status' => 1]);
		} catch (\Exception $exception) {
			return json_encode(['status' => 0]);
		}
	}
}
