<?php

namespace App\Http\Controllers;

/**
 * :: Slider Banner Controller ::
 * To manage slider banner.
 *
 **/

use App\Http\Controllers\Controller;
use App\DownloadForm;
use Illuminate\Http\Request;

class DownloadFormController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
		return view('download-form.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Response
	 */
	public function create()
	{
		return view('download-form.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return \Response
	 */
	public function store(Request $request)
	{
		$inputs = $request->all();
		$validator = (new DownloadForm)->validateDownloadForm($inputs);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		// if ($inputs['pdf-file'] != "") {
		// 	list($width, $height) = getimagesize($_FILES['pdf-file']['tmp_name']);
		// 	if ($width <= 1000 || $height <= 300) {
		// 		return validationResponse(false, 207, lang('messages.invalid_banner_size'));
		// 	}
		// }

		try {
			\DB::beginTransaction();
			$inputs = $inputs + [
				'status' => (!isset($inputs['status']) ? 0 : 1),
			];
			$id = (new DownloadForm)->store($inputs);

			if ($inputs['pdf-file'] != "") {
				$path = ROOT . \Config::get('constants.FORM_UPLOAD');
				$fileName = fileUploader('pdf-file', $id, null, $path);
				$fileImage = [
					'file' => $fileName
				];
				(new DownloadForm)->store($fileImage, $id);
			}
			\DB::commit();
			return validationResponse(true, 201, lang(
				'messages.created',
				lang('download_form.download_form')
			), route('download-form.index'));
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, exception($exception) . lang('messages.server_error'));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function edit($id = null)
	{
		$result = DownloadForm::find($id);
		if (!$result) {
			abort(404);
		}
		return view('download-form.edit', compact('result'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return \Response
	 */
	public function update(Request $request, $id = null)
	{
		$result = DownloadForm::find($id);
		if (!$result) {
			return validationResponse(false, 207, lang('messages.invalid_id', lang('download_form.slider_banner')));
		}

		$inputs = $request->all();
		$validator = (new DownloadForm)->validateDownloadForm($inputs, $id);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		// if ($inputs['pdf-file'] != "") {
		// 	list($width, $height) = getimagesize($_FILES['pdf-file']['tmp_name']);
		// 	if ($width <= 1000 || $height <= 300) {
		// 		return validationResponse(false, 207, lang('messages.invalid_banner_size'));
		// 	}
		// }

		try {
			\DB::beginTransaction();
			$inputs = $inputs + [
				'status' => (!isset($inputs['status']) ? 0 : 1),
				// 'login_required' => (!isset($inputs['login_required']) ? 0 : 1),
				'updated_by' => authUserId()
			];

			if ($inputs['pdf-file'] != "") {
				$path = ROOT . \Config::get('constants.FORM_UPLOAD');
				$fileName = fileUploader('pdf-file', $id, $inputs['old_image'], $path);
				$inputs['file'] = $fileName;
			} else {
				unset($inputs['pdf-file']);
			}
			(new DownloadForm)->store($inputs, $id);
			\DB::commit();
			return validationResponse(true, 201, lang(
				'messages.updated',
				lang('download_form.slider_banner')
			), route('slider-banner.index'));
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, lang('messages.server_error'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function drop($id)
	{
		return "In Progress";
	}

	/**
	 * Used to update slider_banner active status.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function formToggle($id = null)
	{
		if (!\Request::ajax()) {
			return lang('messages.server_error');
		}
		try {
			// get the subscriber w.r.t id
			$result = DownloadForm::find($id);
			$result->update(['status' => !$result->status]);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('download_form.download_form')));
		}
		$response = ['status' => 1, 'current' => (int)$result->status];
		return json_encode($response);
	}

	/**
	 * Used to load more records and render to view.
	 *
	 * @param Request $request
	 * @return \Response
	 * @internal param int $pageNumber
	 *
	 */
	public function downloadFormPaginate(Request $request)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) { //
			return lang('messages.server_error');
		}

		$inputs = $request->all();
		$page = 1;
		if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
			$page = $inputs['page'];
		}

		$perPage = 20;
		if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
			$perPage = $inputs['perpage'];
		}

		$start = ($page - 1) * $perPage;
		if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
			$inputs = array_filter($inputs);
			unset($inputs['_token']);

			$data = (new DownloadForm)->getDownloadForms($inputs, $start, $perPage);
			$totalSliderBanner = (new DownloadForm)->totalDownloadForms($inputs);
			$total = $totalSliderBanner->total;
		} else {

			$data = (new DownloadForm)->getDownloadForms($inputs, $start, $perPage);
			$totalSliderBanner = (new DownloadForm)->totalDownloadForms($inputs);
			$total = $totalSliderBanner->total;
		}
		return view('download-form.load_data', compact('data', 'total', 'page', 'perPage'));
	}

	/**
	 * Used to update product category active status.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function markedFeatured($id)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			$result = DownloadForm::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('download_form.slider_banner')));
		}

		$result->update(['is_featured' => !$result->is_featured]);
		$response = ['status' => 1, 'data' => (int)$result->is_featured . '.png'];
		// return json response
		return json_encode($response);
	}

	/**
	 * Method is used to sort news.
	 *
	 * @param Request $request
	 * @return \Response
	 */
	public function sortingDownloadForm(Request $request)
	{
		$inputAll = $request->all();
		$order = $inputAll['order'];
		try {
			if (count($order) > 0) {
				$index = count($order);
				foreach ($order as $key => $value) {
					DownloadForm::find($value)->update(['_order' => $index--]);
				}
			}
			// return 1 for successfully sorted news.
			echo '1';
		} catch (\Exception $e) {
			// else return 0
			echo '0';
		}
	}
}
