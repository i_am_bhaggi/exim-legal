<?php

namespace App\Http\Controllers;

/**
 * :: Testimonial Controller ::
 * To manage testimonial.
 *
 **/

use App\Http\Controllers\Controller;
use App\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Response
	 */
	public function index()
	{
		return view('testimonial.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Response
	 */
	public function create()
	{
		return view('testimonial.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return \Response
	 */
	public function store(Request $request)
	{
		$inputs = $request->all();
		$validator = (new Testimonial)->validateTestimonial($inputs);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		// if ($inputs['image'] != "") {
		// 	list($width, $height) = getimagesize($_FILES['image']['tmp_name']);
		// 	if ($width != 300 && $height != 300) {
		// 		return validationResponse(false, 207, lang('messages.invalid_file_size'));
		// 	}
		// }

		$url = str_slug($inputs['name']);

		try {
			\DB::beginTransaction();
			$inputs = $inputs + [
				'url_slug' 		=> $url,
				'status'       	=> (!isset($inputs['status']) ? 0 : 1),
				'created_by' 	=> authUserId()
			];
			$id = (new Testimonial)->store($inputs);
			if ($inputs['image'] != "") {
				$fileName = fileUploader('image', $id);
				$fileImage = [
					'image' => $fileName
				];
				(new Testimonial)->store($fileImage, $id);
			}
			\DB::commit();
			return validationResponse(true, 201, lang(
				'messages.created',
				lang('testimonial.testimonial')
			), route('testimonial.index'));
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, lang('messages.server_error'));
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function edit($id = null)
	{
		$result = Testimonial::find($id);
		if (!$result) {
			abort(404);
		}

		return view('testimonial.edit', compact('result'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param Request $request
	 * @param int $id
	 * @return \Response
	 */
	public function update(Request $request, $id = null)
	{
		$result = Testimonial::find($id);
		if (!$result) {
			return validationResponse(false, 207, lang(
				'messages.invalid_id',
				string_manip(lang('testimonial.testimonial'))
			));
		}

		$inputs = $request->all();
		$validator = (new Testimonial)->validateTestimonial($inputs, $id);
		if ($validator->fails()) {
			return validationResponse(false, 206, "", "", $validator->messages());
		}

		// if ($inputs['image'] != "") {
		// 	list($width, $height) = getimagesize($_FILES['image']['tmp_name']);
		// 	if ($width != 300 && $height != 300) {
		// 		return validationResponse(false, 207, lang('messages.invalid_file_size'));
		// 	}
		// }

		try {
			\DB::beginTransaction();

			$url = str_slug($inputs['name']);

			$inputs = $inputs + [
				'url_slug' 		=> $url,
				'status'       => (!isset($inputs['status']) ? 0 : 1),
				'updated_by' => authUserId()
			];

			if ($inputs['image'] != "") {
				$fileName = fileUploader('image', $id, $inputs['old_image']);
				$inputs = [
					'image' => $fileName
				];
			} else {
				unset($inputs['image']);
			}
			(new Testimonial)->store($inputs, $id);
			\DB::commit();
			return validationResponse(true, 201, lang(
				'messages.updated',
				lang('testimonial.testimonial')
			), route('testimonial.index'));
		} catch (\Exception $exception) {
			\DB::rollBack();
			return validationResponse(false, 207, lang('messages.server_error'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function drop($id)
	{
		return "In Progress";
	}

	/**
	 * Used to update testimonial active status.
	 *
	 * @param int $id
	 * @return \Response
	 */
	public function testimonialToggle($id = null)
	{
		if (!\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			// get the testimonial w.r.t id
			$result = Testimonial::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('testimonial.testimonial')));
		}

		$result->update(['status' => !$result->status]);
		$response = ['status' => 1, 'data' => (int)$result->status . '.gif'];
		// return json response
		return json_encode($response);
	}

	/**
	 * Used to load more records and render to view.
	 *
	 * @param Request $request
	 * @return \Response
	 * @internal param int $pageNumber
	 *
	 */
	public function testimonialPaginate(Request $request)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) { //
			return lang('messages.server_error');
		}

		$inputs = $request->all();
		$page = 1;
		if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
			$page = $inputs['page'];
		}

		$perPage = 20;
		if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
			$perPage = $inputs['perpage'];
		}

		$start = ($page - 1) * $perPage;
		if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
			$inputs = array_filter($inputs);
			unset($inputs['_token']);

			$data = (new Testimonial)->getTestimonial($inputs, $start, $perPage);
			$totalTestimonial = (new Testimonial)->totalTestimonial($inputs);
			$total = $totalTestimonial->total;
		} else {

			$data = (new Testimonial)->getTestimonial($inputs, $start, $perPage);
			$totalTestimonial = (new Testimonial)->totalTestimonial($inputs);
			$total = $totalTestimonial->total;
		}
		return view('testimonial.load_data', compact('data', 'total', 'page', 'perPage'));
	}

	/**
	 * Used to update product category active status.
	 *
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function markedFeatured($id)
	{
		if (!\Request::isMethod('post') && !\Request::ajax()) {
			return lang('messages.server_error');
		}

		try {
			$result = Testimonial::find($id);
		} catch (\Exception $exception) {
			return lang('messages.invalid_id', string_manip(lang('testimonial.testimonial')));
		}

		$result->update(['frontpage' => !$result->frontpage]);
		$response = ['status' => 1, 'data' => (int)$result->frontpage . '.png'];
		// return json response
		return json_encode($response);
	}
}
