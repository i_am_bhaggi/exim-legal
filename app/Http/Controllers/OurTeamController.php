<?php

namespace App\Http\Controllers;

/**
 * :: Testimonial Controller ::
 * To manage important-link.
 *
 **/

use App\Http\Controllers\Controller;
use App\OurTeam;
use Illuminate\Http\Request;

class OurTeamController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Response
     */
    public function index()
    {
        return view('our-team.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Response
     */
    public function create()
    {
        return view('our-team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $validator = (new OurTeam)->validateOurTeam($inputs);
        if ($validator->fails()) {
            return validationResponse(false, 206, "", "", $validator->messages());
        }
        $url = str_slug($inputs['name']);

        try {
            \DB::beginTransaction();
            $inputs = $inputs + [
                'status'           => (!isset($inputs['status']) ? 0 : 1),
                'created_by'     => authUserId()
            ];
            $id = (new OurTeam)->store($inputs);
            if ($inputs['image-pic'] != "") {
                $path = ROOT . \Config::get('constants.TEAM_UPLOAD');
                $fileName = fileUploader('image-pic', $id, null, $path);
                $fileImage = [
                    'image-pic' => $fileName
                ];
                (new OurTeam)->store($fileImage, $id);
            }
            \DB::commit();
            return validationResponse(true, 201, lang(
                'messages.created',
                lang('our-team.our-team')
            ), route('our-team.index'));
        } catch (\Exception $exception) {
            \DB::rollBack();
            // return validationResponse(false, 207, lang('messages.server_error'));
            return validationResponse(false, 207, $exception->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Response
     */
    public function edit($id = null)
    {
        $result = OurTeam::find($id);
        if (!$result) {
            abort(404);
        }

        return view('our-team.edit', compact('result'));
    }


    /**
     * Method is used to sort news.
     *
     * @param Request $request
     * @return \Response
     */
    public function sortingImportantLink(Request $request)
    {
        $inputAll = $request->all();
        $order = $inputAll['order'];
        try {
            if (count($order) > 0) {
                $index = count($order);
                foreach ($order as $key => $value) {
                    OurTeam::find($value)->update(['_order' => $index--]);
                }
            }
            // return 1 for successfully sorted news.
            echo '1';
        } catch (\Exception $e) {
            // else return 0
            echo '0';
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return \Response
     */
    public function update(Request $request, $id = null)
    {
        $result = OurTeam::find($id);
        if (!$result) {
            return validationResponse(false, 207, lang(
                'messages.invalid_id',
                string_manip(lang('our-team.our-team'))
            ));
        }
        $inputs = $request->all();
        $validator = (new OurTeam)->validateOurTeam($inputs, $id);
        if ($validator->fails()) {
            return validationResponse(false, 206, "", "", $validator->messages());
        }
        try {
            \DB::beginTransaction();
            $inputs = $inputs + [
                'status'       => (!isset($inputs['status']) ? 0 : 1),
                'updated_by' => authUserId()
            ];
            if ($inputs['image-pic'] != "") {
                $path = ROOT . \Config::get('constants.BANNER_UPLOAD');
                $fileName = fileUploader('image-pic', $id, $inputs['old_image'], $path);
                $inputs['image'] = $fileName;
            } else {
                unset($inputs['image-pic']);
            }
            (new OurTeam)->store($inputs, $id);
            \DB::commit();
            return validationResponse(true, 201, lang(
                'messages.updated',
                lang('our-team.our-team')
            ), route('our-team.index'));
        } catch (\Exception $exception) {
            \DB::rollBack();
            return validationResponse(false, 207, lang('messages.server_error'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Response
     */
    public function drop($id)
    {
        return "In Progress";
    }

    /**
     * Used to update testimonial active status.
     *
     * @param int $id
     * @return \Response
     */
    public function ourTeamToggle($id = null)
    {
        if (!\Request::ajax()) {
            return lang('messages.server_error');
        }
        try {
            // get the subscriber w.r.t id
            $result = OurTeam::find($id);
            $result->update(['status' => !$result->status]);
        } catch (\Exception $exception) {
            return lang('messages.invalid_id', string_manip(lang('our_team.our_team')));
        }
        $response = ['status' => 1, 'current' => (int)$result->status];
        return json_encode($response);
    }

    /**
     * Used to load more records and render to view.
     *
     * @param Request $request
     * @return \Response
     * @internal param int $pageNumber
     *
     */
    public function ourTeamPaginate(Request $request)
    {
        if (!\Request::isMethod('post') && !\Request::ajax()) { //
            return lang('messages.server_error');
        }

        $inputs = $request->all();
        $page = 1;
        if (isset($inputs['page']) && (int)$inputs['page'] > 0) {
            $page = $inputs['page'];
        }

        $perPage = 20;
        if (isset($inputs['perpage']) && (int)$inputs['perpage'] > 0) {
            $perPage = $inputs['perpage'];
        }

        $start = ($page - 1) * $perPage;
        if (isset($inputs['form-search']) && $inputs['form-search'] != '') {
            $inputs = array_filter($inputs);
            unset($inputs['_token']);

            $data = (new OurTeam)->getOurTeam($inputs, $start, $perPage);
            $totalOurTeam = (new OurTeam)->totalOurTeam($inputs);
            $total = $totalOurTeam->total;
        } else {

            $data = (new OurTeam)->getOurTeam($inputs, $start, $perPage);
            $totalOurTeam = (new OurTeam)->totalOurTeam($inputs);
            $total = $totalOurTeam->total;
        }
        return view('our-team.load_data', compact('data', 'total', 'page', 'perPage'));
    }

    /**
     * Used to update product category active status.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function markedFeatured($id)
    {
        if (!\Request::isMethod('post') && !\Request::ajax()) {
            return lang('messages.server_error');
        }

        try {
            $result = OurTeam::find($id);
        } catch (\Exception $exception) {
            return lang('messages.invalid_id', string_manip(lang('our-team.our-team')));
        }

        $result->update(['frontpage' => !$result->frontpage]);
        $response = ['status' => 1, 'data' => (int)$result->frontpage . '.png'];
        // return json response
        return json_encode($response);
    }
}
