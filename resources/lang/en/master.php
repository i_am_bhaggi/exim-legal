<?php

/**
 * :: Master Language File :: 
 * To manage all language phrases used in master layout.
 *
 **/

return [

	'home'					=> 'Front Site',
	'dashboard'				=> 'Dashboard',
	'system_admin'			=> 'System Admin',
	'settings'            	=> 'Settings',
	'list'					=> 'List All',
	'users'            		=> 'Users',
	'roles'            		=> 'User Roles',
	'products' 				=> 'Products',
	'manage_password'		=> 'Change Password',
	'profile'				=> 'Manage Profile',
	'setup'					=> 'Setup',
	'masters'				=> 'Masters',
	'transaction'			=> 'Transactions',
	'report'				=> 'Report',
	'menu'					=> 'Menu',

	'f_home'				=> 'Home',
	'about'					=> 'About',
	'about_us'				=> 'About Us',
	'our_service'			=> 'Our Services',
	'book_appointment'		=> 'Book An Appointment',
	'contact'				=> 'Contact',
	'contact_us'			=> 'Contact Us',

	'advanced_care_physical' => 'Advanced Care Physical Therapy'
];
