<?php

/**
 * :: Download Form Language File :: 
 * To manage download_form related language phrases.
 *
 **/

return [

    'download_form_detail'          => 'Download Form Detail',
    'download_form'                    => 'Download Form',
    'download_forms'                => 'Download Forms',
    'download_form_status'            => 'Download Form Status',
    'download_forms_list'            => 'Download Forms List',
    'title'                            => 'Title',
    'file'                            => 'PDF File',
    'additional_detail'                => 'Additional Detail'
];
