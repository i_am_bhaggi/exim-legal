<?php

/**
 * :: important Language File :: 
 * To manage product category related language phrases.
 *
 **/

return [

    'our_team_details'          => 'Our team Detail',
    'our_team'                 => 'Our team',
    'our_teams'                => 'Our teams',
    'our_team_status'          => 'Our team  Status',
    'title'                     => 'Title',
    'name'                      => 'Name',
    'frontpage'                 => 'Front Page',
    'update'                    => 'Update our team',
    'show_menu'                 => 'Show In Menu',
];
