<?php

/**
 * :: Testimonial Language File :: 
 * To manage product category related language phrases.
 *
 **/

return [

	'testimonial_detail'		=> 'Testimonial Detail',
	'testimonial'				=> 'Testimonial',
	'testimonials'				=> 'Testimonials',
	'testimonial_status'		=> 'Testimonial Status',
	'name'					=> 'User(s) Name',
	'image'					=> 'Image',
	'frontpage'				=> 'Front Page',
	'show_menu'				=> 'Show In Menu',
	'keywords'				=> 'Meta Keywords',
	'description'			=> 'Meta Description',
	'short_intro'			=> 'User Feedback',
	'full_detail'			=> 'Full Detail',

];
