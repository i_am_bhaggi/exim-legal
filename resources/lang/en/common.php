<?php

/**
 * :: Common Language File :: 
 * To manage all kind of common language phrases.
 *
 **/

return [

	'*'					=> ' (*)',
	'id'				=> '#ID',
	'name'            	=> 'Name',
	'code'				=> 'Code',
	'description'		=> 'Description',
	'phone'				=> 'Phone',
	'approve'			=> 'Approve Now',
	'email'				=> 'Email',
	'code_abbreviation' => 'Code Abbreviation',
	'symbol'			=> 'Symbol',
	'value'				=> 'Value',
	'back'				=> 'Back',
	'active'            => 'Active',
	'optional'          => 'Optional',
	'inactive'          => 'In Active',
	'status'			=> 'Status',
	'action'			=> 'Action',
	'all'            	=> 'All',
	'save'				=> 'Save',
	'add'				=> 'Add',
	'add_price'			=> 'Add Price',
	'save_price'		=> 'Save Price',
	'update'			=> 'Update',
	'upload'			=> 'Upload',
	'create_heading'	=> 'Create :attribute',
	'edit_heading'		=> 'Edit :attribute',
	'list_heading'		=> 'List :attribute',
	'view_heading'		=> 'View :attribute',
	'show_heading'		=> 'Show detail :attribute',
	'per_page'			=> 'Per page',
	'other'             => 'Other Detail',
	'date_format'		=> 'Date format: (DD-MM-YYYY)',
	'jump_to'			=> 'Jump to ',
	'pages'				=> 'Pages',
	'total_records'		=> 'Total :attribute records found',
	'first'				=> 'First',
	'last'				=> 'Last',
	'oops_404'			=> '404!',
	'oops_401'			=> '401! Unauthorized Access',
	'something_wrong'	=> 'Oops! Something went wrong',
	'coming_soon'		=> 'Coming Soon!',
	'worry'				=> 'Don\'t worry! We\'re working on it. Coming soon!',
	'permission_denied'	=> 'Sorry! You don\'t have permission to access.',
	'go_back'			=> 'Go Back',
	'change_password'	=> 'Change Password',
	'old_password'		=> 'Old Password',
	'new_password'		=> 'New Password',
	'confirm_password'	=> 'Confirm Password',
	'submit'			=> 'Submit',
	'add_more'			=> 'Add More',
	'create_menu'		=> 'Create Menu',
	'list_menu'			=> 'List Menu',
	'edit_menu'			=> 'Edit Menu',
	'seo_detail'		=> 'SEO Detail',
	'choose_image'		=> 'Choose Image',
	'message'           => 'Message',
	'date'				=> 'Date'
];
