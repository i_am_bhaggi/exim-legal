<?php 
/**
 * :: Appointment Language File :: 
 * To manage product category related language phrases.
 *
 **/

return [

	'appointments_detail'		=> 'Appointment Detail',
	'appointment'				=> 'Appointment',
	'appointments'				=> 'Appointments',
	'appointments_status'		=> 'Appointment Status',
	'name'						=> 'Name',
	'appointment_type'			=> 'Appointment Type',
	'appointment_date'			=> 'Appointment Date',
	'message'					=> 'Message',
	'number'					=> '#Number',
	'apt_no'					=> 'APT-'

];