<?php 
/**
 * :: User Language File :: 
 * To manage users related language phrases.
 *
 **/

return [

	'user_detail'	=> 'User Detail',
	'personal_detail'	=> 'Personal Detail',
	'account_detail'	=> 'Account Detail',
	'address_detail'	=> 'Address Detail',
	'user'			=> 'User',
	'users'			=> 'Users',
	'user_status'	=> 'User Status',
	'users_list'	=> 'Users List',
	'username'		=> 'Username',
	'email'			=> 'Email',
	'password'		=> 'Password',
	'role'			=> 'Role',
	'company'		=> 'Company',
	'name'			=> 'Name',
	'gender'		=> 'Gender',
	'dob'			=> 'DOB',
	'mobile'		=> 'Mobile',
	'phone'			=> 'Phone',
	'address'		=> 'Address',
	'city'			=> 'City',
	'state'			=> 'State',
	'country'		=> 'Country',
	'pin_code'		=> 'Pincode',
	'gst_number'	=> 'GST Number',
	'discount'		=> 'Discount',
	'token_updated'		=> 'Device token successfully updated.',
	'push_sent'			=> 'Push sent successfully.',
	'invalid_request'		=> 'Invalid request, please check request params.',	
];