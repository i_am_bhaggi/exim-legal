<?php

/**
 * :: Subscriber Language File :: 
 * To manage product category related language phrases.
 *
 **/

return [

    'contact_detail'        => 'Contact Detail',
    'contact'                => 'Contact Us',
    'contact'                => 'Contact Us',
    'contact_status'        => 'Contact Status',
    'name'                        => 'Name',
    'contact_type'            => 'Contact Type',
    'contact_date'            => 'Contact Date',
    'message'                    => 'Message',
    'number'                    => '#Number',
    'apt_no'                    => 'APT-'

];
