<?php 
/**
 * :: Service Language File :: 
 * To manage product category related language phrases.
 *
 **/

return [

	'services_detail'		=> 'Service Detail',
	'service'				=> 'Service',
	'services'				=> 'Services',
	'services_status'		=> 'Service Status',
	'name'					=> 'Service Name',
	'image'					=> 'Image',
	'frontpage'				=> 'Front Page',
	'show_menu'				=> 'Show In Menu',
	'keywords'				=> 'Meta Keywords',
	'meta_title'			=> 'Page Title',
	'description'			=> 'Meta Description',
	'short_intro'			=> 'Short Intro',
	'full_detail'			=> 'Full Detail',
	'sub_name'				=> 'Inner Text',

];