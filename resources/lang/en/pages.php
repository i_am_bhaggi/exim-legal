<?php 
/**
 * :: pages Language File :: 
 * To manage pages related language phrases.
 *
 **/

return [

	'pages_detail'		=> 'CMS Page Detail',
	'page'				=> 'CMS Page',
	'pages'				=> 'CMS Pages',
	'pages_status'		=> 'CMS Page Status',
	'pages_list'		=> 'CMS Pages List',
    'page_in_use'       => 'Page already in use'
];