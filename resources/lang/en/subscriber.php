<?php

/**
 * :: Subscriber Language File :: 
 * To manage product category related language phrases.
 *
 **/

return [

	'subscribers_detail'		=> 'Subscriber Detail',
	'subscriber'				=> 'Subscriber',
	'subscribers'				=> 'Subscribers',
	'subscribers_status'		=> 'Subscriber Status',
	'name'						=> 'Name',
	'subscriber_type'			=> 'Subscriber Type',
	'subscriber_date'			=> 'Subscriber Date',
	'message'					=> 'Message',
	'number'					=> '#Number',
	'apt_no'					=> 'APT-'

];
