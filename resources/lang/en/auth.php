<?php
/**
 * :: Auth Language File ::
 * To manage login authorization related language phrases.
 *
 **/

return [

    'success_login'     => 'Welcome to paytrack admin panel',
    'failed_login'      => 'Invalid login credentials',
    'logout'            => 'Logout successfully',
    '404_error'         => 'Invalid request, Page not found',
    'auth_required'		=> 'Invalid request, Authorization required',
    'session_cart_expired' => 'Your session cart is expired, Please relogin again!',
];