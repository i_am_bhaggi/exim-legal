<?php

/**
 * :: important Language File :: 
 * To manage product category related language phrases.
 *
 **/

return [

    'important_detail'          => 'Important Link Detail',
    'important'                 => 'Important Link',
    'importants'                => 'Important Links',
    'important_status'          => 'Important Links Status',
    'title'                     => 'Title',
    'link'                      => 'Link',
    'frontpage'                 => 'Front Page',
    'update'                    => 'Important Link',
    'show_menu'                 => 'Show In Menu',
];
