<?php

/**
 * :: Slider Banner Language File :: 
 * To manage slider_banner related language phrases.
 *
 **/

return [

	'slider_banner_detail'          => 'Slider Banner Detail',
	'slider_banner'					=> 'Slider Banner',
	'slider_banners'				=> 'Slider Banners',
	'slider_banner_status'			=> 'Slider Banner Status',
	'slider_banners_list'			=> 'Slider Banners List',
	'text1'							=> 'Text1',
	'text2'							=> 'Text2',
	'image'							=> 'Image',
	'additional_detail'				=> 'Additional Detail'
];
