@extends('layouts.admin')
@section('content')
<!-- <link href="assets/css/jquery-checktree.css" rel="stylesheet" type="text/css"> -->


<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-default pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10">{!! lang('common.create_heading', lang('user.user')) !!}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    
    {{-- for message rendering --}}
    @include('layouts.messages')
    <div class="row">
        {!! Form::open(array('method' => 'POST', 'route' => array('user.store'), 'id' => 'ajaxSave', 'class' => 'form-horizontal')) !!}
        <div class="col-md-12">
            <div class="panel panel-primary boot-panel">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    {!! lang('user.user_detail') !!}
                </div>
                <div class="panel-body">
                    <div class="col-md-6 padding0">
                        <h2 class="page-header margin0 marginbottom15">
                            <i class="fa fa-user"></i>  &nbsp;
                            {!! lang('user.personal_detail') !!}
                        </h2>

                        <div class="form-group">
                            {!! Form::label('name', lang('common.name'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::text('name', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('mobile', lang('user.mobile'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::text('mobile', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('phone', lang('user.phone'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::text('phone', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('gender', lang('user.gender'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::radio('gender', 1, true) !!} Male
                                {!! Form::radio('gender', 2) !!} Female
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('dob', lang('user.dob'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::text('dob', null, array('class' => 'form-control date-past')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('role', lang('user.role'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::select('role', $role , null, array('class' => 'form-control role select2 padding0')) !!}
                            </div>
                        </div>

                        <div class="role-manage hidden">
                            <div class="form-group">
                                {!! Form::label('discount', lang('user.discount'), array('class' => 'col-sm-3 control-label')) !!}
                                <div class="col-sm-8">
                                    {!! Form::text('discount', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                {!! Form::label('gst_number', lang('user.gst_number'), array('class' => 'col-sm-3 control-label')) !!}
                                <div class="col-sm-8">
                                    {!! Form::text('gst_number', null, array('class' => 'form-control')) !!}
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            {!! Form::label('status', lang('common.active') . '&nbsp;', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                <label class="checkbox col-sm-4">
                                    {!! Form::checkbox('status', '1', true) !!}
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 paddingright0">
                        <h2 class="page-header margin0 marginbottom15">
                            <i class="fa fa-lock"></i>  &nbsp;
                            {!! lang('user.account_detail') !!}
                        </h2>

                        <div class="form-group">
                            {!! Form::label('email', lang('user.email'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::text('email', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', lang('user.password'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::password('password', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>


                    <div class="col-md-6 paddingright0">
                        <h2 class="page-header margin0 marginbottom15">
                            <i class="fa fa-map-marker"></i>  &nbsp;
                            {!! lang('user.address_detail') !!}
                        </h2>

                        <div class="form-group">
                            {!! Form::label('address', lang('user.address'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::textarea('address', null, array('class' => 'form-control', 'size' => '5x3')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('country', lang('user.state'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-4 hidden padding0 paddingleft10">
                                {!! Form::text('country', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="col-sm-4 paddingright5">
                                {!! Form::select('state', $state, null, array('class' => 'form-control padding0 select2', 'id' => 'state')) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            {!! Form::label('city', lang('user.city'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-4 padding0 paddingleft10">
                                {!! Form::text('city', null, array('class' => 'form-control')) !!}
                            </div>

                            <div class="col-sm-4 paddingright5">
                                {!! Form::text('pin_code', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 padding0 margintop10 clearfix text-center">
                    <div class="form-group">
                        {!! Form::submit(lang('common.save'), array('class' => 'btn btn-lg btn-primary')) !!}
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
<!-- /#page-wrapper -->
<script>
    $('.role').on('change', function() {
        value = $(this).val();

        if(value == 3 || value == 2) {
            $('.role-manage').removeClass('hidden');
        } else {
            $('.role-manage').addClass('hidden');
        }
    });
</script>
@stop
