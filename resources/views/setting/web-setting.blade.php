@extends('layouts.admin')
@section('content')
    <div id="page-wrapper">
        <!-- start: PAGE HEADER -->
        <div class="row">
            <div class="col-lg-12">
                <a class="btn btn-sm btn-danger pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
                <h1 class="page-header margintop10">{!! lang('setting.web_setting') !!}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- end: PAGE HEADER -->
        <!-- start: PAGE CONTENT -->

        {{-- for message rendering --}}
        @include('layouts.messages')
        <div class="row">
            <div class="col-md-12">
            {!! Form::model($setting, array('route' => array('setting.web-setting', $setting->id), 'method' => 'PATCH', 'id' => 'ajaxSave', 'class' => 'form-horizontal')) !!}
            <div class="col-md-6 paddingleft0">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Web Site Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-11 marginleft20">
                                {!! Form::text('site_name', null, array('class' => 'form-control', 'placeholder' => 'Site Name', 'id' => 'site_name')) !!}
                            </div>

                            <div class="col-sm-11 margintop20 marginleft20">
                                {!! Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'Email', 'id' => 'email')) !!}
                            </div>

                            <div class="col-sm-11 margintop20 marginleft20">
                                {!! Form::text('mobile', null, array('class' => 'form-control', 'placeholder' => 'Mobile', 'id' => 'mobile')) !!}
                            </div>

                            <div class="col-sm-11 margintop20 marginleft20">
                                {!! Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Phone', 'id' => 'phone')) !!}
                            </div>

                            <div class="col-sm-11 margintop20 marginleft20">
                                {!! Form::text('website', null, array('class' => 'form-control', 'placeholder' => 'Website', 'id' => 'website')) !!}
                            </div>
                        </div>

                        <div class="col-sm-12 margintop20 text-center">
                            <div class="form-group">
                                {!! Form::submit('Save', array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>

            <div class="col-md-6 paddingleft0">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                         SEO Meta Detail
                    </div>
                    <div class="panel-body">
                        <div class="row marginleft10">

                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::textarea('meta_title', null, array('class' => 'form-control','placeholder' => 'Home Page Title',  'size' => '10x2', 'id' => 'meta_title')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::textarea('meta_keyword', null, array('class' => 'form-control','placeholder' => 'Meta Keywords',  'size' => '10x2', 'id' => 'meta_keyword')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::textarea('meta_description', null, array('class' => 'form-control','placeholder' => 'Meta Description',  'size' => '10x3', 'id' => 'meta_description')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::text('facebook_url', null, array('class' => 'form-control', 'placeholder' => 'Facebook', 'id' => 'facebook_url')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::text('instagram_url', null, array('class' => 'form-control', 'placeholder' => 'Linked In', 'id' => 'instagram_url')) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::text('twitter_url', null, array('class' => 'form-control', 'placeholder' => 'Twitter', 'id' => 'twitter_url')) !!}
                                </div>
                            </div>
                            <div class="form-group hidden">
                                <div class="col-sm-11">
                                    {!! Form::text('googleplus_url', null, array('class' => 'form-control', 'placeholder' => 'Google+', 'id' => 'googleplus_url')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            <div class="col-md-6 paddingleft0">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Address / Business Hour(s) Timing
                    </div>
                    <div class="panel-body">

                        <div class="form-group">
                            <div class="col-sm-11">
                                {!! Form::textarea('address', null, array('class' => 'form-control','placeholder' => 'Address',  'size' => '10x5', 'id' => 'address')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-11">
                                {!! Form::textarea('all_rights_reserved', null, array('class' => 'form-control', 'size' => '10x3')) !!}
                            </div>
                        </div>

                        <?php
                            $otherText = json_decode($setting['other_text'], true);
                            $timing = $otherText['timings'];
                            $index = 1;
                        ?>
                        @if(count($timing) > 0)
                            @foreach($timing as $weekday => $time)
                                <div class="row marginbottom10">
                                    <div class="col-sm-5">
                                        {!! Form::text('weekday[' . $index . ']', string_manip($weekday, 'UC'), array('class' => 'form-control', 'placeholder' => 'Weekday', 'id' => 'weekday' . $index)) !!}
                                    </div>

                                    <div class="col-sm-6">
                                        {!! Form::text('timing[' . $index . ']', $time, array('class' => 'form-control', 'placeholder' => 'Timing', 'id' => 'timming' . $index++)) !!}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            @for($i=1; $i<=7; $i++)
                                <div class="row marginbottom10">
                                    <div class="col-sm-5">
                                        {!! Form::text('weekday[' . $i . ']', null, array('class' => 'form-control', 'placeholder' => 'Weekday', 'id' => 'weekday' . $i)) !!}
                                    </div>

                                    <div class="col-sm-6">
                                        {!! Form::text('timing[' . $i . ']', null, array('class' => 'form-control', 'placeholder' => 'Timing', 'id' => 'timming' . $i)) !!}
                                    </div>
                                </div>
                            @endfor
                        @endif

                        <div class="col-sm-12 margintop20 text-center">
                            <div class="form-group">
                                {!! Form::submit('Save', array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            {!! Form::close() !!}
        </div>
            <!-- /#page-wrapper -->
        </div>
    </div>
@stop