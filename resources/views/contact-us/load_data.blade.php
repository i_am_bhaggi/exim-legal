<thead>
<tr>
    <th width="5%" class="text-center">{!! lang('common.id') !!}</th>
    <th>{!! lang('common.name') !!}</th>
    <th>{!! lang('common.email') !!}</th>
    <th>{!! lang('common.phone') !!}</th>
    <th>{!! lang('common.message') !!}</th>
    <th>{!! lang('common.date') !!}</th>
    <th>{!! lang('common.status') !!}</th>
    <th class="text-center hidden">{!! lang('common.action') !!}</th>
</tr>
</thead>
<tbody>
<?php $index = 1; ?>
@foreach($data as $detail)
    <tr id="order_{{ $detail->id }}">
        <td class="text-center">{!! pageIndex($index++, $page, $perPage) !!}</td>
        <td>
            {!! $detail->name !!}
        </td>
        <td>
            {!! $detail->email !!}
        </td>
        <td>
            {!! $detail->contact !!}
        </td>
        <td>
            {!! $detail->message !!}
        </td>
        <td>
            {!! date('d/M/Y h:i:s a',strtotime($detail->created_at)) !!}
        </td>
        <td class="text-center">
            <a onclick="changeStatus(this)" data-message="{!! lang('messages.change_status') !!}" data-route="{!! route('contact.toggle', $detail->id) !!}">
                @if ($detail->status)
                    <button class="btn btn-success">Replied</button>
                @else
                    <button class="btn btn-danger">Pending</button>
                @endif
            </a>
        </td>
    </tr>
@endforeach
@if (count($data) < 1)
    <tr>
        <td class="text-center" colspan="8"> {!! lang('messages.no_data_found') !!} </td>
    </tr>
@else
    <tr class="margintop10">
        <td colspan="8">
            {!! paginationControls($page, $total, $perPage) !!}
        </td>
    </tr>
@endif
</tbody>

