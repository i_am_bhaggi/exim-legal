@extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header margintop10"> {!! lang('contact.contact') !!} </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

	{{-- for message rendering --}}
    @include('layouts.messages')

    <div class="row">
    	<div class="col-md-12">
		<!-- start: BASIC TABLE PANEL -->
		<div class="panel panel-primary boot-panel" style="position: static;">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i> &nbsp;
				{!! lang('common.list_heading', lang('contact.contact')) !!}
			</div>
			<div class="panel-body">
				<div class="col-md-3 text-right pull-right padding0 marginbottom10">
					{!! lang('common.per_page') !!}: {!! Form::select('name', ['20' => '20', '40' => '40', '100' => '100', '200' => '200', '300' => '300'], '20', ['id' => 'per-page']) !!}
				</div>
				<div class="col-md-3 padding0 marginbottom10">
					{!! Form::hidden('page', 'search') !!}
					{!! Form::hidden('_token', csrf_token()) !!}
					{!! Form::text('name', null, array('class' => 'form-control live-search', 'placeholder' => 'Search ' . string_manip(lang('contact.contact'), 'L') .' by email')) !!}
				</div>
				<table id="paginate-load" data-route="{{ route('contact.paginate') }}" class="table table-hover clearfix margin0 col-md-12 padding0">
				</table>
			</div>
		</div>
		<!-- end: BASIC TABLE PANEL -->
		</div>
	</div>	
</div>
<!-- /#page-wrapper -->
@push('script')
<script>
    function changeStatus(elem){
        var goToUrl = $(elem).data('route');
		$.ajax({
			url: goToUrl,
			success:function(response){
				response = JSON.parse(response);
				if(response.status == 1){
					if(response.current == 1){
						$(elem).children("button").text("Replied").addClass("btn-success").removeClass("btn-danger");
					}else{
						$(elem).children("button").text("Pending").addClass("btn-danger").removeClass("btn-success");
					}
				}else{
					alert("Unable to change the status");
				}
			}
		});
    };
</script>
@endpush
@stop

