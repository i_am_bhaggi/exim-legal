@extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10">{!! lang('common.create_heading', lang('testimonial.testimonial')) !!}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    
    {{-- for message rendering --}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-12 padding0">
            {!! Form::open(array('method' => 'POST', 'route' => array('testimonial.store'), 'id' => 'ajaxSave', 'files' => true, 'class' => 'form-horizontal')) !!}
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('testimonial.testimonial_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('name', lang('testimonial.name'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('testimonial', lang('testimonial.image'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::file('image', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('short_intro', lang('testimonial.short_intro'), array('class' => 'col-sm-3 control-label')) !!}

                            <div class="col-sm-9">
                                {!! Form::textarea('short_intro', null, array('class' => 'form-control', 'size' => '5x6')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('status', lang('common.active') . '&nbsp;', array('class' => 'col-sm-2 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                 {!! Form::checkbox('status', '1', true) !!}
                            </div>

                            {!! Form::label('frontpage', lang('testimonial.frontpage') . '&nbsp;', array('class' => 'col-sm-2 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                {!! Form::checkbox('frontpage', '1', false) !!}
                            </div>
                        </div>

                        <div class="col-sm-12 margintop20 clearfix text-center">
                            <div class="form-group">
                                {!! Form::submit(lang('common.save'), array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('testimonial.testimonial_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('keywords', lang('testimonial.keywords'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('keywords', null, array('class' => 'form-control', 'size' => '5x6')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', lang('testimonial.description'), array('class' => 'col-sm-3 control-label')) !!}

                            <div class="col-sm-9">
                                {!! Form::textarea('description', null, array('class' => 'form-control', 'size' => '5x6')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('testimonial.full_detail') !!}
                    </div>
                    <div class="panel-body padding0">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('full_detail', null, array('class' => 'form-control ckeditor')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 margintop10 clearfix text-center">
                    <div class="form-group">
                        {!! Form::submit(lang('common.save'), array('class' => 'btn btn-primary btn-lg')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>    
    </div>
</div>
<script> 
// wait for the DOM to be loaded
</script> 
<!-- /#page-wrapper -->
@stop