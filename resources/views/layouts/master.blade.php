<!DOCTYPE HTML>
<html>
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5GVWG8G');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <meta name="robots" content="index, follow" />
    <title>@if( isset($pageTitle)){!! $pageTitle !!}@else Best Advanced Care Physical Therapy in Milltown, NJ - Advanced Care Therapy @endif</title>
    <meta name="keywords" content="@if( isset($keywords)){!! $keywords !!}@endif" />
    <meta name="description" content="@if( isset($metaDescription) &&  $metaDescription != '' ){!! $metaDescription !!}@else Best Advanced Care Physical Therapy in Milltown, NJ - Advanced Care Therapy @endif" />
    <link rel="canonical" href="{!! Request::url() !!}" />
    <meta property="og:url" content="{!! Request::url() !!}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="@if( isset($pageTitle)) {!! $pageTitle !!} @endif" />
    <meta property="og:description" content="@if( isset($metaDescription) &&  $metaDescription != '' ){!! $metaDescription !!}@else Best Advanced Care Physical Therapy in Milltown, NJ - Advanced Care Therapy @endif" />
    @if(isset($imgPath[0]))
    <meta property="og:image" content="{!! Request::root() !!}/{!!$imgPath[0]!!}"/>
    @else
    <meta property="og:image" content="{!! Request::root() !!}/logo.svg"/>
    @endif
    <!-- favicon -->
    <link rel="shortcut icon" href="{!! asset('public/assets/images/logo.svg') !!}">
    
    <!-- bootstrap Style Sheet (caution ! - Do not edit this stylesheet) -->
    {!! HTML::style('public/site/css/bootstrap.min.css') !!}
    {!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.0/css/font-awesome.min.css') !!}
    {!! HTML::style('public/site/rs-plugin/css/settings.css') !!}
    {!! HTML::style('public/site/css/font-awesome.css') !!}
    {!! HTML::style('public/site/css/owl.carousel.css') !!}
    <link rel="stylesheet" href="https://themes.audemedia.com/html/goodgrowth/css/owl.theme.default.min.css"/>
    {!! HTML::style('public/site/css/style.css') !!}
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">
    {!! HTML::style('public/site/css/template.css') !!}
    @stack('style')
</head>
<body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5GVWG8G"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="<php echo 'http://www.google.com/chromeframe/?redirect=true'; ?>">activate Google Chrome Frame</a> to improve your experience.</p>
            <![endif]-->
            
            
            <!-- Header Start-->
            <div class="header">
                <div class="container">
                    <div class="row" style="padding-top: 10px;padding-bottom: 10px">
                        <div class="col-md-4 col-sm-3">
                            <div class="logo">
                                <a href="{!! route('/') !!}">
                                    {{-- <img src="{!! asset('public/assets/images/logo.svg') !!}" width="140" alt="{!! ucwords(Config::get('app.appname')) !!}"/> --}}
                                    <img src="{!! asset('public/assets/images/logo.svg') !!}" width="140" alt="{!! ucwords(Config::get('app.appname')) !!}"/>
                                </a>
                            </div>
                        </div>
                        <?php $settings = webSetting(); ?>
                        <div class="col-md-8 col-sm-9">
                            <div class="row adressWrp">
                                <div class="col-md-7 col-sm-6">
                                    <div class="headerInfo call">Call Now <span><a class="text-black" href="tel:{!! $settings->mobile !!}">{!! $settings->mobile !!}</a></span></div>
                                </div>
                                <div class="col-md-5 col-sm-6">
                                    <div class="headerInfo">Address <span class="font-16">{!! $settings->address !!}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navigationwrape">
                    <div class="navbar navbar-default" role="navigation">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                            </div>
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav">
                                    <li> <a href="{!! route('/') !!}"> Home </a></li>
                                    <li> <a href="{!! route('about') !!}"> About Us </a></li>
                                    <li class="dropdown"> <a> services <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <?php $services = getServices(['show_menu' => 1]); ?>
                                            @if(is_object($services))
                                            @foreach($services as $detail)
                                            <li>
                                                <a href="{!! route('service-detail', ['title' => $detail->url_slug]) !!}" title="{!! string_manip($detail->name, 'UCW') !!}">{!! string_manip($detail->name, 'UCW') !!}</a>
                                            </li>
                                            @endforeach
                                            <li>
                                                <a href="{!! route('services') !!}">All Services</a>
                                            </li>
                                            @endif
                                        </ul>
                                    </li>
                                    <li> <a href="{!! route('free-consultation') !!}">Fee Schedules</a></li>
                                    <li> <a href="{!! route('contact') !!}"> Contact Us</a></li>
                                    <li> <a href="{!! route('our-team') !!}"> Our Team</a></li>
                                    {{-- <li> <a href="{!! route('contact') !!}"> Blog</a></li> --}}
                                    <li> <a href="{!! route('enquiry') !!}"> Enquiry</a></li>
                                    @if (session()->has('user_id'))
                                    <li><a href="{!! route('frontend.user.profile') !!}">My Profile</a></li>  
                                    <li><a href="{!! route('frontend.user.logout') !!}">Logout</a></li>  
                                    @else
                                    <li><a href="{!! route('frontend.user.register') !!}">Registration</a></li>  
                                    <li><a href="{!! route('frontend.user.login') !!}">Login</a></li>  
                                    @endif
                                </ul>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header End-->
            
            @yield('content')
            
            <!-- footer start -->
            <div class="footer-wrap">
                <div class="container">
                    <div class="footer-info">
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <ul class="social-icon">
                                    <li><a href="{!! $settings->facebook_url !!}" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="{!! $settings->twitter_url !!}" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="{!! $settings->instagram_url !!}" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="email"> <span>Email</span> <a href="mailto:{!! $settings->email !!}">{!! $settings->email !!}</a> </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="footer-call"> <span>Call Us</span> <a href="tel:{!! $settings->mobile !!}" class="text-white">{!! $settings->mobile !!}</a> </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="footer-heading">Important Links</div>
                            <ul class="footer-nav">
                                @php
                                    $importantLinks = importantLinks();
                                @endphp
                                @foreach ($importantLinks as $importantLink)
                                    <li>    
                                        <a href="{{$importantLink->link}}" target="_blank">
                                             {{$importantLink->title}}
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="footer-heading">Quick Links</div>
                            <ul class="footer-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="{!! route('about') !!}">About Us</a></li>
                                <li><a href="{!! route('services') !!}">Services</a></li>
                                <li><a href="{!! route('free-consultation') !!}">Fee Schedules</a></li>
                                <li><a href="{!! route('contact') !!}">Contact Us</a></li>
                                <li><a href="{!! route('our-team') !!}">Our Team</a></li>
                                {{-- <li><a href="{!! route('contact') !!}">Disclaimer</a></li> --}}
                                <li><a href="{!! route('downloadform') !!}">Download Forms</a></li>
                                <li><a href="{!! route('testimonial') !!}">Testimonials</a></li>     
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="footer-heading">Our Services</div>
                            <ul class="footer-nav">
                                <?php $services = getServices(['show_menu' => 1], 5); ?>
                                @if(is_object($services))
                                @foreach($services as $detail)
                                <li>
                                    <a href="{!! route('service-detail', ['title' => $detail->url_slug]) !!}" title="{!! $detail->name !!}">{!! $detail->name !!}</a>
                                </li>
                                @endforeach
                                @endif
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="footer-heading">Newsletter</div>
                            <p>Get latest updates about our services.</p>
                            <form action="#" id="subscribe-form" class="foot-newsletter">
                                <div class="form-group">
                                    <label for="foot-news-email" class="sr-only">Enter a valid email</label>
                                    <input type="email" class="form-control" name="news_email" id="foot-news-email" placeholder="Enter a valid email">
                                </div>
                                <button class="btn btn-foot-news" type="submit"><i class="fa fa-envelope"></i></button>
                                <span id="subscriber-msg"></span>
                                <div class="form-response"></div>
                            </form>
                        </div>
                    </div>
                    <div class="footer-service">
                        <div class="col-sm-6">
                            <div class="copyright text-center">&copy; {{date('Y')}} - {{date('Y')+1}}. &nbsp;{!! $settings->all_rights_reserved !!}</div>
                        </div>
                        <div class="col-sm-3">
                        </div>
                    </div>
                </div>
            </div>
            <a id="scrollUp" href="#top"><i class="fa fa-angle-up"></i></a>
            <!-- footer end -->
            
            <!--processing-->
            <div class="loader">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p class='text-center'>
                                {!! HTML::image(asset('public/site/preload.gif'), 'processing', ['class' => 'processing', 'width' => 40]) !!}
                            </p>
                            <p class='text-center message'>Please Wait...</p>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
            <!-- end processing -->
            <div class="backDrop"> </div>
            {!! HTML::script('public/site/js/jquery-2.1.4.min.js') !!}
            {!! HTML::script('public/site/js/bootstrap.min.js') !!}
            {!! HTML::script('public/site/js/owl.carousel.js') !!}
            {!! HTML::script('public/site/rs-plugin/js/jquery.themepunch.tools.min.js') !!}
            {!! HTML::script('public/site/rs-plugin/js/jquery.themepunch.revolution.min.js') !!}
            {!! HTML::script('public/site/js/jquery.validate.min.js') !!}
            {!! HTML::script('public/site/js/jquery.form.js') !!}
            {!! HTML::script('public/site/js/script.js') !!}
            <script>
                $(window).scroll(function() {
                    var height = $(window).scrollTop();
                    if(height > 200) {
                        $("#scrollUp").fadeIn();
                    }else{
                        $("#scrollUp").fadeOut();
                    }
                });
            </script>
            <!-- GetButton.io widget -->
            <script type="text/javascript">
                (function () {
                    var options = {
                        whatsapp: "+9193183 67900", // WhatsApp number
                        call_to_action: "Message us", // Call to action
                        position: "left", // Position may be 'right' or 'left'
                        pre_filled_message: "Hello Exim Legal", // WhatsApp pre-filled message
                    };
                    var proto = document.location.protocol, host = "getbutton.io", url = proto + "//static." + host;
                    var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
                    s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
                    var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
                })();
                
                $("#subscribe-form").on("submit",function(){
                    var form = $(this);
                    var formData = $(this).serialize();
                    $.ajax({
                        url:"{{url('/subscribe-store')}}",
                        data:formData,
                        type:"get",
                        beforeSend:function(){
                            $("#subscriber-msg").text("Loading...");
                        },
                        success:function(response){
                            $("#subscriber-msg").text(response.message);
                            form.trigger("reset");
                        }
                    });
                    return false;
                });
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
                })
            </script>
            @stack('script')
            <!-- /GetButton.io widget -->
        </body>
        </html>