<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <title>:: {!! \Config::get('app.appname') . ' - ' .  \Config::get('app.slogan') !!} :: Admin Panel</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- Bootstrap Core CSS -->
    {!! HTML::style('public/assets/css/bootstrap.min.css') !!}
    <!-- MetisMenu CSS -->
    {!! HTML::style('public/assets/components/metis-menu/metisMenu.min.css') !!}
    <!-- Custom CSS -->
    {!! HTML::style('public/assets/css/style.css') !!}
    <!-- <link rel="stylesheet" type="text/css" href="http://www.jeasyui.com/easyui/themes/default/easyui.css"> -->
    {{--{!! HTML::style('public/assets/css/easyui.css') !!}--}}
    {!! HTML::style('public/assets/css/icon.css') !!}
    <!-- Custom CSS -->
    {!! HTML::style('public/assets/css/template.css') !!}
    <!-- Custom Fonts -->
    {!! HTML::style('public/assets/components/font-awesome/css/font-awesome.min.css') !!}
    <!-- Dropzone uploader -->
    {!! HTML::style('public/assets/components/dropzone/downloads/css/dropzone.css') !!}
    <!-- Select2 CSS -->
    {!! HTML::style('public/assets/components/select2/select2.min.css') !!}
    {!! HTML::style('public/assets/css/fSelect.css') !!}
    <!-- Jquery UI -->
    {!! HTML::style('public/assets/css/jquery-ui.css') !!}
    {!! HTML::style('public/assets/css/jquery-ui-timepicker.css') !!}
    <!-- jQuery -->
    {!! HTML::script('public/assets/js/jquery.min.js') !!}
    {!! HTML::script('public/assets/js/jquery-ui.js') !!}
    <!-- Bootstrap Core JavaScript -->
    {!! HTML::script('public/assets/js/bootstrap.min.js') !!}
    {!! HTML::script('public/assets/js/clipboard.min.js') !!}
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body {
            background: #35281F;
        }
        .nav-red {background-color: #35281F;}
        #side-menu.nav > li > a, .navbar-top-links > li > a {color: #fff;}
        .sidebar {background-color:#35281F;}
        .sidebar ul li {
            border-bottom: 1px solid #e1e1e1;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1) inset;
        }
        .sidebar ul li a.active {
            background-color: #cc8809;
        }
        .nav > li > a:focus, .nav > li > a:hover {
            background-color: #cc8809;
            text-decoration: none;
        }
        .sidebar .nav-second-level li, #side-menu, .sidebar .nav-third-level li {
            border-top: 1px solid #ffffff;
        }
    </style>
</head>
<body>
    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top nav-red" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{!! route('home') !!}" title="{!! Config::get('app.appdomain') !!}">
                    {{--{!! HTML::image(asset('assets/images/48x48.png'), ucwords(Config::get('app.appname')), ['width' => '20', 'align' => 'left', 'class' => 'marginright10']) !!}--}}
                    {!! Config::get('app.appname') !!}
                </a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li>
                    <a href="{!! route('user.changePassword') !!}">
                        Change Password
                        <i class="fa fa-key fa-fw"></i> &nbsp;
                    </a>
                </li>
                <li>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        @if (\Auth::check())
                            Welcome, {!! \Auth::user()->username !!}
                        @endif &nbsp;
                        <i class="fa fa-user fa-fw"></i> &nbsp;
                    </a>
                    <!-- /.dropdown-user -->
                </li>
                <li><a href="{!! route('logout') !!}"><i class="fa fa-sign-out fa-fw"></i>Logout</a></li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    @include('layouts.sidemenu')
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!--  page content part -->
		@yield('content')
    </div>

    <!--processing-->
    <div class="loader">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <p class='text-center'>
                        {!! HTML::image('public/assets/images/preload.gif', 'processing', ['class' => 'processing', 'width' => 40]) !!}
                    </p>
                    <p class='text-center message'>Loading...</p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    <!-- end processing -->
    <div class="backDrop"> </div>

    <!-- Modal Start for editing all-->
    <div class="modal fade" id="dynamicEdit" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="formTitle"> </h4>
                </div>
                <div id="dataResult" class="modal-body">
                    ...
                </div>
            </div>
        </div>
    </div>
    <!-- Modal editing End-->

    <!-- Modal editing End-->
    <!-- Metis Menu Plugin JavaScript -->
    {!! HTML::script('public/assets/components/metis-menu/metisMenu.js') !!}
    <!-- Main Theme JavaScript -->
    {!! HTML::script('public/assets/js/main.js') !!}
    <!-- Jquery Form JavaScript -->
    {!! HTML::script('public/assets/js/jquery.form.js') !!}
    <!-- Input Mask JavaScript -->
    {!! HTML::script('public/assets/components/input-mask/jquery.inputmask.js') !!}
    {!! HTML::script('public/assets/components/input-mask/jquery.inputmask.date.extensions.js') !!}
    <!-- Select2 JavaScript -->
    {!! HTML::script('public/assets/components/select2/select2.min.js') !!}
    {!! HTML::script('public/assets/components/dropzone/downloads/dropzone.min.js') !!}
    {!! HTML::script('public/assets/js/fSelect.js') !!}

    {!! HTML::script('public/assets/components/ckeditor/ckeditor.js') !!}
    {!! HTML::script('public/assets/components/ckeditor/adapters/jquery.js') !!}

    <!-- Custom JavaScript -->
    {!! HTML::script('public/assets/js/template.js') !!}
    {!! HTML::script('public/assets/js/jquery.easyui.min.js') !!}
    <!-- <script type="text/javascript" src="http://www.jeasyui.com/easyui/jquery.easyui.min.js"></script> -->
    @stack('script')
    @yield('script')
    <script>
        var activeurl = window.location;
        if($('a[href="'+activeurl+'"]').parent('li').parent('ul').parent('li'))
            $('a[href="'+activeurl+'"]').parent('li').parent('ul').parent('li').parent('ul').parent('li').parent('ul').addClass('in');
            $('a[href="'+activeurl+'"]').parent('li').parent('ul').parent('li').parent('ul').parent('li').parent('ul').parent('li').addClass('active');
            

            $('a[href="'+activeurl+'"]').parent('li').parent('ul').parent('li').parent('ul').addClass('in');
            $('a[href="'+activeurl+'"]').parent('li').parent('ul').parent('li').parent('ul').parent('li').addClass('active');
    </script>
</body>
</html>