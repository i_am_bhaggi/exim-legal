<div class="quote-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="quote-heading">Do you need professional legal assistance?</div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis facilisis leo eget maximus volutpat. Nulla eget bibendum urna, et vehicula ante. Donec et diam sodales, pellentesque est a, posuere ex.</p>
            </div>
            <div class="col-md-3">
                <div class="quote-btn"><a href="{!! route('free-consultation') !!}">request a quote</a></div>
            </div>
        </div>
    </div>
</div>