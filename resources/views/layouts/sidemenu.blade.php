<ul class="nav" id="side-menu">
    <li>
        <a href="{!! route('home') !!}"><i class="fa fa-dashboard fa-fw"></i>&nbsp; {!! lang('master.dashboard') !!} </a>
    </li>
    <li>
        <a href="{!! route('contact.index') !!}"><i class="fa fa-phone fa-fw"></i>&nbsp; {!! lang('master.contact_us') !!} </a>
    </li>
    
    <li>
        <a href="{!! route('download-form.index') !!}"> <i class="fa fa-file-pdf-o fa-fw"></i>&nbsp; {!! lang('download_form.download_forms') !!} </a>
    </li>
    <li>
        <a href="{!! route('pages.index') !!}"> <i class="fa fa-cubes fa-fw"></i>&nbsp; {!! lang('pages.pages') !!} </a>
    </li>
     <li>
         <a href="{!! route('/') !!}" target="_blank"><i class="fa fa-random fa-fw"></i>&nbsp; {!! lang('master.home') !!} </a>
     </li>
    <li>
        <a href="{!! route('services.index') !!}"> <i class="fa fa-stethoscope fa-fw"></i>&nbsp; {!! lang('services.services') !!} </a>
    </li>

    <li>
        <a href="{!! route('important-link.index') !!}"> <i class="fa fa-info fa-fw"></i>&nbsp; Important Links </a>
    </li>

    <li>
        <a href="{!! route('our-team.index') !!}"> <i class="fa fa-info fa-fw"></i>&nbsp; Our Team </a>
    </li>

    <li>
        <a href="{!! route('testimonial.index') !!}"> <i class="fa fa-diamond fa-fw"></i>&nbsp; {!! lang('testimonial.testimonials') !!} </a>
    </li>

    {{--<li>
        <a href="{!! route('appointments.index') !!}"> <i class="fa fa-newspaper-o fa-fw"></i>&nbsp; {!! lang('appointments.appointments') !!} </a>
    </li>--}}

    <li>
        <a href="{!! route('subscriber.index') !!}"> <i class="fa fa-users fa-fw"></i>&nbsp; {!! lang('subscriber.subscribers') !!} </a>
    </li>

    <li>
        <a href="{!! route('slider-banner.index') !!}"> <i class="fa fa-image fa-fw"></i>&nbsp; {!! lang('slider_banner.slider_banners') !!} </a>
    </li>

    <li>
        <a href="{!! route('setting.web-setting') !!}"> <i class="fa fa-cogs fa-fw"></i>&nbsp; {!! lang('setting.web_setting') !!} </a>
    </li>
</ul>
