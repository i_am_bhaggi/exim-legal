@extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10"> {!! lang('common.edit_heading', lang('services.service')) !!}: #{{ $result->name }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    
    {{-- for message rendering --}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-12 padding0">
            {!! Form::model($result, array('route' => array('services.update', $result->id), 'method' => 'PATCH', 'id' => 'ajaxSave', 'class' => 'form-horizontal')) !!}
                <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('services.services_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('name', lang('common.name'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group hidden">
                            {!! Form::label('inner_text', lang('services.sub_name'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('inner_text', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group hidden">
                            {!! Form::label('services', lang('services.image'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                <p class="pull-left col-md-12">
                                    {!! renderImage($result->image) !!}
                                </p>
                                {!! Form::file('image', null, array('class' => 'form-control')) !!}
                                {!! Form::hidden('old_image', $result->image, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('short_intro', lang('services.short_intro'), array('class' => 'col-sm-3 control-label')) !!}

                            <div class="col-sm-9">
                                {!! Form::textarea('short_intro', null, array('class' => 'form-control', 'size' => '5x6')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('status', lang('common.active') . '&nbsp;', array('class' => 'col-sm-2 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                 {!! Form::checkbox('status', '1', ($result->status == '1') ? true : false) !!}
                            </div>

                            {!! Form::label('frontpage', lang('services.frontpage') . '&nbsp;', array('class' => 'col-sm-2 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                {!! Form::checkbox('frontpage', '1', ($result->frontpage == '1') ? true : false) !!}
                            </div>

                            {!! Form::label('show_menu', lang('services.show_menu') . '&nbsp;', array('class' => 'col-sm-3 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                {!! Form::checkbox('show_menu', '1', ($result->show_menu == '1') ? true : false) !!}
                            </div>
                        </div>

                        <div class="col-sm-12 margintop20 clearfix text-center">
                            <div class="form-group">
                                {!! Form::submit(lang('common.update'), array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('common.seo_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('meta_title', lang('services.meta_title'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('meta_title', null, array('class' => 'form-control', 'size' => '5x4')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('keywords', lang('services.keywords'), array('class' => 'col-sm-3 control-label')) !!}

                            <div class="col-sm-9">
                                {!! Form::textarea('keywords', null, array('class' => 'form-control', 'size' => '5x4')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('description', lang('services.description'), array('class' => 'col-sm-3 control-label')) !!}

                            <div class="col-sm-9">
                                {!! Form::textarea('description', null, array('class' => 'form-control', 'size' => '5x4')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('services.full_detail') !!}
                    </div>
                    <div class="panel-body padding0">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('full_detail', null, array('class' => 'form-control ckeditor')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 margintop10 clearfix text-center">
                    <div class="form-group">
                        {!! Form::submit(lang('common.update'), array('class' => 'btn btn-primary btn-lg')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

            <div class="col-md-12 hiddenkj">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        Upload Picture(s)
                    </div>
                    <div class="panel-body">

                        <div class="col-md-12"> &nbsp; </div>

                        <?php
                            $path = Config::get('constants.UPLOADS');
                            $folderPath = ROOT . $path;

                            $folder =  $folderPath . '/';
                            $files = glob($folder . $result->id . "__*");
                            if (count($files) > 0 )
                            {
                                $iteration = 0;
                                foreach ($files as $imagePath)
                                {
                                $name = basename($imagePath);

                                if (file_exists($imagePath))
                                {
                                    if ( $iteration++ == 6 )
                                        {
                        //segregate thumbs after 6 by col-md-12
                        ?>

                        <div class="col-md-12"> &nbsp;</div>

                        <?php } ?>

                        <div class="col-md-2 paddingleft0">
                            <div class="thumbnail" style="position:relative;">
                            {!! HTML::image(asset('uploads/' . $name)) !!}
                            <span class="btn btn-xs btn-primary copy-link text-center margintop0" style="position:absolute; right:15px; top:-5px;" title="Copied" data-clipboard-text="{!! asset('uploads/' . $name) !!}">Copy</span>
                            <span class="text-center margintop0" style="position:absolute; right:-10px; top:-5px;">
                                <a data-original-title="Delete" data-placement="right" class="btn btn-xs btn-danger tooltips __drop"  href="javascript:void(0);" data-route="{!! route('services.picture-delete', [$result->id, base64_encode($name)]) !!}" data-message="Are you sure? want to delete this image."><i class="fa-times fa fa-white"></i></a>
                            </span>
                            </div>
                        </div>
                        <?php } } } ?>
                        {!! Form::open(array('route' => array('services.upload', 'id' => $result->id), 'id' => 'my-awesome-dropzone', 'class' => 'dropzone col-md-12')) !!}

                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
        </div>    
    </div>
</div>
<!-- /#page-wrapper -->
<script>
    var clipboard = new Clipboard('.copy-link');
    clipboard.on('success', function(e) {
        $(e.trigger).attr('title', 'Copied').tooltip('fixTitle').tooltip('show');
        $(e.trigger).hover(function(){
            $(this).tooltip("hide");
        });
    });
</script>
@stop