<thead>
<tr>
    <th width="5%" class="text-center">{!! lang('common.id') !!}</th>
    <th>{!! lang('common.name') !!}</th>
    <th class="text-center">  </th>
    <th class="text-center">{!! lang('common.action') !!}</th>
</tr>
</thead>
<tbody>
<?php $index = 1; ?>
@foreach($data as $detail)
    <tr id="order_{{ $detail->id }}">
        <td class="text-center">{!! pageIndex($index++, $page, $perPage) !!}</td>
        <td> 
            <a href="{{ route('services.edit', [$detail->id]) }}">
                {!! $detail->name !!}
            </a>
        </td>
        <td class="text-center">
            <a href="javascript:void(0);" class="toggle-status" data-message="{!! lang('messages.change_status') !!}" data-route="{!! route('services.toggle', $detail->id) !!}">
                {!! HTML::image('public/assets/images/' . $detail->status . '.gif') !!}
            </a>
            &nbsp;
            <a href="javascript:void(0);" class="toggle-status" data-message="{!! lang('messages.change_status') !!}" data-width="16px" data-route="{!! route('services.featured', $detail->id) !!}">
                {!! HTML::image('assets/images/home' . $detail->frontpage . '.png', '', ['width' => '16px']) !!}
            </a>
        </td>
        <td class="text-center col-md-1">
            <a class="btn btn-xs btn-primary" href="{{ route('services.edit', [$detail->id]) }}"><i class="fa fa-edit"></i></a>
        </td>
    </tr>
@endforeach
@if (count($data) < 1)
    <tr>
        <td class="text-center" colspan="6"> {!! lang('messages.no_data_found') !!} </td>
    </tr>
@else
    <tr class="margintop10">
        <td colspan="6">
            {!! paginationControls($page, $total, $perPage) !!}
        </td>
    </tr>
@endif
</tbody>