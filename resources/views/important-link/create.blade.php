@extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10">{!! lang('common.create_heading', lang('important-link.important')) !!}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    
    {{-- for message rendering --}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-12 padding0">
            {!! Form::open(array('method' => 'POST', 'route' => array('important-link.store'), 'id' => 'ajaxSave', 'files' => true, 'class' => 'form-horizontal')) !!}
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('important-link.important_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('title', lang('important-link.title'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('title', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('link', lang('important-link.link'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::url('link', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 margintop10 clearfix text-center">
                                <div class="form-group">
                                    {!! Form::submit(lang('common.save'), array('class' => 'btn btn-primary btn-lg')) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            
            {!! Form::close() !!}
        </div>    
    </div>
</div>
<script> 
// wait for the DOM to be loaded
</script> 
<!-- /#page-wrapper -->
@stop