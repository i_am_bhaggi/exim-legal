@extends('layouts.master')
<?php
    $pageTitle = "";
    $metaDescription = "";
    $keywords = "";
?>
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
            <h3>500 Error Page</h3>
        </div>
    </div>
    <!--Inner Heading end-->

    <!--inner-content start-->
    <div class="inner-content innerbg">
        <div class="container">

            <!-- 404 start -->

            <div class="inner-wrap">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="four-zero-page">
                            <h2>500</h2>
                            <h3>Sorry Server Error.</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 404 end -->

        </div>
    </div>
    <!--inner-content end-->
@stop
