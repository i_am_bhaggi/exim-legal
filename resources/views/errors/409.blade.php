@extends('layouts.master')
<?php
$pageTitle = "";
$metaDescription = "";
$keywords = "";
?>
@section('content')
    <!--Inner Heading start-->
    <div class="inner-heading">
        <div class="container">
            <h3>404 Page</h3>
        </div>
    </div>
    <!--Inner Heading end-->

    <!--inner-content start-->
    <div class="inner-content innerbg">
        <div class="container">

            <!-- 404 start -->

            <div class="inner-wrap">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="four-zero-page">
                            <h2>404</h2>
                            <h3>Sorry Page Was Not Found</h3>
                            <p>The page you are looking is not available or has been removed.
                                Try going to Home Page by using the button below.</p>
                            <div class="readmore"> <a href="{!! route('/') !!}">Go Back To Home page</a> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 404 end -->

        </div>
    </div>
    <!--inner-content end-->
@stop
