@extends('layouts.master')
<?php
	$pageTitle = "";
	$metaDescription = "";
	$keywords = "";
?>
@section('content')
	<!--Inner Heading start-->
	<div class="inner-heading">
		<div class="container">
			<h3>Our Team</h3>
		</div>
	</div>
	<!--Inner Heading end--> 
	<!--inner-content start-->
	<div class="inner-content service-wrap">
		<div class="container">
			<?php $ourTeams = getOurTeams(); ?>

			<!-- service start -->
			<ul class="row serv-area">
				@if(is_object($ourTeams))
					@foreach($ourTeams as $detail)
						<li class="col-md-4">
							<div class="service-block">
								<?php $path = "public". \Config::get('constants.TEAM_UPLOAD'); ?>
								<div class="service-icon" style="overflow: hidden;height:150px;width:150px;border:none">
									{!! HTML::image($path . $detail['image-pic'], '', ['height' => '100%','width' => '100%']) !!}
								</div>
								<h4>
                                    <a href="{!! route('service-detail', ['title' => $detail->url_slug]) !!}" title="{!! string_manip($detail->name, 'UCW') !!}">
										{!! string_manip($detail->name, 'UCW') !!}
									</a>
								</h4>
                                    <span>
                                        {!! string_manip($detail->designation, 'UCW') !!}
                                    </span>
								{{--<p class="content">
									{!! $detail->short_intro !!}
								</p>--}}
								<p>&nbsp;</p>
								<div class="readmore"><a href="{!! route('team-detail', ['id' => $detail->id]) !!}" title="{!! string_manip($detail->name, 'UCW') !!}">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
							</div>
						</li>
					@endforeach
				@endif
			</ul>
			<!-- service end -->
		</div>
		@include('layouts.partials.quote')
	</div>
	<!--inner-content end-->
@stop
