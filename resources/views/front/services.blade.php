@extends('layouts.master')
<?php
	$pageTitle = "";
	$metaDescription = "";
	$keywords = "";
?>
@section('content')
	<!--Inner Heading start-->
	<div class="inner-heading">
		<div class="container">
			<h3>Our Services</h3>
		</div>
	</div>
	<!--Inner Heading end-->
	<!--inner-content start-->
	<div class="inner-content service-wrap">
		<div class="container">
			<?php $services = getServices(); ?>

			<!-- service start -->
			<ul class="row serv-area">
				@if(is_object($services))
					@foreach($services as $detail)
						<li class="col-md-4">
							<div class="service-block">
								<div class="service-icon"><i class="fa fa-gavel" aria-hidden="true"></i></div>
								<h4><a href="{!! route('service-detail', ['title' => $detail->url_slug]) !!}" title="{!! string_manip($detail->name, 'UCW') !!}">
										{!! string_manip($detail->name, 'UCW') !!}
									</a>
								</h4>
								{{--<p class="content">
									{!! $detail->short_intro !!}
								</p>--}}
								<p>&nbsp;</p>
								<div class="readmore"><a href="{!! route('service-detail', ['title' => $detail->url_slug]) !!}" title="{!! string_manip($detail->name, 'UCW') !!}">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
							</div>
						</li>
					@endforeach
				@endif
			</ul>
			<!-- service end -->
		</div>
		@include('layouts.partials.quote')
	</div>
	<!--inner-content end-->
@stop
