<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="google-signin-client_id" content="31702594769-b63d3trugcscnp45ppo5co23vlr0gavv.apps.googleusercontent.com">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{!!  asset('public/logo.svg') !!}">
    <title>:: {!! Config::get('app.appname') . ' - ' .  Config::get('app.slogan') !!} ::</title>
    <!-- Bootstrap Core CSS -->
    {!! HTML::style('public/assets/css/bootstrap.min.css') !!}
    
    <!-- Custom CSS -->
    {!! HTML::style('public/assets/css/style.css') !!}
    
    <!-- Custom Fonts -->
    {!! HTML::style('public/assets/components/font-awesome/css/font-awesome.min.css') !!}
    
    <!-- jQuery -->
    {!! HTML::script('public/assets/js/jquery.min.js') !!}
    <!-- Bootstrap Core JavaScript -->
    {!! HTML::script('public/assets/js/bootstrap.min.js') !!}  
    <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    <style>
        a{
            text-decoration: none !important;
        }
        .login-with-google-btn {
            transition: background-color .3s, box-shadow .3s;
            
            padding: 12px 16px 12px 42px;
            border: none;
            border-radius: 3px;
            box-shadow: 0 -1px 0 rgba(0, 0, 0, .04), 0 1px 1px rgba(0, 0, 0, .25);
            
            color: #757575;
            font-size: 14px;
            font-weight: 500;
            font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",sans-serif;
            
            background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cGF0aCBkPSJNMTcuNiA5LjJsLS4xLTEuOEg5djMuNGg0LjhDMTMuNiAxMiAxMyAxMyAxMiAxMy42djIuMmgzYTguOCA4LjggMCAwIDAgMi42LTYuNnoiIGZpbGw9IiM0Mjg1RjQiIGZpbGwtcnVsZT0ibm9uemVybyIvPjxwYXRoIGQ9Ik05IDE4YzIuNCAwIDQuNS0uOCA2LTIuMmwtMy0yLjJhNS40IDUuNCAwIDAgMS04LTIuOUgxVjEzYTkgOSAwIDAgMCA4IDV6IiBmaWxsPSIjMzRBODUzIiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48cGF0aCBkPSJNNCAxMC43YTUuNCA1LjQgMCAwIDEgMC0zLjRWNUgxYTkgOSAwIDAgMCAwIDhsMy0yLjN6IiBmaWxsPSIjRkJCQzA1IiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48cGF0aCBkPSJNOSAzLjZjMS4zIDAgMi41LjQgMy40IDEuM0wxNSAyLjNBOSA5IDAgMCAwIDEgNWwzIDIuNGE1LjQgNS40IDAgMCAxIDUtMy43eiIgZmlsbD0iI0VBNDMzNSIgZmlsbC1ydWxlPSJub256ZXJvIi8+PHBhdGggZD0iTTAgMGgxOHYxOEgweiIvPjwvZz48L3N2Zz4=);
            background-color: white;
            background-repeat: no-repeat;
            background-position: 12px 11px;
            
            &:hover {
                box-shadow: 0 -1px 0 rgba(0, 0, 0, .04), 0 2px 4px rgba(0, 0, 0, .25);
            }
            
            &:active {
                background-color: #eeeeee;
            }
            
            &:focus {
                outline: none;
                box-shadow: 
                0 -1px 0 rgba(0, 0, 0, .04),
                0 2px 4px rgba(0, 0, 0, .25),
                0 0 0 3px #c8dafc;
            }
            
            &:disabled {
                filter: grayscale(100%);
                background-color: #ebebeb;
                box-shadow: 0 -1px 0 rgba(0, 0, 0, .04), 0 1px 1px rgba(0, 0, 0, .25);
                cursor: not-allowed;
            }
        }
    </style>
    </head>
    
    <body class="login-page">
        
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-panel panel panel-info">
                        <div class="panel-heading" style="background:hsl(0, 0%, 100%);">
                            <p class="text-center margin0">
                                <a href="{{url('/')}}">
                                {!! HTML::image(asset('public/assets/images/logo.svg'), ucwords(Config::get('app.slogan')), ['width' => 200]) !!}
                                </a>
                            </p>
                        </div>
                        <div class="panel-body">
                            <h5 class="text-center">
                                <a href="{{url('/')}}">
                                    <i class="fa fa-arrow-left"></i> Back to home
                                </a>                                            
                            </h5>
                            <h3 class="panel-title clearfix text-center"> <p> Sign in to {!! ucwords(Config::get('app.appname')) !!} </p></h3>
                            <h4 class="text-center" id="login-message"></h4>
                            @if (Session::has('error'))
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close">
                                    &times;
                                </button>
                                <i class="fa fa-times-circle"></i> &nbsp;
                                {!! Session::get('error') !!}
                            </div>
                            @endif
                            {!! Form::open(array('route' => array('frontend.user.dologin'), 'class' => 'form-login', 'method' => 'post', 'onsubmit' => 'return loginUser(this)')) !!}
                            <fieldset>
                                <div class="form-group">
                                    <label for=""> Email</label>
                                    <input class="form-control" placeholder="Email" name="email" type="email" autofocus autocomplete="false" required>
                                </div>
                                <div class="form-group">
                                    <label for=""> Password</label>
                                    <input class="form-control password-box" placeholder="Password" name="password" type="password" value="" required>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-primary" type="button" onclick="togglePassword(this)" style="margin-bottom: 10px">
                                    <i class="fa fa-eye"></i>
                                </button>
                                <button type="submit" class="btn btn-danger" style="margin-bottom: 10px">
                                    Login <i class="fa fa-arrow-circle-right"></i>
                                </button>
                                &nbsp;&nbsp;OR&nbsp;&nbsp;
                                <a href="{{route('frontend.user.register')}}">
                                    <button type="button" class="btn btn-danger" style="margin-bottom: 10px">
                                        Register
                                    </button>
                                </a>
                                {{-- <button type="button" class="btn-block login-with-google-btn g-signin2" style="margin-top:10px" data-onsuccess="onSignIn">
                                    Sign in with Google
                                </button> --}}
                                {{-- <div class="g-signin2" data-onsuccess="onSignIn" style="margin-top: 10px"></div> --}}
                                {{-- <div id="my-signin2"></div> --}}
                            </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- Custom Theme JavaScript -->
        <script>
            var isShown = 0;
            var passwordBoxes = $(".password-box");
            function togglePassword(elem){                
                if(isShown == 0){
                    passwordBoxes.attr("type","text");
                    $(elem).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
                }else{
                    passwordBoxes.attr("type","password");
                    $(elem).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
                }
                isShown = !isShown;
            }
            
            // function onSuccess(googleUser) {
            //     var profile = googleUser.getBasicProfile();
            //     var name = profile.getName();
            //     var email = profile.getEmail(); // This is null if the 'email' scope is not present.
            //     if(name != null && email != null){
            //        window.location.href = `{{url('/google-login/callback')}}/?name=${name}&email=$email`; 
            //     }
            // }
            // function onFailure(error){
            //     console.log(error);
            // }
            // function renderButton() {
            //     gapi.signin2.render('my-signin2', {
            //         'scope': 'profile email',
            //         'width': "300",
            //         'height': 50,
            //         'margin': 10,
            //         'longtitle': true,
            //         'theme': 'dark',
            //         'onsuccess': onSuccess,
            //         'onfailure': onFailure
            //     });
            // }
        </script>
        <script>
            var loginMessageBox = $('#login-message');
            var signInBtn  = $('#sign-in-btn');
            function loginUser(form){
                var data = $(form).serialize();
                signInBtn.html('LOADING... <i class="fa fa-arrow-circle-right"></i>');
                $.ajax({
                    type:"post",
                    url:"{{url('/login')}}",
                    data:data,
                    success:function(response){
                        if(response.status){
                            loginMessageBox.addClass("text-success").removeClass("text-danger").text(response.message);
                            $(form).trigger('reset');
                            if(response.sigin_requested_from != ""){
                                var redirectTo = response.sigin_requested_from;
                            }else{
                                var redirectTo =  "{{url('/')}}";
                            }
                            var timer = 5;
                            setInterval(function() {
                                timer--;
                                loginMessageBox.text(`Login successful, Redirecting you in ${timer} seconds`);
                            }, 1000)
                            setTimeout(function() {
                                window.location.href = redirectTo;
                            }, 4000);
                        }else{
                            loginMessageBox.addClass("text-danger").removeClass("text-success").text(response.message);
                        }
                        signInBtn.html('Loginin <i class="fa fa-arrow-circle-right"></i>');
                    }
                })
                return false;
            }
        </script>
    </body>
    </html>