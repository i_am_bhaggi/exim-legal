@extends('layouts.master')
<?php
$pageTitle = "";
$metaDescription = "";
$keywords = "";
?>
@push('style')
<meta name="google-signin-client_id" content="31702594769-b63d3trugcscnp45ppo5co23vlr0gavv.apps.googleusercontent.com">
<script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
<style>
    a{
        text-decoration: none !important;
    }
    .login-with-google-btn {
        transition: background-color .3s, box-shadow .3s;
        
        padding: 12px 16px 12px 42px;
        border: none;
        border-radius: 3px;
        box-shadow: 0 -1px 0 rgba(0, 0, 0, .04), 0 1px 1px rgba(0, 0, 0, .25);
        
        color: #757575;
        font-size: 14px;
        font-weight: 500;
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Fira Sans","Droid Sans","Helvetica Neue",sans-serif;
        
        background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgiIGhlaWdodD0iMTgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGcgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIj48cGF0aCBkPSJNMTcuNiA5LjJsLS4xLTEuOEg5djMuNGg0LjhDMTMuNiAxMiAxMyAxMyAxMiAxMy42djIuMmgzYTguOCA4LjggMCAwIDAgMi42LTYuNnoiIGZpbGw9IiM0Mjg1RjQiIGZpbGwtcnVsZT0ibm9uemVybyIvPjxwYXRoIGQ9Ik05IDE4YzIuNCAwIDQuNS0uOCA2LTIuMmwtMy0yLjJhNS40IDUuNCAwIDAgMS04LTIuOUgxVjEzYTkgOSAwIDAgMCA4IDV6IiBmaWxsPSIjMzRBODUzIiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48cGF0aCBkPSJNNCAxMC43YTUuNCA1LjQgMCAwIDEgMC0zLjRWNUgxYTkgOSAwIDAgMCAwIDhsMy0yLjN6IiBmaWxsPSIjRkJCQzA1IiBmaWxsLXJ1bGU9Im5vbnplcm8iLz48cGF0aCBkPSJNOSAzLjZjMS4zIDAgMi41LjQgMy40IDEuM0wxNSAyLjNBOSA5IDAgMCAwIDEgNWwzIDIuNGE1LjQgNS40IDAgMCAxIDUtMy43eiIgZmlsbD0iI0VBNDMzNSIgZmlsbC1ydWxlPSJub256ZXJvIi8+PHBhdGggZD0iTTAgMGgxOHYxOEgweiIvPjwvZz48L3N2Zz4=);
        background-color: white;
        background-repeat: no-repeat;
        background-position: 12px 11px;
        
        &:hover {
            box-shadow: 0 -1px 0 rgba(0, 0, 0, .04), 0 2px 4px rgba(0, 0, 0, .25);
        }
        
        &:active {
            background-color: #eeeeee;
        }
        
        &:focus {
            outline: none;
            box-shadow: 
            0 -1px 0 rgba(0, 0, 0, .04),
            0 2px 4px rgba(0, 0, 0, .25),
            0 0 0 3px #c8dafc;
        }
        
        &:disabled {
            filter: grayscale(100%);
            background-color: #ebebeb;
            box-shadow: 0 -1px 0 rgba(0, 0, 0, .04), 0 1px 1px rgba(0, 0, 0, .25);
            cursor: not-allowed;
        }
    }
    .abcRioButtonContentWrapper{
        margin: auto;
        width: 200px;
    }
</style>
@endpush
@section('content')
<!--Inner Heading start-->
<div class="inner-heading">
    <div class="container">
        <h3>Login</h3>
    </div>
</div>
<!--Inner Heading end-->
<!--inner-content start-->
<div class="inner-content paddingtop8">
    <div class="container">
        <div class="contact-wrap">
            <div class="row">
                <?php $settings = webSetting(); ?>
                <div class="col-md-12 col-sm-12 column">.
                    <h1 class="text-center">Enter <span>Your Details </span></h1>
                    <div class="contact-form">
                        <div class="col-md-12">
                            <div id="response-container"></div>
                            <div id="error-container"></div>
                        </div>
                        <div id="message"></div>
                        <form method="post" action="{!! route('contact-send') !!}" id="contact_form">
                            <div class="row">
                                <div class="col-md-12 text-left">
                                    <label for="email">Email:</label>
                                    <input name="email" type="email" id="email" class="required"  title="* Please provide your email" placeholder="Email"  />
                                </div>
                                <div class="col-md-12 margintop10 text-left">
                                    <label for="password"><br/> Password:</label>
                                    <input name="password" type="password" class="required password" title="* Please provide a valid password address" id="password" placeholder="Password"  />
                                </div>
                                {{-- <div class="col-md-12 margintop10">
                                    <div id="my-signin2"></div>
                                </div> --}}
                                <!-- Change this to a button or input when using this as a form -->
                             </div>
                                <div class="margintop10">
                                    <button class="button readmore form-submit-btn" type="button" onclick="togglePassword(this)" style="margin-bottom: 10px">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                    <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                                    <button class="button readmore form-submit-btn" type="submit" id="submit">Login</button>
                                </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact end -->
    </div>
</div>
<!--inner-content end-->
@stop
{{-- 
    @push('script')
    <!-- Custom Theme JavaScript -->
    <script>
        var isShown = 0;
        var passwordBoxes = $(".password-box");
        function togglePassword(elem){                
            if(isShown == 0){
                passwordBoxes.attr("type","text");
                $(elem).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
            }else{
                passwordBoxes.attr("type","password");
                $(elem).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
            }
            isShown = !isShown;
        }
        
        function onSuccess(googleUser) {
            var profile = googleUser.getBasicProfile();
            var name = profile.getName();
            var email = profile.getEmail(); // This is null if the 'email' scope is not present.
            if(name != null && email != null){
                window.location.href = `{{url('/google-login/callback')}}/?name=${name}&email=$email`; 
            }
        }
        function onFailure(error){
            console.log(error);
        }
        function renderButton() {
            gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': "300",
                'height': 50,
                'margin': 10,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
            });
        }
    </script>
    <script>
        var loginMessageBox = $('#login-message');
        var signInBtn  = $('#sign-in-btn');
        function loginUser(form){
            var data = $(form).serialize();
            signInBtn.html('LOADING... <i class="fa fa-arrow-circle-right"></i>');
            $.ajax({
                type:"post",
                url:"{{url('/login')}}",
                data:data,
                success:function(response){
                    if(response.status){
                        loginMessageBox.addClass("text-success").removeClass("text-danger").text(response.message);
                        $(form).trigger('reset');
                        if(response.sigin_requested_from != ""){
                            var redirectTo = response.sigin_requested_from;
                        }else{
                            var redirectTo =  "{{url('/')}}";
                        }
                        var timer = 5;
                        setInterval(function() {
                            timer--;
                            loginMessageBox.text(`Login successful, Redirecting you in ${timer} seconds`);
                        }, 1000)
                        setTimeout(function() {
                            window.location.href = redirectTo;
                        }, 4000);
                    }else{
                        loginMessageBox.addClass("text-danger").removeClass("text-success").text(response.message);
                    }
                    signInBtn.html('Loginin <i class="fa fa-arrow-circle-right"></i>');
                }
            })
            return false;
        }
    </script>
    @endpush --}}
    @push('script')
    <script>
        var isShown = 0;
        var passwordBoxes = $(".password-box");
        function togglePassword(elem){                
            if(isShown == 0){
                passwordBoxes.attr("type","text");
                $(elem).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
            }else{
                passwordBoxes.attr("type","password");
                $(elem).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
            }
            isShown = !isShown;
        }
        
        function onSuccess(googleUser) {
            var profile = googleUser.getBasicProfile();
            var name = profile.getName();
            var email = profile.getEmail(); // This is null if the 'email' scope is not present.
            if(name != null && email != null){
                window.location.href = `{{url('/google-login/callback')}}/?name=${name}&email=$email`; 
            }
        }
        function onFailure(error){
            console.log(error);
        }
        function renderButton() {
            gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': "300",
                'height': 50,
                'margin': 10,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
            });
        }
    </script>
    <script>
        var loginMessageBox = $('#login-message');
        var signInBtn  = $('#sign-in-btn');
        function loginUser(form){
            var data = $(form).serialize();
            signInBtn.html('LOADING... <i class="fa fa-arrow-circle-right"></i>');
            $.ajax({
                type:"post",
                url:"{{url('/login')}}",
                data:data,
                success:function(response){
                    if(response.status){
                        loginMessageBox.addClass("text-success").removeClass("text-danger").text(response.message);
                        $(form).trigger('reset');
                        if(response.sigin_requested_from != ""){
                            var redirectTo = response.sigin_requested_from;
                        }else{
                            var redirectTo =  "{{url('/')}}";
                        }
                        var timer = 5;
                        setInterval(function() {
                            timer--;
                            loginMessageBox.text(`Login successful, Redirecting you in ${timer} seconds`);
                        }, 1000)
                        setTimeout(function() {
                            window.location.href = redirectTo;
                        }, 4000);
                    }else{
                        loginMessageBox.addClass("text-danger").removeClass("text-success").text(response.message);
                    }
                    signInBtn.html('Loginin <i class="fa fa-arrow-circle-right"></i>');
                }
            })
            return false;
        }
    </script>
    @endpush