@extends('layouts.master')
<?php
	$pageTitle = "";
	$metaDescription = "";
	$keywords = "";
?>
@section('content')
	<!--Inner Heading start-->
	<div class="inner-heading">
		<div class="container">
			<h3>Testimonials</h3>
		</div>
	</div>
	<!--Inner Heading end-->
	<!--inner-content start-->
	<div class="inner-content paddingtop8">
		<div class="container">
			<div class="contact-wrap">
                <div id="testimonials-page" class="row">
                    <!--TESTIMONIAL Start-->
                    @php
                        $testimonials = getTestimonials();
                    @endphp
                    @foreach ($testimonials as $testimonial)
                    <div class="col-lg-4 col-md-4 col-sm-6 item">
                        <div class="shadow-effect">
                            <?php $path = \Config::get('constants.UPLOADS'); ?>
                            <div class="img-box">
                                {!! HTML::image($path . $testimonial->image, '', ['height' => '100px', 'class' => 'img-circle']) !!}
                            </div>
                            <p>{{$testimonial->short_intro}}</p>
                            <div class="testimonial-name">- {{$testimonial->name}}</div>
                        </div>
                    </div>
                    @endforeach
                    <!--END OF TESTIMONIAL -->
                </div>
            </div>
			</div>
			<!-- contact end -->

		</div>
	</div>
	<!--inner-content end-->
@stop
