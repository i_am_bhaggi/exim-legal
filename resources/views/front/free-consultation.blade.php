@extends('layouts.master')
<?php
	$pageTitle = "";
	$metaDescription = "";
	$keywords = "";
?>
@section('content')
	<!--Inner Heading start-->
	<div class="inner-heading">
		<div class="container">
			<h3>Fee Schedules</h3>
		</div>
	</div>
	<!--Inner Heading end-->

	<!--inner-content start-->
	<div class="inner-content innerbg">
		<div class="container">
			<!-- Login start -->
			<div class="row">
				<div class="col-md-2 col-sm-2"></div>
				<div class="col-md-8 col-sm-8">
					<div class="appointment">
						<div class="contctxt"><h1>Fee <span>Schedules</span></h1></div>
						<div class="formint conForm">
							<div class="col-md-12">
								<div id="response-container"></div>
								<div id="error-container"></div>
							</div>

							<form method="post" action="{!! route('consultation-send') !!}" id="contact_form">
								<div class="row">
									<div class="col-md-6">
										<div class="input-wrap">
											<input type="text" name="name" placeholder="Name" class="form-control required"  title="* Please provide your name" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="input-wrap">
											<input type="text" name="email" placeholder="Email" class="form-control required email" title="* Please provide a valid email address" >
										</div>
									</div>

									<div class="col-md-6">
										<div class="input-wrap">
											<input type="text" name="mobile" placeholder="Mobile Number" class="form-control required" data-msg-required="* Please provide a valid mobile number" >
										</div>
									</div>
									<div class="col-md-6">
										<div class="input-wrap">
											<input type="text" name="subject" placeholder="Subject" class="form-control required" title="* Please provide your subject">
										</div>
									</div>
									<div class="col-md-12">
										<div class="input-wrap">
											<select name="service" class="form-control required" title="* Please select service.">
												<?php $services = getServices(); ?>
													@if(is_object($services))
														@foreach($services as $detail)
															<option value="{!! string_manip($detail->name, 'UCW') !!}">{!! string_manip($detail->name, 'UCW') !!}</option>
														@endforeach
													@endif
											</select>
										</div>
									</div>
									<div class="col-md-12">
										<div class="input-wrap">
											<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
											<textarea class="form-control required" placeholder="Message" name="message" title="* Please provide your message"></textarea>
										</div>
									</div>
									<div class="col-md-12">
										<div class="sub-btn">
											<input type="submit" class="sbutn" value="Send Message">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-2"></div>
			</div>
			<!-- Login end -->
		</div>
		@include('layouts.partials.quote')
	</div>
	<!--inner-content end-->
@stop
