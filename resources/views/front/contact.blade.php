@extends('layouts.master')
<?php
	
	$pageTitle = "";
	$metaDescription = "";
	$keywords = "";
?>
{!! NoCaptcha::renderJs() !!}
@section('content')
	<!--Inner Heading start-->
	<div class="inner-heading">
		<div class="container">
			<h3>Contact Us</h3>
		</div>
	</div>
	<!--Inner Heading end-->
	<!--inner-content start-->
	<div class="inner-content paddingtop8">
		<div class="container-fluid padding0">
			<!-- contact start -->
			<div class="map">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3503.963177142991!2d77.21592440031006!3d28.57086843235603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce25df1ba5991%3A0x2cc498e63d20a646!2sSouth%20Extension%2C%20Block%20J%2C%20South%20Extension%20I%2C%20New%20Delhi%2C%20Delhi!5e0!3m2!1sen!2sin!4v1598372701393!5m2!1sen!2sin" width="100" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			</div>
		</div>
		<div class="container">
			<div class="contact-wrap">
				<div class="row">
					<?php $settings = webSetting(); ?>
					<div class="col-md-8 col-sm-8 column">.
						<h1>Get <span>In Touch </span></h1>
						<div class="contact-form">
							<div class="col-md-12">
								<div id="response-container"></div>
								<div id="error-container"></div>
							</div>
							<div id="message"></div>
							<form method="post" action="{!! route('contact-send') !!}" id="contact_form">
								<div class="row">
									<div class="col-md-6">
										<input name="name" type="text" id="name" class="required"  title="* Please provide your name" placeholder="Full Name">
									</div>
									<div class="col-md-6">
										<input type="text" name="mobile" class="required number" data-msg-required="* Please provide a valid mobile number" minlength="10" maxlength="10" placeholder="Mobile Number">
									</div>
									<div class="col-md-12 margintop10">
										<input name="email" type="text" class="required email" title="* Please provide a valid email address" id="email" placeholder="Email">
									</div>
									<input type="hidden" name="success-message" value="Thank you for your message, we will get back to you soon">
									<div class="col-md-12 margintop10">
										<textarea rows="4" name="message" class="required" id="message" placeholder="Message"  title="* Please provide your message"></textarea>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 margintop10">
										{!! NoCaptcha::display() !!}
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 margintop10">
										<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
										<button class="button readmore form-submit-btn" type="submit" id="submit">Submit</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 column">
						<div class="contact-now">
							<div class="contact">
								<div class="contact-icon"> <span><i class="fa fa-map-marker" aria-hidden="true"></i></span></div>
								<div class="information"> <strong>Address:</strong>
									<p>{!! $settings->address !!}</p>
								</div>
							</div>
							<!-- Contact Info -->
							<div class="contact">
								<div class="contact-icon"><span><i class="fa fa-envelope"></i></span></div>
								<div class="information"> <strong>Email Address:</strong>
									<p>{!! $settings->email !!}</p>
								</div>
							</div>
							<!-- Contact Info -->
							<div class="contact">
								<div class="contact-icon"> <span><i class="fa fa-phone"></i></span></div>
								<div class="information"> <strong>Phone No:</strong>
									<p>{!! $settings->mobile !!}</p>
								</div>
							</div>
							<!-- Contact Info -->
						</div>
						<!-- Contact Now -->
					</div>
				</div>
			</div>
			<!-- contact end -->

		</div>
	</div>
	<!--inner-content end-->
@stop
