@extends('layouts.master')
<?php
	$pageTitle = "Book An Appointment with AdvancedCareTherapy";
?>
@section('content')
	<!--banner-->
	<div class="banner clearfix">
		<img src="{!! asset('public/uploads/appointment-bnr.jpg') !!}" title="Advanced Care Physical Therapy"/>
	</div>
	<div class="page-top clearfix">
		<div class="container">
			<div class="row">
				<!--page main heading-->
				<div class="col-lg-9 col-md-8 col-sm-7 ">
					{{--<h1 class="entry-title hidden">Book An Appointment</h1>--}}
					<nav class="bread-crumb">
						<ul class="breadcrumb clearfix">
							<li><a href="{!! route('/') !!}">Home</a><span class="divider"></span></li>
							<li class="active">Appointment</li>
						</ul>
					</nav>
				</div>
				<div class="col-lg-3 col-md-4 col-sm-5 hidden">
					<!--search bar-->
					{{--<div id="search" class="widget clearfix">
						<form method="get" id="search-form" class="search-form" action="#">
							<div>
								<input type="text" value="" name="s" id="search-text" placeholder="Search"/>
								<input type="submit" id="search-submit" value=""/>
							</div>
						</form>
					</div>--}}
				</div>
			</div>
		</div>
	</div>

	<div class="appoint-page bg-white paddingtop0 paddingbottom10 clearfix">
		<div class="container">
			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
					<article class="page type-page status-publish hentry clearfix">
						<div class="entry-content marginbottom0 text-center">
							<h1>Book An Appointment</h1>
							<p class="for-border margintop20 marginbottom10"></p>
						</div>
					</article>

					<!--appointment form-->
					{{--<div class="appoint-section clearfix">
						<div class="top-icon"><img src="{!! asset('site/images/appoint-form-top.png') !!}" alt="Book An Appointment" /></div>

					</div>--}}

					<div class="appointment-form">
						<form id="appointment_form_main paddingtop10" style="background:none;padding-bottom: 10px;" action="{!! route('appointment-save') !!}" method="post">
							<div class="row">
								<div class="col-md-12">
									<div id="response-container"></div>
									<div id="error-container"></div>
								</div>

								<div class="col-md-6 col-sm-6 marginbottom20">
									<input type="text" name="name" id="app-name" class="required" placeholder="Name" data-msg-required="* Required" />
								</div>

								<div class="col-md-6 col-sm-6 marginbottom20">
									<input type="text" name="number" id="app-number" class="required number" minlength="9" maxlength="10" placeholder="Mobile Number" data-msg-required="* Required" />
								</div>
							</div>

							<div class="row">

								<div class="col-md-6 col-sm-6 marginbottom20">
									<input type="email" name="email" id="app-email" class="required email" placeholder="Email Address" data-msg-required="* Required" data-msg-email="* Enter valid email" />
								</div>

								<div class="col-md-6 col-sm-6 common marginbottom20">
									<?php $aTypes = ['' => '-Select Appointment Type-', 1 => 'General Consultation', 2 => 'Physical Therapy', 3 => 'Occupational Therapy']; ?>
									{!! Form::select('appointment_type', $aTypes, null, ['class' => 'form-control required', 'data-msg-required' => '* Required']) !!}
								</div>
							</div>

							<div class="row">
								<div class="col-md-6 col-sm-6 marginbottom20">
									<input type="text" name="date" id="datepicker" class="required" readonly="readonly" placeholder="Appointment Date" data-msg-required="* Required">
								</div>

								<div class="col-md-6 col-sm-6 marginbottom20">
									<?php $aSlot = ['' => '-Select Appointment Date First-'];?>
									{!! Form::select('appointment_slot', $aSlot, null, ['class' => 'form-control required appointment-slots', 'data-msg-required' => '* Required']) !!}
								</div>
							</div>

							<textarea name="message" id="app-message" class="required" cols="50" rows="1" placeholder="Message" data-msg-required="* Required"></textarea>

							<div class="col-md-12 clearfix">&nbsp;</div>
							<div class="row">
								<div class="col-md-12 text-center heavy font-16">
									Please do not submit any Protected Health Infomation (PHI). <br/><br/>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 text-center">
									<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
									<input type="submit" name="Submit" class="appointment-button form-submit-btn" value="Book Now"/>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-sm-1"></div>
			</div>
		</div>
	</div>
@stop
