@extends('layouts.master')
<?php
	// $pageTitle = $teamDetail->meta_title ?? "";
	// $metaDescription = $teamDetail->description ?? "";
	// $keywords = $teamDetail->keywords ?? "";
?>
@section('content')
<!--Inner Heading start-->
<?php $path = "public". \Config::get('constants.TEAM_UPLOAD'); ?>
{{--  style="background: url({{asset($path . $teamDetail['image-pic'])}})  no-repeat top;" --}}
<div class="inner-heading">
	<div class="container">
		<h3>{!! $teamDetail->name !!}</h3>
        <h4>{!! $teamDetail->designation !!}</h4>
	</div>
</div>

<!--inner-content start-->
<div class="inner-content">
	<div class="container">
        <table class="table table-bordered">
            <tr style="background: var(--themeColor)" class="text-white">
                <th>Professional Affiliations</th>
            </tr>
            <tr>
                <td>{!! $teamDetail->professional !!}</td>
            </tr>
            <tr style="background: var(--themeColor)" class="text-white">
                <th>Area of Practices</th>
            </tr>
            <tr>
                <td>{!! $teamDetail->area_of_practice !!}</td>
            </tr>
            <tr style="background: var(--themeColor)" class="text-white">
                <th>Other Details</th>
            </tr>
            <tr>
                <td>{!! $teamDetail->description !!}</td>
            </tr>
        </table>
	</div>
</div>
<!--inner-content end-->
@stop
