@extends('layouts.master')
@section('content')
<div role="main" class="main">

<section class="page-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb">
					<li><a href="{!! route('index') !!}">Home</a></li>
					<li class="active">Term & Conditions</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h1>Terms & Conditions</h1>
			</div>
		</div>
	</div>
</section>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<p>The following terms & conditions of sale apply to each contract of sale of goods by 'Karyana Bazaar'.</p>
			<p>All goods supplied remain the property of Karyana Bazar until paid in full.</p>
			<p>Deliveries will be made to customers as per their agreed delivery days.</p>
			<p>Delivery notes will be supplied at time of delivery. If a customer does not receive a delivery note, he or she must inform Freshways within 24 hours, otherwise delivery will be deemed to have been made.</p>
			<p>Delivery notes do not need to be signed by the customer in order for Freshways to prove that delivery has been made. Customers who require signed delivery notes, as proof of delivery, must request this in writing.</p>
			<p>
				&nbsp;
			</p>
		</div>
	</div>

</div>

</div>
@stop
