@extends('layouts.master')
<?php
$pageTitle = "";
$metaDescription = "";
$keywords = "";
?>

@push('style')
<style>
	.download-link:hover{
		cursor: pointer;
		color:blue;
		text-decoration: underline;
	}
</style>
@endpush
@section('content')
<!--Inner Heading start-->
<div class="inner-heading">
	<div class="container">
		<h3>Download Forms</h3>
	</div>
</div>
<!--Inner Heading end-->
<!--inner-content start-->
<div class="inner-content paddingtop8">
	<div class="container">
		<div class="contact-wrap">
			<div class="row">
				<?php $settings = webSetting(); ?>
				<div class="col-md-12 col-sm-12 column">
					<h1 class="text-center">Click to Download <span>Form </span></h1>
				</div>
				@php
				$path = "public".config('constants.FORM_UPLOAD');
				@endphp
				@foreach ($forms as $form)
				
				<div class="col-md-6 col-sm-12 link-box">
					<div>
						<i class="fa fa-file-pdf-o"></i>
						&nbsp;
						<span target="_blank" data-link="{{asset($path.$form->file)}}" data-title="{{$form->title}}" class="download-link" onclick="downloadForm(this)">
							{{$form->title}}
						</span>
					</div>
				</div>
				@endforeach
			</div>
		</div>  
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="#" id="my_captcha_form">
							<div class="g-recaptcha" data-sitekey="6LfrFKQUAAAAAMzFobDZ7ZWy982lDxeps8cd1I2i" >
							</div>
							<p>
								<a href="" id="download-form-link" download></a>
							</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Verify</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- contact end -->
		
	</div>
</div>
<!--inner-content end-->
@stop

@push('script')
<script src="https://www.google.com/recaptcha/api.js" async defer>
</script>
<script>
	function downloadForm(elem){
		var title = $(elem).data("title");
		var link = $(elem).data("link");
		$("#exampleModalLabel").text(title);
		$("#download-form-link").attr("href",link);
		$("#exampleModal").modal("show");
	}
	document.getElementById("my_captcha_form").addEventListener("submit",function(evt)
	{
		var response = grecaptcha.getResponse();
		if(response.length == 0) 
		{ 
			alert("Please verify you are humann!");		
		}else{		
			// $("#download-form-link").click();
			window.open($('#download-form-link').attr('href'));
			// $("#download-form-link").children("button").attr("disabled",false);
			$("#exampleModal").modal("hide");
		}
		grecaptcha.reset();
		evt.preventDefault();
		return false;
	});
</script>
@endpush