@extends('layouts.master')
<?php
	$pageTitle = "";
	$metaDescription = "";
	$keywords = "";
?>
@section('content')
	<!--Inner Heading start-->
	<div class="inner-heading">
		<div class="container">
			<h3>{!! $about->byline !!}</h3>
		</div>
	</div>
	<!--Inner Heading end-->

	<!--inner-content start-->
	<div class="inner-content innerbg">
		<div class="container">

			<!-- about-wrap start -->
			<div class="welcomeWrap">

				<div class="row">

					<div class="col-md-8">
						<div class="about text-justify">
							<h1>{!! $about->title !!}</h1>
							{!! $about->full_detail !!}
						</div>
					</div>
					<div class="col-md-4">
						<div class="welImg"><span><img src="{!! asset('public/site/images/welcome-img.jpg') !!}" alt=""></span></div>
						<br/>
						<?php $services = getServices(); ?>

						<div class="sidebar">
							<div class="widget">
								<h5 class="widget-title">Our Services </h5>
								<ul class="categories">
									@if(is_object($services))
										@foreach($services as $detail)
											<li>
												<a href="{!! route('service-detail', ['title' => $detail->url_slug]) !!}" title="{!! string_manip($detail->name, 'UCW') !!}">
													{!! string_manip($detail->name, 'UCW') !!}
												</a>
											</li>
										@endforeach
									@endif
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- about-wrap end -->
		</div>

		@include('layouts.partials.quote')

		</div>
	</div>
	<!--inner-content end-->
@stop
