@extends('layouts.master')
<?php
$pageTitle = "Update Profile";
$metaDescription = "";
$keywords = "";
?>
<style>
	.required span::after{
		content: " *";
		color: red;
	}
</style>
@section('content')
<!--Inner Heading start-->
<div class="inner-heading">
	<div class="container">
		<h3>Profile</h3>
	</div>
</div>
<!--Inner Heading end-->
<!--inner-content start-->
<div class="inner-content paddingtop8">
	<div class="container">
		<div class="contact-wrap">
			<div class="row">
				<?php $settings = webSetting(); ?>
				@php
				$userDetails = getLoggedInUser();
				@endphp
				<div class="col-md-12 col-sm-12 column">
					<h1 class="text-center">Update <span>Profile </span></h1>
					<div class="contact-form">
						<div class="col-md-12">
							@if(session()->has('success'))
							<div class="alert alert-success">
								{{ session()->get('success') }}
							</div>
							@endif
							@if ($errors->any())
							<div class="alert alert-danger">
								<ul>	
									@foreach ($errors->all() as $error)
									<li> {{$error}} </li>
									@endforeach
								</ul>
							</div>
							@endif
						</div>
						<div id="message"></div>
						<form method="post" action="{!! route('frontend.user.profile.update') !!}">
							<div class="row">
								<div class="col-md-6 required">
									<span style="display: block;text-align:left">Full Name</span>
									<input name="name" type="text" id="name" class="required"  value="{{$userDetails->name}}" title="Please provide your name" placeholder="Full Name">
								</div>
								<div class="col-md-6 required">
									<span style="display: block;text-align:left">Email</span>
									<input type="text" name="email" readonly placeholder="Email"  data-toggle="tooltip" data-placement="top" title="Email cannot be updated" value="{{$userDetails->email}}" >
								</div>	
							</div>
							<div class="row">
								<div class="col-md-6 margintop10 required">
									<span style="display: block;text-align:left">Contact</span>
									<input name="contact" type="text" id="contact" class="required"  value="{{$userDetails->mobile_number}}" title="Please provide your contact number" placeholder="Contact Number">
								</div>
								<div class="col-md-6 margintop10 required">
									<span style="display: block;text-align:left">{{$userDetails->id_type == "1" ? "Aadhar number" : "Passport number"}}</span>
									<input type="text" name="id_number"  title="Please provide your {{$userDetails->id_type == "1" ? "Aadhar Number" : "Passport number"}}"   class="required" placeholder="{{$userDetails->id_type == '1' ? 'Aadhar Number' : 'Passport number'}}" value="{{$userDetails->id_number}}" >
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 margintop10">
									<span style="display:block;text-align:left"> Address </span>
									<textarea name="address" id="" cols="20" rows="5">{{$userDetails->address}}</textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 margintop10">
									<span style="display:block;text-align:left"> City </span>
									<input type="text" name="city" placeholder="City Name" value="{{$userDetails->city}}"/>
								</div>
								<div class="col-md-6 margintop10">
									<span style="display:block;text-align:left"> State </span>
									<input type="text" name="state" placeholder="State Name" value="{{$userDetails->state}}"/>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6 margintop10">
									<span style="display:block;text-align:left"> County </span>
									<input type="text" name="country" placeholder="Country Name" value="{{$userDetails->country}}"/>
								</div>
								<div class="col-md-6 margintop10">
									<span style="display:block;text-align:left"> Zipcode </span>
									<input type="text" name="zipcode" placeholder="Zipcode" value="{{$userDetails->zipcode}}"/>
								</div>	
							</div>
							<div class="row">
								<div class="col-md-12">	
									<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
									<button class="margintop10 button readmore form-submit-btn" type="submit" id="submit">Update</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- contact end -->
		
	</div>
</div>
<!--inner-content end-->
@stop
