@extends('layouts.master')
<?php
$pageTitle = "";
$metaDescription = "";
$keywords = "";
?>
{!! NoCaptcha::renderJs() !!}

@section('content')
<!--Inner Heading start-->
<div class="inner-heading">
	<div class="container">
		<h3>Enquiry</h3>
	</div>
</div>
<!--Inner Heading end-->
<!--inner-content start-->
<div class="inner-content paddingtop8">
	<div class="container">
		<div class="contact-wrap">
			<div class="row">
				<?php $settings = webSetting(); ?>
				<div class="col-md-12 col-sm-12 column">.
					<h1 class="text-center">Send <span>Your Enquiry </span></h1>
					<div class="contact-form">
						<div class="col-md-12">
							<div id="response-container"></div>
							<div id="error-container"></div>
						</div>
						<div id="message"></div>
						<form method="post" action="{!! route('contact-send') !!}" id="contact_form">
							<div class="row">
								<div class="col-md-6">
									<input name="name" type="text" id="name" class="required"  title="* Please provide your name" placeholder="Full Name">
								</div>
								<div class="col-md-6">
									<input type="text" name="mobile" class="required number" data-msg-required="* Please provide a valid mobile number" minlength="10" maxlength="10" placeholder="Mobile Number">
								</div>
								<input type="hidden" name="success-message" value="Thanks for sending  your enquiry, we will get back to you soon.">
								<div class="col-md-12 margintop10">
									<input name="email" type="text" class="required email" title="* Please provide a valid email address" id="email" placeholder="Email">
								</div>
								<div class="col-md-12 margintop10">
									<textarea rows="4" name="message" class="required" id="message" placeholder="Enquiry"  title="*Please provide your enquiry"></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 margintop10">
									{!! NoCaptcha::display() !!}
								</div>
							</div>
							<div class="row">
								<input type="hidden" name="_token" value="{!! csrf_token() !!}" />
								<button class="button readmore form-submit-btn" type="submit" id="submit">Submit</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- contact end -->
		
	</div>
</div>
<!--inner-content end-->
@stop
