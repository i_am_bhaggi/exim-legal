@extends('layouts.master')
<?php
$settings = webSetting();
$pageTitle = $settings->meta_title;
$keywords = $settings->meta_keyword;
$metaDescription = $settings->meta_description;
?>
@section('content')
<!-- Revolution slider start -->
<div class="tp-banner-container">
    <div class="tp-banner">
        <ul>
            
            @if(is_object($banners))
            <?php $path = "public".\Config::get('constants.BANNER_UPLOAD'); ?>
            @foreach($banners as $detail)
            <li data-slotamount="7" data-transition="fade" data-masterspeed="1000" data-saveperformance="on"> <img alt="" src="{!! asset('public/site/images/dummy.png') !!}" data-lazyload="{!! asset($path . $detail->image) !!}">
                {{--<div class="caption lft large-title tp-resizeme slidertext2" data-x="left" data-y="120" data-speed="600" data-start="1600">Welcome To Our </div>--}}
                <div class="caption lfl large-title tp-resizeme slidertext3" data-x="left" data-y="160" data-speed="600" data-start="2200"><span>{!! $detail->text1 !!}</span></div>
                <div class="caption lfb large-title tp-resizeme slidertext6" data-x="left" data-y="240" data-speed="600" data-start="2800">
                    {!! nl2br($detail->text2) !!}
                </div>
                {{--<div class="caption lfl large-title tp-resizeme slidertext7" data-x="left" data-y="330" data-speed="600" data-start="3500"><a href="#">Contact Us <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>--}}
            </li>
            @endforeach
            @endif
            {{--<li data-slotamount="7" data-transition="fade" data-masterspeed="1000" data-saveperformance="on"> <img alt="" src="{!! asset('public/site/images/dummy.png') !!}" data-lazyload="{!! asset('public/site/images/banner.jpg') !!}">
                <div class="caption lft large-title tp-resizeme slidertext2" data-x="left" data-y="120" data-speed="600" data-start="1600">Welcome To Global </div>
                <div class="caption lfl large-title tp-resizeme slidertext3" data-x="left" data-y="160" data-speed="600" data-start="2200"><span>Business LawFirm</span></div>
                <div class="caption lfb large-title tp-resizeme slidertext6" data-x="left" data-y="240" data-speed="600" data-start="2800">Donec convallis laoreet orci, id ornare elit aliquet  a lectus mauris.<br/>
                    Suspendisse viverra, velit ullamcorper lobortis tincidunt, felis<br/>
                    porta interdum lorem enim imperdiet nisi.</div>
                    <div class="caption lfl large-title tp-resizeme slidertext7" data-x="left" data-y="330" data-speed="600" data-start="3500"><a href="#">Contact Us <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                </li>--}}
            </ul>
        </div>
    </div>
    <!-- Revolution slider end -->
    
    <!-- lawInfo start -->
    <div class="lawInfo">
        <div class="container">
            <ul class="info-service">
                <li class="col-md-4">
                    <div class="lawWrp">
                        <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                        <div class="heading"><a href="{!! route('free-consultation') !!}">Case Evaluation</a></div>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="lawWrp">
                        <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                        <div class="heading"><a href="{!! route('contact') !!}">Offices Location</a></div>
                    </div>
                </li>
                <li class="col-md-4">
                    <div class="lawWrp">
                        <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                        <div class="heading"><a href="{!! route('free-consultation') !!}">Fee Schedules</a></div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- lawInfo end -->
    
    <!-- Welcome start -->
    <div class="welcomeWrap">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="about">
                        <?php $about = getPage(1); ?>
                        <h1>About <span>Exim Legal</span></h1>
                        <p class="text-justify">{!! $about->short_intro !!}</p>
                        <div class="readmore"><a href="{!! route('about') !!}">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                    </div>
                </div>
            </div>
            <div class="welImg"><span><img src="{!! asset('public/site/images/welcome-img.jpg') !!}" alt=""></span></div>
        </div>
    </div>
    <!-- Welcome end -->
    
    <!-- Servise start -->
    <section class="service-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="headingTitle">
                        <div style="color:white">Our <span>Services</span></div>
                        <span>
                            <img src="{!! asset('public/site/images/headong-line.png') !!}" alt=""/>
                        </span>
                    </div>
                </div>
            </div>
            <ul class="row serv-area owl-carousel">
                <?php $services = getServices([], 6); ?>
                @if(is_object($services))
                @foreach($services as $detail)
                <li class="item">
                    <div class="service-block">
                        <div class="service-icon"><i class="fa fa-gavel" aria-hidden="true"></i></div>
                        <h4 style="height: 50px"><a href="{!! route('service-detail', ['title' => $detail->url_slug]) !!}" title="{!! string_manip($detail->name, 'UCW') !!}">
                            {!! string_manip($detail->name, 'UCW') !!}
                        </a>
                    </h4>
                    <p class="content text-justify">
                        {!! substr($detail->short_intro, 0, 200) !!}..
                    </p>
                    <div class="readmore"><a href="{!! route('service-detail', ['title' => $detail->url_slug]) !!}" title="{!! string_manip($detail->name, 'UCW') !!}">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                </div>
            </li>
            @endforeach
            @endif
            {{--<li class="item">
                <div class="service-block">
                    <div class="service-icon"><i class="fa fa-hourglass" aria-hidden="true"></i></div>
                    <h4><a href="#">Special Services</a></h4>
                    <p class="content">Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.</p>
                    <div class="readmore"><a href="aboutus.html">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                </div>
            </li>
            <li class="item">
                <div class="service-block">
                    <div class="service-icon"><i class="fa fa-gavel"></i></div>
                    <h4><a href="#">Discuss Strategy Builds</a></h4>
                    <p class="content">Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.</p>
                    <div class="readmore"><a href="aboutus.html">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                </div>
            </li>
            <li class="item">
                <div class="service-block">
                    <div class="service-icon"><i class="fa fa-university" aria-hidden="true"></i></div>
                    <h4><a href="#">Free Consultating</a></h4>
                    <p class="content">Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.</p>
                    <div class="readmore"><a href="aboutus.html">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                </div>
            </li>
            <li class="item">
                <div class="service-block">
                    <div class="service-icon"><i class="fa fa-hourglass" aria-hidden="true"></i></div>
                    <h4><a href="#">Special Services</a></h4>
                    <p class="content">Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.</p>
                    <div class="readmore"><a href="aboutus.html">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                </div>
            </li>
            <li class="item">
                <div class="service-block">
                    <div class="service-icon"><i class="fa fa-gavel"></i></div>
                    <h4><a href="#">Discuss Strategy Builds</a></h4>
                    <p class="content">Morbi semper, dui sodales aliquet imperdiet, lacus ligula congue neque, quis pretium lectus libero id.</p>
                    <div class="readmore"><a href="aboutus.html">Read More <i class="fa fa-arrow-right" aria-hidden="true"></i></a></div>
                </div>
            </li>--}}
        </ul>
    </div>
</section>
<!-- Servise end -->
@php
$testimonials = getTestimonials(0);
@endphp
@if (count($testimonials))
<section class="container-fluid">
    <div class="container">
        <!-- TESTIMONIALS -->
        <section class="testimonials">
            <div class="headingTitle">
                <div>
                    Our <span>Testimonials</span>
                    <hr class="title-underline"/>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="customers-testimonials" class="owl-carousel">
                            <!--TESTIMONIAL Start-->
                            @foreach ($testimonials as $testimonial)
                            <div class="item">
                                <div class="shadow-effect">
                                    <?php $path = "public".\Config::get('constants.UPLOADS'); ?>
                                    {!! HTML::image($path . $testimonial->image, '', ['height' => '100px', 'class' => 'img-circle']) !!}
                                    <p>{{substr($testimonial->short_intro,0,200)}}...</p>
                                </div>
                                <div class="testimonial-name">- {{$testimonial->name}}</div>
                            </div>
                            @endforeach
                            <!--END OF TESTIMONIAL -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END OF TESTIMONIALS -->
    </div>
</section>
@endif
<div class="inner-content paddingtop20">
    @include('layouts.partials.quote')
</div>

@stop