@extends('layouts.master')
<?php
	$pageTitle = $service->meta_title;
	$metaDescription = $service->description;
	$keywords = $service->keywords;
?>
@section('content')
<!--Inner Heading start-->
<div class="inner-heading">
	<div class="container">
		<h3>{!! $service->name !!}</h3>
	</div>
</div>

<!--inner-content start-->
<div class="inner-content">
	<div class="container">
		<?php
			$firstWord = $remName = "";
			$srName = explode(' ',  $service->name);
			if (isset($srName[0])) {
				$firstWord = $srName[0];
				unset($srName[0]);
			}

			if (count($srName) > 0) {
				$remName = implode(' ', $srName);
			}
		?>
		{{-- <h1> {!! $firstWord !!} <span>{!! $remName !!}</span> </h1> --}}
		<div class="headingTitle">
			<div style="color:black">{!! $firstWord !!} <span style="color: #cc8809">{!! $remName !!}</span></div>
			<span style="display: block;text-align: center;margin: 10px;">
				<img src="http://localhost/exim-legal/public/site/images/headong-line.png" alt="">
			</span>
		</div>
		<!-- blog start -->
		<div class="blogWraper blogdetail">
			<ul class="blogList">
				<li>
					<div class="line-height-30 text-justify">
						{!! $service->full_detail !!}
					</div>
				</li>
			</ul>
		</div>
		<!-- blog end -->

	</div>
</div>
<!--inner-content end-->
@stop
