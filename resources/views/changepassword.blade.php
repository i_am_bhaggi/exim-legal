@extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-default pull-right margintop10 _back" href="javascript:void(0)">
                <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
                <h1 class="page-header margintop10">{!! lang('common.change_password') !!}</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- end: PAGE HEADER -->
        <!-- start: PAGE CONTENT -->
        {{-- for message rendering --}}
        @include('layouts.messages')
        <div class="row">
            {{-- @if (session()->has('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
            @endif
            @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif --}}
            <div class="col-md-12 padding0">
                {!! Form::open(array('method' => 'POST', 'route' => array('user.updatePassword'), 'id' => 'changepassword-form', 'class' => 'form-horizontal')) !!}
                <div class="col-md-6">
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group">
                                {!! Form::label('old_password', lang('common.old_password'), array('class' => 'col-sm-3 control-label')) !!}
                                <div class="col-sm-8">
                                    {{-- {!! Form::password('old_password', null, ['class' => 'form-control password-box']) !!} --}}
                                    <input type="password" name="old_password" class="form-control password-box" required/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                {!! Form::label('new_password', lang('common.new_password'), array('class' => 'col-sm-3 control-label')) !!}
                                <div class="col-sm-8">
                                    {{-- {!! Form::password('new_password', null, ['class' => 'form-control password-box']) !!} --}}
                                    <input type="password" name="new_password" class="form-control password-box" required/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                {!! Form::label('confirm_password', lang('common.confirm_password'), array('class' => 'col-sm-3 control-label')) !!}
                                <div class="col-sm-8">
                                    {{-- {!! Form::password('confirm_password', null, ['class' => 'form-control password-box']) !!} --}}
                                    <input type="password" name="confirm_password" class="form-control password-box" required/>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-12 margintop10 clearfix text-center">
                            <div class="form-group">
                                <button class="btn" type="button" onclick="togglePassword(this)">
                                    <i class="fa fa-eye"></i>
                                </button>
                                {!! Form::submit('Update Password', array('class' => 'btn btn-primary')) !!}
                            </div>
                        </div>
                        
                    </div>
                    <!-- end: TEXT FIELDS PANEL -->
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @stop

    @push('script')
        <script>
            var isShown = 0;
            var passwordBoxes = $(".password-box");
            function togglePassword(elem){                
                if(isShown == 0){
                    passwordBoxes.attr("type","text");
                    $(elem).children('i').removeClass('fa-eye').addClass('fa-eye-slash');
                }else{
                    passwordBoxes.attr("type","password");
                    $(elem).children('i').removeClass('fa-eye-slash').addClass('fa-eye');
                }
                isShown = !isShown;
            }
        </script>
    @endpush