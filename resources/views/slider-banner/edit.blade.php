@extends('layouts.admin')

@section('content')
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10">
                {!! lang('common.edit_heading', lang('slider_banner.slider_banner')) !!}
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    {{-- for message rendering --}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-12 padding0">
        {!! Form::model($result, array('route' => array('slider-banner.update', $result->id), 'method' => 'PATCH', 'id' => 'ajaxSave', 'class' => 'form-horizontal')) !!}

            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('slider_banner.slider_banner_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('slider_banner', lang('slider_banner.image'), array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-8">
                                <div class="showImage">
                                    <?php $path = \Config::get('constants.BANNER_UPLOAD'); ?>
                                    <img src="{!! asset($path . $result->image) !!}" alt="{!! $path . $result->image !!}" class="img-responsive thumbnail">
                                </div>
                                {!! Form::label('image-pic', lang('common.choose_image'), array('class' => 'col-sm-9 padding0 control-label', 'id' => 'img-label')) !!}
                                {!! Form::file('image-pic', null) !!}
                                {!! Form::hidden('old_image', $result->image, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('status', lang('common.active') . '&nbsp;', array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-1 col-xs-3 margintop5">
                                {!! Form::checkbox('status', '1', ($result->status == '1') ? true : false) !!}
                            </div>
                        </div>

                        <div class="col-sm-12 margintop10 clearfix text-center">
                            <div class="form-group margin0">
                                {!! Form::submit(lang('common.update'), array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('slider_banner.additional_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('text1', lang('slider_banner.text1'), array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::textarea('text1', null, array('class' => 'form-control', 'size' => '3x5')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', lang('slider_banner.text2'), array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::textarea('text2', null, array('class' => 'form-control', 'size' => '3x5')) !!}
                            </div>
                        </div>
                    </div>
                    <!-- end: TEXT FIELDS PANEL -->
                </div>
            </div>
            {!! Form::close() !!}
        </div>    
    </div>
</div>
<!-- /#page-wrapper -->
@stop