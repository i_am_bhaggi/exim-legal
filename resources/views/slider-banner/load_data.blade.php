<thead>
<tr>
    <th width="5%" class="text-center">{!! lang('common.id') !!}</th>
    <th width="25%">{!! lang('slider_banner.image') !!}</th>
    <th>{!! lang('slider_banner.text1') !!}</th>
    <th>{!! lang('slider_banner.text2') !!}</th>
    <th width="10%" class="text-center"> {!! lang('common.status') !!} </th>
    <th class="text-center">{!! lang('common.action') !!}</th>
</tr>
</thead>
<tbody>
<?php $index = 1; ?>
@foreach($data as $detail)
    <tr id="order_{{ $detail->id }}">
        <td class="text-center">{!! pageIndex($index++, $page, $perPage) !!}</td>
        <td>
            <?php $path = "public". \Config::get('constants.BANNER_UPLOAD'); ?>
            {!! HTML::image($path . $detail->image, '', ['height' => '100px']) !!}
        </td>
        <td>
            {!! $detail->text1 !!}
        </td>
        <td>
            {!! $detail->text2 !!}
        </td>
        <td class="text-center">
            <a href="javascript:void(0);" class="toggle-status" data-message="{!! lang('messages.change_status') !!}" data-route="{!! route('slider-banner.toggle', $detail->id) !!}">
                {!! HTML::image('public/assets/images/' . $detail->status . '.gif') !!}
            </a>
            {{--&nbsp;
            <a href="javascript:void(0);" class="toggle-status" data-message="{!! lang('messages.change_status') !!}" data-width="16px" data-route="{!! route('brand.featured', $detail->id) !!}">
                {!! HTML::image('assets/images/home' . (int)$detail->is_featured . '.png', '', ['width' => '16px']) !!}
            </a>--}}
        </td>
        <td class="text-center col-md-1">
            <a class="btn btn-xs btn-primary" href="{{ route('slider-banner.edit', [$detail->id]) }}"><i class="fa fa-edit"></i></a>
        </td>
    </tr>
@endforeach
@if (count($data) < 1)
    <tr>
        <td class="text-center" colspan="7"> {!! lang('messages.no_data_found') !!} </td>
    </tr>
@else
    <tr class="margintop10">
        <td colspan="7">
            {!! paginationControls($page, $total, $perPage) !!}
        </td>
    </tr>
@endif
</tbody>