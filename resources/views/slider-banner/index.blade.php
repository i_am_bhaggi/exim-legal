@extends('layouts.admin')

@section('content')
<div id="page-wrapper">

	<div class="row">
		<div class="col-lg-12">
			<a class="btn btn-sm btn-primary pull-right margintop10" href="{{ route('slider-banner.create') }}"> <i class="fa fa-plus fa-fw"></i> {!! lang('common.create_heading', lang('slider_banner.slider_banner')) !!} </a>
			<h1 class="page-header margintop10">
				{!! lang('slider_banner.slider_banners') !!}
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>

	{{-- for message rendering --}}
    @include('layouts.messages')

    <div class="row">
    	<div class="col-md-12">
		<!-- start: BASIC TABLE PANEL -->
		<div class="panel panel-primary boot-panel" style="position: static;">
			<div class="panel-heading">
				<i class="fa fa-external-link-square"></i> &nbsp;
				{!! lang('slider_banner.slider_banners_list') !!}
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<div class="col-md-3 col-xs-4 text-right pull-right padding0 marginbottom10">
						{!! lang('common.per_page') !!}: {!! Form::select('name', ['20' => '20', '40' => '40', '100' => '100', '200' => '200', '300' => '300'], '20', ['id' => 'per-page']) !!}
					</div>
					<div class="col-md-3 col-xs-8 padding0 marginbottom10">
						{!! Form::hidden('page', 'search') !!}
						{!! Form::hidden('_token', csrf_token()) !!}
						{!! Form::text('name', null, array('class' => 'form-control live-search', 'placeholder' => 'Search slider banner')) !!}
					</div>
					<table id="paginate-load" data-route="{{ route('slider-banner.paginate') }}" data-sorting="{{ route('slider-banner.sort') }}" class="table table-hover clearfix margin0 col-md-12 padding0">
					</table>
				</div>
			</div>
		</div>
		<!-- end: BASIC TABLE PANEL -->
		</div>
	</div>	
</div>
<!-- /#page-wrapper -->
@stop
