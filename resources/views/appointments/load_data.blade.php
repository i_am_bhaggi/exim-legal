<thead>
<tr>
    <th width="5%" class="text-center">{!! lang('common.id') !!}</th>
    <th>{!! lang('appointments.number') !!}</th>
    <th>{!! lang('appointments.name') !!}</th>
    <th width="15%">{!! lang('appointments.appointment_type') !!}</th>
    <th width="18%">{!! lang('appointments.appointment_date') !!}</th>
    <th>{!! lang('appointments.message') !!}</th>
    <th class="text-center"> {!! lang('common.status') !!} </th>
    <th class="text-center hidden">{!! lang('common.action') !!}</th>
</tr>
</thead>
<tbody>
<?php $index = 1; ?>
@foreach($data as $detail)
    <tr id="order_{{ $detail->id }}">
        <td class="text-center">{!! pageIndex($index++, $page, $perPage) !!}</td>
        <td>
            {!! lang('appointments.apt_no') . $detail->id !!}
        </td>
        <td>
            <b>{!! $detail->name !!}</b> <br/>
            {!! $detail->email !!} <br/>
            {!! $detail->mobile !!} <br/>
        </td>
        <td>
            {!! ($detail->appointment_type == 1) ? 'General Consultation' : 'Physical Therapy' !!}
        </td>
        <td>
            Dt: {!! convertToLocal($detail->appointment_utc_date, 'd.m.Y') !!}<br/>
            Time: {!! $detail->appointment_time_slot !!}
        </td>
        <td>
            {!! $detail->message !!}
        </td>
        <td class="text-center">
            <a href="javascript:void(0);" class="toggle-status" data-message="{!! lang('messages.change_status') !!}" data-route="{!! route('appointment.toggle', $detail->id) !!}">
                {!! HTML::image('assets/images/' . (int)!$detail->status . '.gif') !!}
            </a>
        </td>
        <td class="text-center col-md-1 hidden">
            <a class="btn btn-xs btn-primary" href="{{ route('doctors.edit', [$detail->id]) }}"><i class="fa fa-edit"></i></a>
        </td>
    </tr>
@endforeach
@if (count($data) < 1)
    <tr>
        <td class="text-center" colspan="8"> {!! lang('messages.no_data_found') !!} </td>
    </tr>
@else
    <tr class="margintop10">
        <td colspan="8">
            {!! paginationControls($page, $total, $perPage) !!}
        </td>
    </tr>
@endif
</tbody>