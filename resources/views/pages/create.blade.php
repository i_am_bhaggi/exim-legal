@extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row topheading-row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 marginbottom10  _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10">{!! lang('common.create_heading', lang('pages.page')) !!}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->

    {{--for message rendering--}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-12">
        {!! Form::open(array('method' => 'POST', 'route' => array('pages.store'),  'id' => 'ajaxSave', 'class' => 'form-horizontal')) !!}
        <div class="col-md-6 paddingleft0">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    Page Detail
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-sm-11 marginleft20">
                            {!! Form::text('byline', null, array('class' => 'form-control', 'placeholder' => 'Page Name', 'id' => 'byline')) !!}
                        </div>

                        <div class="col-sm-11 margintop20 marginleft20">
                            <div>
                                {!! Form::textarea('title', null, array('class' => 'form-control', 'placeholder' => 'Page Title', 'id' => 'title', 'size' => '10x3')) !!}
                            </div>
                            <span class="help-block"><i class="fa fa-info-circle"></i> Maximum 255 characters </span>
                        </div>

                        <div class="col-sm-11 marginleft20">
                            {!! Form::textarea('short_intro', null, array('class' => 'form-control', 'placeholder' => 'Enter short intro', 'size' => '10x3')) !!}
                        </div>

                        <div class="col-sm-11 marginleft20"> <br/>
                            {!! Form::label('status', 'Status', array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-1 margintop10">
                                {!! Form::checkbox('status', '1', true) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 margintop20 text-center">
                        <div class="form-group">
                            {!! Form::submit('Save', array('class' => 'btn btn-primary btn-lg')) !!}
                        </div>
                    </div>

                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>

        <div class="col-md-6 paddingright0">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    SEO Meta Details
                </div>
                <div class="panel-body">
                    <div class="row marginleft10">

                        <div class="form-group">
                            <div class="col-sm-11">
                                {!! Form::textarea('meta_title', null, array('class' => 'form-control','placeholder' => 'Page Title',  'size' => '10x2', 'id' => 'meta_title')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-11">
                                {!! Form::textarea('meta_keywords', null, array('class' => 'form-control','placeholder' => 'Meta Keywords',  'size' => '10x2', 'id' => 'meta_keyword')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-11">
                                {!! Form::textarea('meta_description', null, array('class' => 'form-control','placeholder' => 'Meta Description',  'size' => '10x3', 'id' => 'meta_description')) !!}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end: TEXT FIELDS PANEL -->
        </div>

        <div class="col-md-12 paddingleft0 paddingright0">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    Full Page Detail
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                            {!! Form::textarea('full_detail', null, array('class' => 'ckeditor form-control', 'size' => '10x5')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 hidden">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i>
                    Embedd Video
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        {!! Form::label('youtube_video', 'Youtube Video', array('class' => 'col-sm-2 control-label')) !!}
                        <div class="col-sm-8">
                            {!! Form::textarea('youtube_video', null, array('class' => 'form-control', 'size' => '10x2')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 text-center">
            <div class="form-group">
                {!!  Form::submit('Save', array('class' => 'btn btn-primary btn-lg'))  !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    </div>
</div>
@stop