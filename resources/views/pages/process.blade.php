@extends('layouts.admin')

@section('content')
    <!-- start: PAGE -->
    <div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            &nbsp;<!-- end: PAGE TITLE & BREADCRUMB -->
        </div>
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    <div class="col-md-12">
        <div class="alert alert-block alert-success fade in">
            <button data-dismiss="alert" class="close" type="button">
                &times;
            </button>
            <h4 class="alert-heading"><i class="fa fa-check-circle"></i> Success!</h4>
            <p>
                Page Created successfully. <br /><br />
            </p>
            <p>
                <a href="{{ route('pages.create') }}" class="btn btn-success">
                    Create More Page
                </a>
                <a href="{{ route('pages.edit', ['id' => $id]) }}" class="btn btn-primary">
                    Edit Page Detail
                </a>
                <a href="{{ route('pages.index') }}" class="btn btn-default">
                    List Pages
                </a>
            </p>
        </div>
    </div>
@stop