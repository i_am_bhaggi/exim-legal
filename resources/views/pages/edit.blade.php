    @extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row topheading-row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 marginbottom10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10"> {!! lang('common.edit_heading', lang('pages.page')) !!} #{!! $result->title !!}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    
    {{--for message rendering--}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding0">
            {!! Form::model($result, array('route' => array('pages.update', $result->id), 'method' => 'PATCH',  'id' => 'ajaxSave', 'class' => 'form-horizontal')) !!}

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Page Detail
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-sm-11 marginleft20">
                                {!! Form::text('byline', null, array('class' => 'form-control', 'placeholder' => 'Page Name', 'id' => 'byline')) !!}
                            </div>

                            <div class="col-sm-11 margintop20 marginleft20">
                                <div>
                                    {!! Form::textarea('title', null, array('class' => 'form-control', 'placeholder' => 'Page Title', 'id' => 'title', 'size' => '10x3')) !!}
                                </div>
                                <span class="help-block"><i class="fa fa-info-circle"></i> Maximum 255 characters </span>
                            </div>

                            <div class="col-sm-11 marginleft20">
                                {!! Form::textarea('short_intro', null, array('class' => 'form-control', 'placeholder' => 'Enter short intro', 'size' => '10x3')) !!}
                            </div>

                            <div class="col-sm-11 marginleft20"> <br/>
                                {!! Form::label('status', 'Status', array('class' => 'col-sm-2 control-label')) !!}
                                <div class="col-sm-1 margintop10">
                                    {!! Form::checkbox('status', '1', ($result->status == 1) ? true : false) !!}
                                </div>
                            </div>

                        </div>

                        <div class="col-sm-12 margintop20 text-center">
                            <div class="form-group">
                                {!! Form::submit('Update', array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>

            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        SEO Meta Details
                    </div>
                    <div class="panel-body">
                        <div class="row marginleft10">

                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::textarea('meta_title', null, array('class' => 'form-control','placeholder' => 'Page Title',  'size' => '10x2', 'id' => 'meta_title')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::textarea('meta_keywords', null, array('class' => 'form-control','placeholder' => 'Meta Keywords',  'size' => '10x2', 'id' => 'meta_keyword')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-11">
                                    {!! Form::textarea('meta_description', null, array('class' => 'form-control','placeholder' => 'Meta Description',  'size' => '10x3', 'id' => 'meta_description')) !!}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>

            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Full Page Detail
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('full_detail', $result->full_detail, array('class' => 'ckeditor form-control', 'size' => '10x5')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 hidden">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i>
                        Embedd Video
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('youtube_video', 'Youtube Video', array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::textarea('youtube_video', null, array('class' => 'form-control', 'size' => '10x2')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 text-center">
                <div class="form-group">
                    {!! Form::submit('Update', array('class' => 'btn btn-primary btn-lg')) !!}
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="col-sm-12 col-md-12">

        <?php
        $path = Config::get('constants.UPLOADS');
        $folderPath = ROOT . $path[1];

        $folder =  $folderPath . '/';
        $files = glob($folder . $result->id . "__*");
        ?>
        <!-- start: DROPZONE PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">

                    <?php if( count($files) > 0) { ?>
                        {{--<a class="btn btn-xs btn-primary pull-right"  href="{!! route('admin.news-article.zip-download', [$detail->fn_id]) !!}"><i class="fa-times fa fa-cloud-download"></i> Download Images</a>--}}
                    <?php } ?>

                    <i class="fa fa-external-link-square"></i>
                    Upload Page Images By Drag & Drop
                </div>
                <div class="panel-body">
                    <?php

                        if (count($files) > 0 ) {
                            $iteration = 0;
                            foreach ($files as $imagePath) {

                            $files = explode('/', $imagePath);

                            $total = count($files);
                            $file = $files[$total - 1];

                            $thumb = $folder . 'thumbs/' . $file;
                            $main = $folder . $file;

                            $startPosition = strpos($thumb, $path[1]);
                            $fileLocation = substr($thumb, $startPosition);

                            $startPositionMain = strpos($main, $path[1]);
                            $fileLocationMain = substr($main, $startPosition);

                            if ( file_exists($imagePath) && file_exists($thumb) ) {
                                if ( $iteration++ == 6 ) {
                                //segregate thumbs after 6 by col-md-12
                    ?>

                    <div class="col-md-12">    </div>

                    <?php } ?>

                    <div class="col-md-2">
                        <div class="thumbnail" style="position:reletive;">
                            {!! HTML::image($fileLocation) !!}
                            <span class="btn btn-xs btn-primary copy-link text-center margintop0" style="position:absolute; right:30px; top:0;" title="Copied" data-clipboard-text="{!! asset($path[1] . '/'.$file) !!}">Copy</span>
                            <span class="text-center margintop0" style="position:absolute; right:6px; top:0;">
                                <a data-original-title="Delete" data-placement="right" class="btn btn-xs btn-danger tooltips __drop"  href="javascript:void(0);" data-route="{!! route('pages.picture-delete', [$result->id, base64_encode($file)]) !!}" data-message="Are you sure? want to delete this image."><i class="fa-times fa fa-white"></i></a>
                            </span>
                        </div>
                    </div>
                    <?php } } } ?>

                    {!! Form::open(array('route' => array('pages.upload', 'id' => $result->id), 'id' => 'my-awesome-dropzone', 'class' => 'dropzone col-md-12')) !!}

                    {!! Form::close() !!}
                </div>
            </div>
            <!-- end: DROPZONE PANEL -->
        </div>
    </div>
</div>
<!-- /#page-wrapper -->
<script>
    var clipboard = new Clipboard('.copy-link');
    clipboard.on('success', function(e) {
        $(e.trigger).attr('title', 'Copied').tooltip('fixTitle').tooltip('show');
        $(e.trigger).hover(function(){
            $(this).tooltip("hide");
        });
    });
</script>
@stop