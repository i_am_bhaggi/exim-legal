<thead>
<tr>
    <th width="5%" class="text-center">{!! lang('common.id') !!}</th>
    <th width="30%">
        {!! lang('common.name') !!}
    </th>
     <th class="text-center"> {!! lang('common.status') !!} </th>
     <th class="text-center">
         {!! lang('common.action') !!}
     </th>
</tr>
</thead>
<tbody>
<?php $index = 1; ?>
@foreach($data as $detail)
    <tr id="order_{!! $detail->id !!}">
        <td class="text-center">{!! pageIndex($index++, $page, $perPage) !!}</td>
        <td>
         <a title="{!! lang('common.edit') !!}" href="{{ route('pages.edit', [$detail->id]) }}">
            {!! $detail->byline !!}
         </a>
        </td>
        <td class="text-center">
            <a title="{!! lang('common.status') !!}" href="javascript:void(0);" class="toggle-status" data-message="{!! lang('messages.change_status') !!}" data-route="{!! route('pages.toggle', $detail->id) !!}">
                {!! HTML::image('public/assets/images/' . $detail->status . '.gif') !!}
            </a>
        </td>
        <td class="text-center col-md-1">
            <a title="{!! lang('common.edit') !!}" class="btn btn-xs btn-primary" href="{!! route('pages.edit', [$detail->id]) !!}"><i class="fa fa-edit"></i></a>
            {{--<a title="{!! lang('common.delete') !!}" class="btn btn-xs btn-danger __drop" data-route="{!! route('pages.drop', [$detail->id]) !!}" data-message="{!! lang('messages.sure_delete', string_manip(lang('pages.pages'))) !!}" href="javascript:void(0)"><i class="fa fa-times"></i></a>--}}
        </td>
    </tr>
@endforeach
@if (count($data) < 1)
    <tr>
        <td class="text-center" colspan="7"> {!! lang('messages.no_data_found') !!} </td>
    </tr>
@else
    <tr class="margintop10">
        <td colspan="7">
            {!! paginationControls($page, $total, $perPage) !!}
        </td>
    </tr>
@endif
</tbody>