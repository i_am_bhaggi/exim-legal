<thead>
<tr>
    <th width="5%" class="text-center">{!! lang('common.id') !!}</th>
    <th width="25%">{!! lang('download_form.title') !!}</th>
    <th>{!! lang('download_form.file') !!}</th>
    <th width="10%" class="text-center"> {!! lang('common.status') !!} </th>
    <th class="text-center">{!! lang('common.action') !!}</th>
</tr>
</thead>
<tbody>
<?php $index = 1; ?>
@foreach($data as $detail)
    <tr id="order_{{ $detail->id }}">
        <td class="text-center">{!! pageIndex($index++, $page, $perPage) !!}</td>
        <td>
            {!! $detail->title !!}
        </td>
        <td>
            <?php $path = \Config::get('constants.FORM_UPLOAD'); ?>            
            <a href="{{asset($path . $detail->file)}}" target="_blank" download="">
                <i class="fa fa-file-pdf-o fa-fw">&nbsp;Download</i>
            </a>
        </td>
        <td class="text-center">
            <a onclick="changeStatus(this)" data-message="{!! lang('messages.change_status') !!}" data-route="{!! route('download-form.toggle', $detail->id) !!}">
                @if ($detail->status)
                    <button class="btn btn-success">Active</button>
                @else
                    <button class="btn btn-danger">Inactive</button>
                @endif
            </a>
        </td>
        <td class="text-center col-md-1">
            <a class="btn btn-xs btn-primary" href="{{ route('download-form.edit', [$detail->id]) }}"><i class="fa fa-edit"></i></a>
        </td>
    </tr>
@endforeach
@if (count($data) < 1)
    <tr>
        <td class="text-center" colspan="7"> {!! lang('messages.no_data_found') !!} </td>
    </tr>
@else
    <tr class="margintop10">
        <td colspan="7">
            {!! paginationControls($page, $total, $perPage) !!}
        </td>
    </tr>
@endif
</tbody>