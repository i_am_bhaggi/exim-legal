@extends('layouts.admin')

@section('content')
<div id="page-wrapper">

    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10">
                {!! lang('common.create_heading', lang('download_form.download_form')) !!}
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    {{-- for message rendering --}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-12 padding0">
            {!! Form::open(array('method' => 'POST', 'route' => array('download-form.store'), 'id' => 'ajaxSave', 'files' => true, 'class' => 'form-horizontal')) !!}
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('download_form.download_form_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('title', lang('download_form.title'), array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-8">
                                {!! Form::textarea('title', null, array('class' => 'form-control', 'size' => '3x5')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('slider_banner', lang('download_form.file'), array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-10">
                                {!! Form::file('pdf-file', null) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('status', lang('common.active') . '&nbsp;', array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-1 col-xs-3 margintop5">
                                 {!! Form::checkbox('status', '1', true) !!}
                            </div>
                        </div>

                        <div class="col-sm-12 margintop10 clearfix text-center">
                            <div class="form-group margin0">
                                {!! Form::submit(lang('common.save'), array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            {!! Form::close() !!}
        </div>    
    </div>
</div>
<!-- /#page-wrapper -->
@stop