@extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10">{!! lang('common.create_heading', lang('our-team.our_team')) !!}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    
    {{-- for message rendering --}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-12 padding0">
            {!! Form::open(array('method' => 'POST', 'route' => array('our-team.store'), 'id' => 'ajaxSave', 'files' => true, 'class' => 'form-horizontal')) !!}
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('our-team.our_team_details') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('name', lang('our-team.name'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name', null, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('name','Designation', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('designation', null, array('class' => 'form-control')) !!}
                            </div>

                        </div>

                        
                        <div class="form-group">
                            {!! Form::label('slider_banner','Image', array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-10">
                                <div class="showImage">
                                    <img src="" alt="" width="150" class="img-responsive thumbnail">
                                </div>
                                {!! Form::label('image-pic', lang('common.choose_image'), array('class' => 'col-sm-7 padding0 control-label', 'id' => 'img-label')) !!}
                                {!! Form::file('image-pic', null) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            {!! Form::label('status', lang('common.active') . '&nbsp;', array('class' => 'col-sm-2 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                 {!! Form::checkbox('status', '1', true) !!}
                            </div>

                            {!! Form::label('frontpage', lang('our-team.frontpage') . '&nbsp;', array('class' => 'col-sm-2 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                {!! Form::checkbox('frontpage', '1', false) !!}
                            </div>

                            {!! Form::label('show_menu', lang('our-team.show_menu') . '&nbsp;', array('class' => 'col-sm-3 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                {!! Form::checkbox('show_menu', '1', false) !!}
                            </div>
                        </div>

                        {{-- <div class="col-sm-12 margintop20 clearfix text-center">
                            <div class="form-group">
                                {!! Form::submit(lang('common.save'), array('class' => 'btn btn-primary btn-lg')) !!}
                            </div>
                        </div> --}}

                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            {{-- <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('common.seo_detail') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('meta_title', lang('our-team.meta_title'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('meta_title', null, array('class' => 'form-control', 'size' => '5x4')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('keywords', lang('our-team.keywords'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('keywords', null, array('class' => 'form-control', 'size' => '5x4')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('description', lang('our-team.description'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::textarea('description', null, array('class' => 'form-control', 'size' => '5x4')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div> --}}
            <div class="col-md-12">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        Professional Affiliations
                    </div>
                    <div class="panel-body padding0">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('professional', null, array('class' => 'form-control ckeditor')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        Area of Practice
                    </div>
                    <div class="panel-body padding0">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('area_of_practice', null, array('class' => 'form-control ckeditor')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        Description
                    </div>
                    <div class="panel-body padding0">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('description', null, array('class' => 'form-control ckeditor')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 margintop10 clearfix text-center">
                    <div class="form-group">
                        {!! Form::submit(lang('common.save'), array('class' => 'btn btn-primary btn-lg')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>    
    </div>
</div>
<script> 
// wait for the DOM to be loaded
</script> 
<!-- /#page-wrapper -->
@stop