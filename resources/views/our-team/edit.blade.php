@extends('layouts.admin')
@section('content')
<div id="page-wrapper">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-lg-12">
            <a class="btn btn-sm btn-danger pull-right margintop10 _back" href="javascript:void(0)"> <i class="fa fa-arrow-left fa-fw"></i> {!! lang('common.back') !!} </a>
            <h1 class="page-header margintop10"> {!! lang('common.edit_heading', lang('important-link.important')) !!}: #{{ $result->name }}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- end: PAGE HEADER -->
    <!-- start: PAGE CONTENT -->
    
<<<<<<< HEAD
    {{-- for message rendering --}} 
=======
    {{-- for message rendering --}}
>>>>>>> 67e95f879de1273d4a99c6b5354122cc2f5adccf
    {{-- 
        
        --}}
    @include('layouts.messages')
    <div class="row">
        <div class="col-md-12 padding0">
            {!! Form::model($result, array('route' => array('our-team.update', $result->id), 'method' => 'PATCH', 'id' => 'ajaxSave', 'class' => 'form-horizontal')) !!}
            <div class="col-md-6">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        {!! lang('our-team.our_team_details') !!}
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            {!! Form::label('name', lang('our-team.name'), array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('name', $result->name, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('name','Designation', array('class' => 'col-sm-3 control-label')) !!}
                            <div class="col-sm-9">
                                {!! Form::text('designation', $result->designation, array('class' => 'form-control')) !!}
                            </div>

                        </div>
                        <div class="form-group">
                            {!! Form::label('slider_banner', lang('slider_banner.image'), array('class' => 'col-sm-2 control-label')) !!}
                            <div class="col-sm-8">
                                <div class="showImage">
                                    <?php $path = "public".\Config::get('constants.TEAM_UPLOAD'); ?>
                                    <img src="{!! asset($path . $result['image-pic']) !!}" alt="{!! $path . $result['image-pic'] !!}" class="img-responsive thumbnail">
                                </div>
                                {!! Form::label('image-pic', lang('common.choose_image'), array('class' => 'col-sm-9 padding0 control-label', 'id' => 'img-label')) !!}
                                {!! Form::file('image-pic', null) !!}
                                {!! Form::hidden('old_image', $result->image, array('class' => 'form-control')) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::label('status', lang('common.active') . '&nbsp;', array('class' => 'col-sm-2 padding0 paddingtop10 control-label')) !!}
                            <div class="col-sm-1 margintop8">
                                 {!! Form::checkbox('status', '1', ($result->status == '1') ? true : false) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end: TEXT FIELDS PANEL -->
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        Professional Affiliations
                    </div>
                    <div class="panel-body padding0">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('professional', $result->professional, array('class' => 'form-control ckeditor')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        Area of Practice
                    </div>
                    <div class="panel-body padding0">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('area_of_practice', $result->area_of_practice, array('class' => 'form-control ckeditor')) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-primary boot-panel">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> &nbsp;
                        Description
                    </div>
                    <div class="panel-body padding0">
                        <div class="form-group">
                            <div class="col-sm-12">
                                {!! Form::textarea('description', $result->description, array('class' => 'form-control ckeditor')) !!}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 margintop10 clearfix text-center">
                    <div class="form-group">
                        {!! Form::submit(lang('common.save'), array('class' => 'btn btn-primary btn-lg')) !!}
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>    
    </div>
</div>
<!-- /#page-wrapper -->
@stop