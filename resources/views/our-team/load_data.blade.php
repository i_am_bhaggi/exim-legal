<thead>
<tr>
    <th width="5%" class="text-center">{!! lang('common.id') !!}</th>
    <th>Name</th>
    <th>Designation</th>
    <th>Professional Affiliatation</th>
    <th>Area of practice</th>
    <th class="text-center">  </th>
    <th class="text-center">{!! lang('common.action') !!}</th>
</tr>
</thead>
<tbody>
<?php $index = 1; ?>
@foreach($data as $detail)
    <tr id="order_{{ $detail->id }}">
        <td class="text-center">{!! pageIndex($index++, $page, $perPage) !!}</td>
        <td> 
            <a href="{{ route('our-team.edit', [$detail->id]) }}">
                {!! $detail->name !!}
            </a>
        </td>
        <td>
            {!! $detail->designation !!}
        </td>
        <td>
            {!! $detail->professional !!}
        </td>
        <td>
            {!! $detail->area_of_practice !!}
        </td>
        <td class="text-center">
            <a onclick="changeStatus(this)" data-message="{!! lang('messages.change_status') !!}" data-route="{!! route('our-team.toggle', $detail->id) !!}">
                @if ($detail->status)
                    <button class="btn btn-success">Active</button>
                @else
                    <button class="btn btn-danger">Inactive</button>
                @endif
            </a>
        </td>
        <td class="text-center col-md-1">
            <a class="btn btn-xs btn-primary" href="{{ route('our-team.edit', [$detail->id]) }}"><i class="fa fa-edit"></i></a>
        </td>
    </tr>
@endforeach
@if (count($data) < 1)
    <tr>    
        <td class="text-center" colspan="6"> {!! lang('messages.no_data_found') !!} </td>
    </tr>
@else
    <tr class="margintop10">
        <td colspan="6">
            {!! paginationControls($page, $total, $perPage) !!}
        </td>
    </tr>
@endif
</tbody>