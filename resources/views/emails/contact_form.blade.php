<div style="font-family: Arial, sans-serif; width: 600px; margin: auto; background: #fff; border:1px solid #dddddd; margin-top: 10px; ">
    <!-- Header -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr align="center">
            <td style="padding: 6px 0px; font-size:22px;"> <img width="250" height="80" style="vertical-align: middle;" class="" src="{{ asset('logo.svg') }}" alt="Exim Legal Consultants" /></td>
        </tr>
    </table>
    <!-- Middle Section  -->
    <table border="0" cellpadding="0" cellspacing="0" width="100%">

        <tr>
            <td height="1" colspan="2" bgcolor="#f15b5a"></td>
        </tr>

        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: 13px; padding: 0px 16px; color: #333333;" align="left">
                <p>
                    @if($user)
                        Your contact query received, we will contact you soon.
                    @else
                        New contact query received, please check the mention below details.
                    @endif
                </p>

                <p>
                    <strong>Name:</strong> {!! $inputs['name'] !!}
                </p>
                <p>
                    <strong>Email:</strong> {!! $inputs['email'] !!}
                </p>
                <p>
                    <strong>Mobile:</strong> {!! $inputs['mobile'] !!}
                </p>

                <p>
                    <strong>Message:</strong> {!! nl2br($inputs['message']) !!}
                </p>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <!-- End Middle Section  -->
    <!-- Footer -->
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #fff; background: #777777; font-size: 13px;">
        <tr>
            <td align="center" height="40px">
                &copy; Copyright 2020. All Rights Reserved By <b>Exim Legal Consultants</b>
                <!-- End Footer -->
            </td>
        </tr>

    </table>
</div>