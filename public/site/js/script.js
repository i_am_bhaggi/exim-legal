(function ($) {
  "use strict";

  /* ================ Revolution Slider. ================ */
  if ($(".tp-banner").length > 0) {
    $(".tp-banner").show().revolution({
      delay: 6000,
      startheight: 550,
      startwidth: 1040,
      hideThumbs: 1000,
      navigationType: "none",
      touchenabled: "on",
      onHoverStop: "on",
      navOffsetHorizontal: 0,
      navOffsetVertical: 0,
      dottedOverlay: "none",
      fullWidth: "on",
    });
  }
  if ($(".tp-banner-full").length > 0) {
    $(".tp-banner-full").show().revolution({
      delay: 6000,
      hideThumbs: 1000,
      navigationType: "none",
      touchenabled: "on",
      onHoverStop: "on",
      navOffsetHorizontal: 0,
      navOffsetVertical: 0,
      dottedOverlay: "none",
      fullScreen: "on",
    });
  }

  /*testimonials*/
  $(document).ready(function () {
    $(".owl-carousel").owlCarousel({
      loop: true,
      margin: 10,
      nav: false,
      autoplay: true,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true,
        },
        700: {
          items: 1,
          nav: true,
        },
        1170: {
          items: 3,
          nav: true,
        },
      },
    });

    /**faq**/
    $(".collaps h4").each(function () {
      var tis = $(this),
        state = false;
      tis.on("click", function () {
        var answer = tis.next("p").hide().css("height", "auto").slideUp();
        $(".collaps p").hide().css("height", "auto").slideUp();
        $(".collaps h4").removeClass("active");
        answer.slideToggle(state);
        state = true;
        tis.toggleClass("active", state);
      });
    });
  });

  var submitButton = $(".form-submit-btn"),
    ajaxLoader = $(".ajax-loader"),
    messageContainer = $("#response-container"),
    errorContainer = $("#error-container");

  var formOptions = {
    beforeSubmit: function () {
      submitButton.attr("disabled", "disabled");
      ajaxLoader.fadeIn("fast");
      messageContainer.fadeOut("fast");
      errorContainer.fadeOut("fast");
      $(".backDrop").fadeIn(100, "linear");
      $(".loader").fadeIn(100, "linear");
    },
    success: function (ajax_response, statusText, xhr, $form) {
      var response = ajax_response; //$.parseJSON ( ajax_response );

      ajaxLoader.fadeOut("fast");
      submitButton.removeAttr("disabled");
      if (response.success) {
        $form.resetForm();
        messageContainer
          .html(
            "<div class='alert alert-success'>" + response.message + "</div>"
          )
          .fadeIn(200);
      } else {
        errorContainer
          .html(
            "<div class='alert alert-danger'>" + response.message + "</div>"
          )
          .fadeIn(200);
      }
      setTimeout(function () {
        $(".backDrop").fadeOut(100, "linear");
        $(".loader").fadeOut(100, "linear");
      }, 80);
    },
  };

  /* Form Handler */
  $("#contact_form").validate({
    //errorLabelContainer: errorContainer,
    submitHandler: function (form) {
      $(form).ajaxSubmit(formOptions);
    },
  });
})(jQuery);
