<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
})->name('/');*/

Route::get('/queue-work', function () {
    $exitCode = Artisan::call('queue:work');
    return redirect('/');
});

// Clear Cache Url
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:clear');
    return redirect('/');
});
// ---------------
// Storage Link Url
Route::get('/storage-link', function () {
    $exitCode = Artisan::call('storage:link');
    return redirect('/');
});

Route::get('/', array(
    'as' => '/',
    'uses' => 'Front\IndexController@index'
));

Route::get('about-us', array(
    'as' => 'about',
    'uses' => 'Front\IndexController@about'
));

/*Route::get('book-an-appointment', array('as' => 'appointment',
    'uses' => 'Front\IndexController@appointment'));

Route::get('appointment-verify/{dt?}', array('as' => 'appointment-verify',
    'uses' => 'Front\IndexController@appointmentValidate'));

Route::any('appointment-save', array('as' => 'appointment-save',
    'uses' => 'Front\IndexController@appointmentStore'));*/

Route::get('services', array(
    'as' => 'services',
    'uses' => 'Front\IndexController@services'
));

Route::get('service/{title?}', array(
    'as' => 'service-detail',
    'uses' => 'Front\IndexController@serviceDetail'
));

Route::get('fee-schedules', array(
    'as' => 'free-consultation',
    'uses' => 'Front\IndexController@freeConsultation'
));

Route::get('contact-us', array(
    'as' => 'contact',
    'uses' => 'Front\IndexController@contact'
));

Route::get('our-team', ['as' => 'our-team', 'uses' => 'Front\IndexController@ourTeam']);
Route::get('team-details/{id}', 'Front\IndexController@teamDetail')->name('team-detail');

Route::get('download-form', array(
    'as' => 'downloadform',
    'uses' => 'Front\IndexController@downloadform'
));


Route::get('enquiry', array(
    'as' => 'enquiry',
    'uses' => 'Front\IndexController@enquiry'
));

Route::get('testmonial', 'Front\IndexController@testimonial')->name('testimonial');


Route::any('contact-send', array(
    'as' => 'contact-send',
    'uses' => 'Front\IndexController@contactStore'
));

Route::any('consultation-send', array(
    'as' => 'consultation-send',
    'uses' => 'Front\IndexController@consultationStore'
));

Route::any('subscribe-store', array(
    'as' => 'subscribe-store',
    'uses' => 'Front\IndexController@subscribeStore'
));

Route::get('/login', 'Front\WebsiteUserController@showLogin')->name('frontend.user.login');
Route::get('/new-login', function () {
    return view('front.login-page');
})->name('frontend.user.new-login');
Route::post('/login', 'Front\WebsiteUserController@login')->name('frontend.user.dologin');
Route::get('/logout', function () {
    session()->forget(['user_id', 'user_name']);
    return redirect()->back();
})->name('frontend.user.logout');
Route::get('/profile', 'Front\WebsiteUserController@profile')->name('frontend.user.profile');
Route::post('/profile', 'Front\WebsiteUserController@updateProfile')->name('frontend.user.profile.update');
Route::get('/register', 'Front\WebsiteUserController@showRegiter')->name('frontend.user.register');
Route::post('/register', 'Front\WebsiteUserController@register')->name('frontend.user.doregister');
// Route::get('google-login', function () {
//     return Socialite::driver('google')->redirect();
// });
Route::get('google-login/callback', 'Front\WebsiteUserController@googleLogin');

/******************* FOR ADMIN *****************/
Route::group(['prefix' => 'eadmin'], function () {
    Route::get('/', 'Auth\LoginController@login')->name('admin.login');
    Route::get('login', 'Auth\LoginController@login')->name('login');
    Route::post('do-login', 'Auth\LoginController@doLogin')->name('admin.dologin');
});

Route::prefix('eadmin')->middleware(['auth', 'prevent-back'])->group(function () {
    Route::any('logout', 'Auth\LoginController@authLogout')->name('logout');
    Route::get('dashboard', 'DashboardController@index')->name('home');

    Route::any('web-setting', array(
        'as' => 'setting.web-setting',
        'uses' => 'SettingController@webSetting'
    ));

    // User Routes
    Route::resource(
        'user',
        'UserController',
        [
            'names' => [
                'index'     => 'user.index',
                'create'    => 'user.create',
                'store'     => 'user.store',
                'edit'      => 'user.edit',
                'update'    => 'user.update',
            ],
            'except' => ['show', 'destroy']
        ]
    );

    Route::get('change-password', 'UserController@changePassword')->name('user.changePassword');
    Route::post('update-password', 'UserController@updatePassword')->name('user.updatePassword');

    Route::any('user/paginate/{page?}', [
        'as' => 'user.paginate',
        'uses' => 'UserController@userPaginate'
    ]);
    Route::any('user/action', [
        'as' => 'user.action',
        'uses' => 'UserController@userAction'
    ]);
    Route::any('user/toggle/{id?}', [
        'as' => 'user.toggle',
        'uses' => 'UserController@userToggle'
    ]);
    Route::get('user/drop/{id?}', [
        'as' => 'user.drop',
        'uses' => 'UserController@drop'
    ]);

    // Services Routes
    Route::resource(
        'services',
        'ServicesController',
        [
            'names' => [
                'index'     => 'services.index',
                'create'    => 'services.create',
                'store'     => 'services.store',
                'edit'      => 'services.edit',
                'update'    => 'services.update'
            ],
            'except' => ['show', 'destroy']
        ]
    );

    Route::any('services/paginate/{page?}', [
        'as' => 'services.paginate',
        'uses' => 'ServicesController@servicePaginate'
    ]);

    Route::any('services/sort', [
        'as' => 'services.sort',
        'uses' => 'ServicesController@sortingService'
    ]);

    Route::any('services/toggle/{id?}', [
        'as' => 'services.toggle',
        'uses' => 'ServicesController@serviceToggle'
    ]);

    Route::any('services/featured/{id?}', [
        'as' => 'services.featured',
        'uses' => 'ServicesController@markedFeatured'
    ]);

    Route::get('services/drop/{id?}', [
        'as' => 'services.drop',
        'uses' => 'ServicesController@drop'
    ]);

    Route::any('services-upload-pictures/{id?}', [
        'as' => 'services.upload',
        'uses' => 'ServicesController@pictureUpload'
    ]);

    Route::any('services-drop-pictures/{id?}/{file?}', [
        'as' => 'services.picture-delete',
        'uses' => 'ServicesController@pictureDelete'
    ]);

    // Slider Banner Routes
    Route::resource(
        'slider-banner',
        'SliderBannerController',
        [
            'names' => [
                'index'     => 'slider-banner.index',
                'create'    => 'slider-banner.create',
                'store'     => 'slider-banner.store',
                'edit'      => 'slider-banner.edit',
                'update'    => 'slider-banner.update'
            ],
            'except' => ['show', 'destroy']
        ]
    );

    Route::any('slider-banner/paginate/{page?}', [
        'as' => 'slider-banner.paginate',
        'uses' => 'SliderBannerController@sliderBannerPaginate'
    ]);

    Route::any('slider-banner/toggle/{id?}', [
        'as' => 'slider-banner.toggle',
        'uses' => 'SliderBannerController@sliderBannerToggle'
    ]);

    Route::any('slider-banner/sort', [
        'as' => 'slider-banner.sort',
        'uses' => 'SliderBannerController@sortingSliderBanner'
    ]);

    Route::any('slider-banner/featured/{id?}', [
        'as' => 'slider-banner.featured',
        'uses' => 'SliderBannerController@markedFeatured'
    ]);

    Route::get('slider-banner/drop/{id?}', [
        'as' => 'slider-banner.drop',
        'uses' => 'SliderBannerController@drop'
    ]);

    // --------------
    // Download Form Routes
    Route::resource(
        'download-form',
        'DownloadFormController',
        [
            'names' => [
                'index'     => 'download-form.index',
                'create'    => 'download-form.create',
                'store'     => 'download-form.store',
                'edit'      => 'download-form.edit',
                'update'    => 'download-form.update'
            ],
            'except' => ['show', 'destroy']
        ]
    );

    Route::any('download-form/paginate/{page?}', [
        'as' => 'download-form.paginate',
        'uses' => 'DownloadFormController@downloadFormPaginate'
    ]);

    Route::any('download-form/toggle/{id?}', [
        'as' => 'download-form.toggle',
        'uses' => 'DownloadFormController@formToggle'
    ]);

    Route::any('download-form/sort', [
        'as' => 'download-form.sort',
        'uses' => 'DownloadFormController@sortingdownloadForm'
    ]);

    Route::any('download-form/featured/{id?}', [
        'as' => 'download-form.featured',
        'uses' => 'DownloadFormController@markedFeatured'
    ]);

    Route::get('slider-banner/drop/{id?}', [
        'as' => 'slider-banner.drop',
        'uses' => 'DownloadFormController@drop'
    ]);

    // ---------------------------------------------

    // Pages Routes
    Route::resource(
        'cms-pages',
        'PageController',
        [
            'names' => [
                'index'     => 'pages.index',
                'create'    => 'pages.create',
                'store'     => 'pages.store',
                'edit'      => 'pages.edit',
                'update'    => 'pages.update'
            ],
            'except' => ['show', 'destroy']
        ]
    );

    Route::any('cms-pages/paginate/{page?}', [
        'as' => 'pages.paginate',
        'uses' => 'PageController@pagePaginate'
    ]);

    Route::any('cms-pages/action', [
        'as' => 'pages.action',
        'uses' => 'PageController@pageAction'
    ]);

    Route::any('cms-pages/toggle/{id?}', [
        'as' => 'pages.toggle',
        'uses' => 'PageController@pageToggle'
    ]);

    Route::any('cms-pages/sorter', [
        'as' => 'pages.sorter',
        'uses' => 'PageController@sorter'
    ]);

    Route::get('cms-process-page/{id?}', array(
        'as' => 'pages.process',
        'uses' => 'PageController@processPage'
    ));

    Route::any('cms-page-upload-pictures/{id?}', array(
        'as' => 'pages.upload',
        'uses' => 'PageController@pictureUpload'
    ));

    Route::any('cms-page-drop-pictures/{id?}/{file?}', array(
        'as' => 'pages.picture-delete',
        'uses' => 'PageController@pictureDelete'
    ));

    // Important Link Routes
    // important-link Routes
    Route::resource(
        'important-link',
        'ImportantLinkController',
        [
            'names' => [
                'index'     => 'important-link.index',
                'create'    => 'important-link.create',
                'store'     => 'important-link.store',
                'edit'      => 'important-link.edit',
                'update'    => 'important-link.update'
            ],
            'except' => ['show', 'destroy']
        ]
    );

    Route::any('important-link/paginate/{page?}', [
        'as' => 'important-link.paginate',
        'uses' => 'ImportantLinkController@importantLinkPaginate'
    ]);

    Route::any('important-link/sort', [
        'as' => 'important-link.sort',
        'uses' => 'ImportantLinkController@sortingImportantLink'
    ]);

    Route::any('important-link/toggle/{id?}', [
        'as' => 'important-link.toggle',
        'uses' => 'ImportantLinkController@importantLinkToggle'
    ]);

    Route::any('important-link/featured/{id?}', [
        'as' => 'important-link.featured',
        'uses' => 'ImportantLinkController@markedFeatured'
    ]);

    Route::get('important-link/drop/{id?}', [
        'as' => 'important-link.drop',
        'uses' => 'ImportantLinkController@drop'
    ]);
    // -----------------------

    // Our Team
    Route::resource(
        'our-team',
        'OurTeamController',
        [
            'names' => [
                'index'     => 'our-team.index',
                'create'    => 'our-team.create',
                'store'     => 'our-team.store',
                'edit'      => 'our-team.edit',
                'update'    => 'our-team.update'
            ],
            'except' => ['show', 'destroy']
        ]
    );

    Route::any('our-team/paginate/{page?}', [
        'as' => 'our-team.paginate',
        'uses' => 'OurTeamController@ourTeamPaginate'
    ]);

    Route::any('our-team/sort', [
        'as' => 'our-team.sort',
        'uses' => 'OurTeamController@sortingOurTeam'
    ]);

    Route::any('our-team/toggle/{id?}', [
        'as' => 'our-team.toggle',
        'uses' => 'OurTeamController@ourTeamToggle'
    ]);

    Route::any('our-team/featured/{id?}', [
        'as' => 'our-team.featured',
        'uses' => 'OurTeamController@markedFeatured'
    ]);

    Route::get('our-team/drop/{id?}', [
        'as' => 'our-team.drop',
        'uses' => 'OurTeamController@drop'
    ]);
    // -----------------------

    // Testimonial Routes
    Route::resource(
        'testimonial',
        'TestimonialController',
        [
            'names' => [
                'index'     => 'testimonial.index',
                'create'    => 'testimonial.create',
                'store'     => 'testimonial.store',
                'edit'      => 'testimonial.edit',
                'update'    => 'testimonial.update'
            ],
            'except' => ['show', 'destroy']
        ]
    );

    Route::any('testimonial/paginate/{page?}', [
        'as' => 'testimonial.paginate',
        'uses' => 'TestimonialController@testimonialPaginate'
    ]);

    Route::any('testimonial/sort', [
        'as' => 'testimonial.sort',
        'uses' => 'TestimonialController@sortingTestimonial'
    ]);

    Route::any('testimonial/toggle/{id?}', [
        'as' => 'testimonial.toggle',
        'uses' => 'TestimonialController@testimonialToggle'
    ]);

    Route::any('testimonial/featured/{id?}', [
        'as' => 'testimonial.featured',
        'uses' => 'TestimonialController@markedFeatured'
    ]);

    Route::get('testimonial/drop/{id?}', [
        'as' => 'testimonial.drop',
        'uses' => 'TestimonialController@drop'
    ]);


    // Appointments Routes
    Route::any('appointments', [
        'as' => 'appointments.index',
        'uses' => 'AppointmentsController@index'
    ]);

    Route::any('appointments/paginate/{page?}', [
        'as' => 'appointments.paginate',
        'uses' => 'AppointmentsController@appointmentPaginate'
    ]);

    Route::any('appointment/toggle/{id?}', [
        'as' => 'appointment.toggle',
        'uses' => 'AppointmentsController@appointmentToggle'
    ]);

    Route::any('subscribers', [
        'as' => 'subscriber.index',
        'uses' => 'SubscriberController@index'
    ]);

    Route::any('subscribers/paginate/{page?}', [
        'as' => 'subscriber.paginate',
        'uses' => 'SubscriberController@subscriberPaginate'
    ]);

    Route::any('contact-us', [
        'as' => 'contact.index',
        'uses' => 'ContactController@index'
    ]);

    Route::any('contact-us/toggle/{id?}', [
        'as' => 'contact.toggle',
        'uses' => 'ContactController@contactToggle'
    ]);

    Route::any('contact-us/paginate/{page?}', [
        'as' => 'contact.paginate',
        'uses' => 'ContactController@contactPaginate'
    ]);
});
