-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2021 at 08:45 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beta`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment_master`
--

CREATE TABLE `appointment_master` (
  `id` int(5) UNSIGNED NOT NULL,
  `appointment_type` tinyint(4) NOT NULL,
  `appointment_date` date DEFAULT NULL,
  `appointment_utc_date` datetime DEFAULT NULL,
  `appointment_time_slot` varchar(30) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(25) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1: Replied, 0: Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `contact`, `message`, `status`, `created_at`, `updated_at`) VALUES
(6, 'BHAGIRATH GIRI', 'giribhagirath169@gmail.com', '8141855269', 'sdasdasdsa', 1, '2021-07-11 03:07:08', '2021-07-11 03:15:34'),
(7, 'BHAGIRATH GIRI', 'giribhagirath169@gmail.com', '8141855269', 'wqwq', 1, '2021-07-11 03:10:33', '2021-07-11 03:15:34'),
(8, 'BHAGIRATH GIRI', 'giribhagirath169@gmail.com', '8141855269', 'aSASDSA', 1, '2021-07-11 03:12:09', '2021-07-11 03:15:33'),
(9, 'BHAGIRATH GIRI', 'giribhagirath169@gmail.com', '8141855269', 'qweqweqwe', 1, '2021-07-11 03:15:17', '2021-08-31 00:27:05'),
(10, 'Bhagirath', 'testing@gmail.com', '8141855269', 'qwertyuiop[', 0, '2021-09-27 21:30:05', '2021-09-27 21:30:05'),
(11, 'qwert', 'bhagirth@gmail.com', '1234567332', '133eeq', 0, '2021-09-27 21:40:03', '2021-09-27 21:40:03'),
(12, 'qwert', 'bhagirth@gmail.com', '1234567332', 'qwertyuio', 0, '2021-09-27 21:43:38', '2021-09-27 21:43:38'),
(13, 'qwert', 'bhagirth@gmail.com', '1234567332', 'qeqwewqeqwe', 0, '2021-09-27 21:44:22', '2021-09-27 21:44:22'),
(14, 'qwert', 'bhagirth@gmail.com', '1234567332', '12345678', 0, '2021-09-27 21:47:56', '2021-09-27 21:47:56'),
(15, 'qwert', 'bhagirth@gmail.com', '1234567332', 'qwertyhfdsdsf', 0, '2021-09-27 21:48:07', '2021-09-27 21:48:07'),
(16, 'qwert', 'bhagirth@gmail.com', '1234567332', 'qwertyuio', 0, '2021-09-27 22:24:36', '2021-09-27 22:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `download_forms`
--

CREATE TABLE `download_forms` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `_order` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `download_forms`
--

INSERT INTO `download_forms` (`id`, `title`, `file`, `_order`, `status`, `created_at`, `updated_at`) VALUES
(7, 'Delhi-HC-Application-for-Inspection', '7__lgn6ha_delhi-hc-application-for-inspection.pdf', 1, 1, '2021-07-16 03:16:34', '2021-07-16 03:16:34'),
(8, 'board-resolution-sign-vakalatnama-format', '8__ngegsl_board-resolution-sign-vakalatnama-format.pdf', 1, 1, '2021-07-16 03:16:54', '2021-07-16 03:16:54'),
(13, 'Testing', '13__3492co_riya_bhardwaj_resume.pdf', 1, 1, '2021-09-07 08:24:07', '2021-09-07 08:24:07');

-- --------------------------------------------------------

--
-- Table structure for table `important_links`
--

CREATE TABLE `important_links` (
  `id` int(5) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` text DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `_order` int(11) DEFAULT 1,
  `frontpage` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `important_links`
--

INSERT INTO `important_links` (`id`, `title`, `link`, `status`, `_order`, `frontpage`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'Testing done', 'https://bhagirathgiri.com', 1, 2, 0, NULL, 1, '2021-08-31 04:36:16', '2021-08-31 00:16:24', NULL),
(6, 'Testing the data', 'http://bhagirathgiri.com', 1, 1, 0, 1, 1, '2021-08-30 23:17:49', '2021-08-31 00:14:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_failed_attempts`
--

CREATE TABLE `login_failed_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `our_teams`
--

CREATE TABLE `our_teams` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `designation` varchar(255) NOT NULL,
  `image-pic` varchar(200) NOT NULL,
  `professional` text DEFAULT NULL,
  `area_of_practice` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `_order` int(11) DEFAULT 1,
  `frontpage` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `our_teams`
--

INSERT INTO `our_teams` (`id`, `name`, `designation`, `image-pic`, `professional`, `area_of_practice`, `description`, `status`, `_order`, `frontpage`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(21, 'Bhagirath', 'Sr. Dev', '21__ozyvzd_bhagirath_giri.jpg', '<ul>\n	<li>B.A (Hon&rsquo;s) from B.N. Mandal&rsquo;s University</li>\n	<li>LL.B. from Campus Law Centre, University of Delhi</li>\n	<li>PGDBF (Post Graduate Diploma in Banking and Finance) from the University of Pondicherry.</li>\n</ul>', '<ul>\n	<li>Enrolled with Bar Council of Delhi, India.</li>\n	<li>Member of New Delhi Bar Association, New Delhi.</li>\n	<li>Member of Saket Bar Association, Saket, New Delhi.</li>\n	<li>Member of Delhi High Court Bar Association,[DHCBA], New Delhi.</li>\n	<li>Member of Supreme Court Bar Association [SCBA], INDIA.</li>\n</ul>', '<p>Mr Singh is the founder member and the Managing Partner of the firm, A. K. SINGH &amp; CO. (Law Offices). He has video expertise over matters related to Civil &amp; Commercial Disputes, Family and Matrimonial Disputes, Consumer Disputes and Recovery of Debts for Banking &amp; Financial Institutions and Alternate Dispute Resolution. He is known for his self developed and distinguished negotiation skills, extempore arguments. His experience covers a wide range of litigations, legal consultancy and arbitrations.</p>\n\n<p>He possesses superb drafting skills, well-acclaimed consummate and effective argumentation skills and unparallel client counselling skills. His career started with Intellect Law Partners (Advocates &amp; Solicitor) where he joined as an associate, after completing of his LL.B from Campus Law Center, Delhi University and also worked with C&amp;C Associates. Where he acquired basic litigation skills as well as comprehensive exposure of drafting and pleading of various Applications, Petitions, Writes, Suits, Deeds, Agreements, MOU etc. He worked extensively in Civil and Corporate Litigation including Civil Suits related to recovery, Injunction, Declaration, Specific Performance, Dispute related to Delhi Rent Control, Probate Petitions, Passing off and infringement of trademarks, consumer disputes, any disputes related to the recovery of debts due to banking and financial institutions, writs, revisions, review, appeals etc. And he practised law before the Delhi High Court, National Commission, State Commission, Consumer forums, DRT, DRAT, All-District Courts of Delhi and before a number of arbitrators.</p>', 1, 1, 0, 1, 1, '2021-09-12 12:27:07', '2021-09-12 13:13:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(8) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `byline` varchar(1000) DEFAULT NULL,
  `url_slug` varchar(255) NOT NULL,
  `short_intro` text DEFAULT NULL,
  `full_detail` text DEFAULT NULL,
  `publish_dt` datetime DEFAULT NULL,
  `p_order` int(3) DEFAULT 1,
  `image` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `hits` bigint(6) NOT NULL DEFAULT 0,
  `status` tinyint(4) NOT NULL DEFAULT 1 COMMENT '0 - Pending, 1 - Published, 2 - Accepted, 3 - Review, 4 - Rejected',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `byline`, `url_slug`, `short_intro`, `full_detail`, `publish_dt`, `p_order`, `image`, `meta_keywords`, `meta_title`, `meta_description`, `hits`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'About Exim Legal', 'About Us', 'about-us', 'We at Exim Legal, offer multi-level tailor made solutions in India to the commercial disputes between the parties. Our solutions include legal counseling, mediation, representing at various levels with the government authorities to resolve the disputes.', '<p>​We at Exim Legal, offer multi-level tailor made solutions in India to the commercial disputes between the parties. Our solutions include legal counseling, mediation, representing at various levels with the government authorities to resolve the disputes.&nbsp;&nbsp;</p>\n\n<p>We indulge in comprehensive investigation at the first stage and after due diligence and suggest the client the possible ways to resolve their disputes.</p>\n\n<p>Our team consist of experienced legal experts and retired government officers having the in-depth knowledge of the system and have comprehensive know how on negotiations, processes and possible solutions.</p>', '2020-08-06 04:00:00', 1, NULL, NULL, 'About Exim Legal', 'About Exim Legal', 0, 1, 1, 1, '2019-07-30 05:03:01', '2021-07-20 21:11:14', NULL),
(3, 'Area of practice', 'Area of Practice', 'area-of-practice', NULL, NULL, NULL, 1, NULL, NULL, 'Area of practice', NULL, 0, 1, 1, NULL, '2021-06-23 09:02:38', '2021-07-20 21:11:17', NULL),
(4, 'Contact Us', 'Contact ', 'contact-us', NULL, NULL, '2021-07-11 10:49:56', 1, NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, '2021-07-11 08:50:24', '2021-07-20 21:11:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_master`
--

CREATE TABLE `service_master` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `inner_text` varchar(255) DEFAULT NULL,
  `meta_title` text DEFAULT NULL,
  `keywords` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `url_slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `position` int(5) DEFAULT NULL,
  `short_intro` text DEFAULT NULL,
  `full_detail` text DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `_order` int(11) DEFAULT 1,
  `frontpage` tinyint(4) DEFAULT 0,
  `show_menu` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_master`
--

INSERT INTO `service_master` (`id`, `name`, `code`, `inner_text`, `meta_title`, `keywords`, `description`, `url_slug`, `image`, `position`, `short_intro`, `full_detail`, `status`, `_order`, `frontpage`, `show_menu`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted_at`) VALUES
(1, 'Corporate', NULL, 'Corporate Law', 'Corporate', 'Corporate', 'Corporate law is the body of laws, rights, rules, regulations, relations, and practices that govern the formation and operation of companies, organizations, and businesses.', 'corporate', NULL, NULL, 'Corporate law is the body of laws, rights, rules, regulations, relations, and practices that govern the formation and operation of companies, organizations, and businesses. It is the body of the law that regulates legal entities that exist to conduct corporations or the theory of corporations. The corporate contracts are based on the rights and obligations of all of the masses involved with forming, owning, controlling, and managing a corporation.', '<p>Corporate law is the body of laws, rights, rules, regulations, relations, and practices that govern the formation and operation of companies, organizations, and businesses. It is the body of the law that regulates legal entities that exist to conduct corporations or the theory of corporations. The corporate contracts are based on the rights and obligations of all of the masses involved with forming, owning, controlling, and managing a corporation. It is an agreement between two parties to avoid disputes in the future. Corporate contracts are meant to be friendly for business, company, and organization.</p>\n\n<p>Our proficient corporate lawyers advise clients of the appropriate clause to be used in a corporate contract as per their needs. Our team look deeply into every potential of the business and draft a contract accordingly so that the client knows better what to do, in case a dispute arises.</p>\n\n<p>Corporations are the most general, formal, and legal establishments all over the globe for diverse purposes, proper business governance is the most common. Though the corporate contract laws vary from nation to nation, the usual and ordinary attributes of a business corporation are limited liability, shareholder ownership, a separate legal personality, professional and centralized management structure.</p>\n\n<p>Exim Legal is one of the leading corporate contract firms in India. It is offering exquisite corporate legal services to almost all types of corporations for the benefit of clients. Our impeccable legal services are available everywhere in the whole world and anyone can take the services as per need. Owing to a complete range of diverse contract services and outstanding performances, Exim Legal is a full-blown legal organization successful and reputed in countries globally. Our national and international expertise enables us to hold up your business with any flotation, acquisition, and fundraising. We assist clients to understand the corporate contracts (for example Faregeek and Protocol) that best suit their needs. In case a dispute arises among parties, we are always ready to provide efficient solutions to resolve it.</p>\n\n<p>Exim Legal has been assisting people as well as corporations of varied sectors in tackling smoothly the burgeoning business competition, intellectual property violations, business disputes, highly violate business markets, environmental issues, industrial and public relations, customs, travel industry, business outsourcing, etc. for their solid sustainability, high efficiency, and profitability.</p>\n\n<p>CORPORATE LAW LEGAL SERVICESCorporate law legal services we provide are broadly classified into the following categories and practice fields:</p>\n\n<ul>\n	<li>Formation and Development of Corporations</li>\n	<li>Intellectual Property Law</li>\n	<li>Business Process Outsourcing</li>\n	<li>Legal Outsourcing</li>\n	<li>Immigration Law</li>\n	<li>International Business</li>\n	<li>Corporate Taxation</li>\n	<li>Environmental Law</li>\n</ul>', 0, 1, 1, 1, 1, '2020-08-07 12:10:08', 1, '2021-07-07 11:28:34', NULL),
(2, 'Business Agreements', NULL, NULL, 'Business Agreements', 'Business Agreements', 'Business Agreements', 'business-agreements', NULL, NULL, 'It is observed that In the international trade, be it export /import of services or of goods, writing the contracts and or enforcing implementation are very crucial. We at Exim Legal, with the expert team draft those contracts and also monitor the enforcement on day to day/ periodic basis', '<p>​It is observed that In the international trade, be it export /import of services or of goods, writing the contracts and or enforcing implementation are very crucial. We at Exim Legal, with the expert team draft those contracts and also monitor the enforcement on day to day/ periodic basis</p>', 0, 1, 1, 1, 1, '2020-09-06 11:14:37', 1, '2021-07-07 11:28:36', NULL),
(9, 'Trial', NULL, NULL, 'this content is just added for testing', 'this content is just added for testing', 'this content is just added for testing', 'trial', NULL, NULL, 'this content is just added for testing', NULL, 0, 1, 1, 1, 1, '2021-06-21 10:20:54', 1, '2021-07-07 11:30:11', NULL),
(3, 'Intellectual Property', NULL, NULL, 'Intellectual Property Law', 'Intellectual Property Law', 'Intellectual Property Law', 'intellectual-property', NULL, NULL, 'In today’s competitive market, Intellectual Property is an integral part of most of the businesses and professions. IPR law deals with the rules and regulations to secure and enforce legal rights to innovations, designs, and artistic works.', '<p>In today&rsquo;s competitive market, Intellectual Property is an integral part of most of the businesses and professions.IPR law deals with the rules and regulations to secure and enforce legal rights to innovations, designs, and artistic works.</p>\n\n<p>From startups to market leaders, we aid clients in securing their market position and enhance the value of their intellectual property. Just like the law protects ownership of personal or physical property, similarly, it protects the exclusive control of intangible assets. The aim of these laws is not only to provide an incentive for developing creative works, products, or assets that benefit society but also ensure that they can profit from their works without fright of misappropriation by others. These laws entitle the owners to avert others from using, dealing, or tempering with his/her asset without prior permission of the owner.</p>\n\n<p>If you need a legal advisor who can understand the business properly and advise accordingly, then we at Exim Legal identify, plan, prepare and prevent your intellectual property by giving you a competitive edge so that you will never face problems in the future.</p>\n\n<p>Exim Legal is a respected full-service law firm in India working with a wide range of clients from diverse sectors such as pharmaceuticals, customs, excise, and travel industry, etc. We assist and advise clients in the process of filing IP applications (patents, trademarks, designs, copyrights, and trade secrets, etc.), prosecuting, and defending their intellectual rights for their developments, both nationally and internationally. From patents, trademarks, copyright assets to trade secrets, our lawyers are well-versed with all the fundamentals of intellectual property law today.</p>\n\n<p>At Exim Legal, we specialize exclusively in all aspects of intellectual property for works or businesses of all types and sizes. We at Exim Legal offer multi-level tailored solutions to the international civil disputes between the parties. We indulge in the comprehensive investigation at the first stage and after due diligence and suggest the client the possible ways to resolve their disputes.</p>\n\n<p>&nbsp;</p>', 1, 1, 1, 1, 1, '2020-09-06 11:15:01', 1, '2020-09-25 22:09:44', NULL),
(4, 'Business Process Outsourcing', NULL, NULL, 'Business Process Outsourcing Law', 'Business Process Outsourcing Law', 'Business Process Outsourcing Law', 'business-process-outsourcing', NULL, NULL, 'An Outsourcing Agreement is a business contract between a company and a service provider in which the service provider promises to deliver necessary services such as data administration and data processing, using its own personnel and equipment, and often at its own facilities.', '<p>An Outsourcing Agreement is a business contract between a company and a service provider in which the service provider promises to deliver necessary services such as data administration and data processing, using its own personnel and equipment, and often at its own facilities.</p>\n\n<p>BPO refers to Business Process Outsourcing, which includes outsourcing services related to real-time accounting, human resources (HR), payroll, benefits, and finance functions and activities. KPO refers to Knowledge Process Outsourcing, which includes outsourcing services related to research, analysis, legal, paralegal, and other highly skilled activities. At Exim Legal, we offer employment agreement, BPO, and KPO agreement services to small scale as well as large scale businesses of all types for saving their time and money.</p>\n\n<p>We at Exim Legal, deliver a good Outsourcing Agreement including a comprehensive road map of the duties and obligations of both the parties - outsourcer and service provider. It helps to minimize the complications among them in case of a dispute. Our team finalizes an outsourcing agreement after discussing and negotiating the terms in front of both parties to avoid any misunderstanding at a later stage. We modify every outsourcing agreement, which is applicable under different circumstances as one brush should not paint all the painting. Thus, we draft a proper outsourcing agreement for businesses to save a lot of problems later on.</p>\n\n<p>From start-up to market leaders we assist clients in securing their market position and enhance their productivity and profitability through serving employment, BPO, and KPO agreement law services in India and abroad as well. Our team delivers services as per the need of the outsourcer after assessing the ability of the service provider to meet performance requirements in both qualitative and quantitative terms.</p>\n\n<p>Some of the areas we include in a good Outsourcing Agreement to protect your outsourcing business are:</p>\n\n<p>● A Detailed Description of Services<br />\n● Duties and Obligations of Outsourcer and Service Provider<br />\n● Security and Confidentiality<br />\n● Fees and Payment Terms<br />\n● No Employer-Employee Relation<br />\n● Details of staff appointed by a Service Provider<br />\n● Inspection and Acceptance<br />\n● Dispute Resolution<br />\n● Legal Compliance</p>', 1, 1, 1, 1, 1, '2020-09-06 11:15:29', 1, '2021-07-09 23:24:35', NULL),
(5, 'Litigation and arbitration law', NULL, NULL, 'Litigation and arbitration law', 'Litigation and arbitration law', 'Litigation and arbitration law', 'litigation-and-arbitration-law', NULL, NULL, 'Exim legal render specialized Litigation and Arbitration legal services in India as a multidisciplinary law firm. We offer various services in all domains including corporate disputes, regulatory disputes, tax disputes, commercial disputes, domestic and international arbitration, etc.', '<p>Exim legal render specialized Litigation and Arbitration legal services in India as a multidisciplinary law firm. We offer various services in all domains including corporate disputes, regulatory disputes, tax disputes, commercial disputes, domestic and international arbitration, etc. Our Litigation and Arbitration practice for dispute resolution is built on the strength of a team that is at the cutting edge of legal knowledge and research skills as well as has mastered the art of advocacy.</p>\n\n<p>Arbitration can be a valuable dispute resolving approach where privacy and confidentiality are of utmost importance. Arbitration is an alternative way of resolving a dispute in which both parties agree that in case of conflict they will submit the dispute to one or more arbitrators to make a binding decision on the dispute after studying and listening to the parties. When parties decide to arbitrate, they are going for a private dispute resolution for resolving controversies instead of going to court.</p>\n\n<p>Litigation is a legal process in which the parties resort to the court for settling the disputes among them. Litigation Lawyers at Exim Legal manage all phases of the litigation from the investigation, pleadings, and recognition by setting the pre-trial, trial, settlement, and appeal processes. Success in litigation can signify through means of multiple things such as securing a positive out of court settlement, defending business relationships, or fighting it out in the highest courts. In general, Legal disputes are stressful and expensive, hence it is crucial to have a well-versed team of lawyers who will serve you in a correct and practical strategy on your behalf, whether it is a matter related to business or personal.</p>\n\n<p>At Exim Legal, we assist our clients in ascertaining which dispute resolution process will best suit their requirements. We provide proper guidance regarding the appropriate clause to be used in a contract for ensuring that the client knows what to expect and do in the event of a controversy. Our clients not only admire our legal specialization but also our ability to deliver creative solutions as per the matter and our profound understanding of diverse cultures and jurisdictions in which our clients work.</p>\n\n<p>We at Exim Legal handle almost all types of legal disputes among the parties throughout the country and globe. We offer multi-level tailor-made solutions to the clients as each case is equally important to us.</p>\n\n<p>Some of the areas in which our Litigation lawyers deal:</p>\n\n<p>● Civil Litigation<br />\n● Corporate Litigation<br />\n● Criminal Litigation</p>', 0, 1, 1, 1, 1, '2020-09-06 11:15:46', 1, '2020-09-25 22:09:54', NULL),
(6, 'Legal Outsourcing', NULL, NULL, 'Legal Outsourcing', 'Legal Outsourcing', 'Legal Outsourcing', 'legal-outsourcing', NULL, NULL, 'Legal Outsourcing, also known as LPO (Legal Process Outsourcing), refers to the practice of obtaining legal support services by a law firm or corporation from an external law firm or legal support services company (LPO provider). LPO consists of various processes from investigation and analytics to drafting, Intellectual Property Rights (IPR), paralegal services, patents, and legal research.', '<p>Legal Outsourcing, also known as LPO (Legal Process Outsourcing), refers to the practice of obtaining legal support services by a law firm or corporation from an external law firm or legal support services company (LPO provider). LPO consists of various processes from investigation and analytics to drafting, Intellectual Property Rights (IPR), paralegal services, patents, and legal research.</p>\n\n<p>Exim Legal LPO service is a diverse, end-to-end solution provider for all kinds of legal process outsourcing requirements. There are a plethora of services that are being offered by LPO at Exim Legal in India as well as globally. Our legal Outsourcing services incorporate contract management, document review for due diligence, deposition summaries, legal research, litigation documents, patent renewals and analytics, and so on. When the LPO provider or outsourced entity is based in another country, the practice is sometimes known as Offshoring; we serve them too.</p>\n\n<p>We at Exim Legal, a leading legal Process Outsourcing company offer high-quality legal support services to law firms and corporate legal divisions nationally as well as internationally. We understand the varying requirement of clients, and our purpose is to delight you by an amalgamation of industry specialists and service quality. Rather than setting up an in-house legal department in an organization, outsourcing LPO services not only assist in saving the cost of hiring legal resources but also ensures accuracy in results without any inconvenience.</p>\n\n<p>Exim legal is the best choice for you if you are looking for expert legal process outsourcing services at affordable prices. We are proficient at meeting any LPO requirements of your legal firm or corporation by our extensive experience in providing legal drafting solutions, and also serve you with all the benefits of a legal firm. Avail cost-saving services by outsourcing your LPO requirements to Exim Legal team that includes:</p>\n\n<p>● Skilled Lawyers and Attorneys<br />\n● Specialist SMEs (Subject Matter Experts)<br />\n● Well-experienced Paralegals<br />\n● LPO Coders<br />\n● Legal Document Experts</p>\n\n<p>The most important characteristic that any firm or corporation find out in their legal Process Outsourcing partner is experience and reliability, and this is something that makes Exim Legal a key player in this domain.</p>', 0, 1, 1, 1, 1, '2020-09-06 11:15:54', 1, '2020-09-25 22:09:52', NULL),
(7, 'Asset Management in India', NULL, NULL, 'Assest Management in India', 'Assest Management in India', 'Assest Management in India', 'asset-management-in-india', NULL, NULL, 'PIO is an abbreviation for a Person of Indian Origin. PIO means a foreign citizen who is not the citizen of India, but at any time held an Indian passport, either of his/her parents, grandparents were born and permanently citizen in India in terms of the provisions contained in the constitution of India or Citizenship Act, 1955.', '<p>PIO is an abbreviation for a Person of Indian Origin. PIO means a foreign citizen who is not the citizen of India, but at any time held an Indian passport, either of his/her parents, grandparents were born and permanently citizen in India in terms of the provisions contained in the constitution of India or Citizenship Act, 1955.</p>\n\n<p>Generally, a PIO can invest in India in any kind of business in which an ordinary Indian can invest. Our highly trained and professional team is offering high-quality suggestions and superior risk-return investments. Our clients receive a comprehensive service. Our experienced team study the needs of our clients, create an actionable investment planning, implement the strategy in practice, and oversee its development through time.</p>\n\n<p>At Exim Legal, we advise and facilitate different options of investment that a PIO can invest in India. We manage clients&rsquo; investments and bestow them with the strategies and expertise that would allow them to achieve their goals and secure their financial future. We offer several services for businesses of all types in India in which a PIO can invest and enhance its future growth.</p>\n\n<p>We at Exim Legal handle disputes for international clients in India by using various methods such as litigation, arbitration, mediation, collaborative law, corporate law, etc. We deliver multi-level customized solutions in India to PIO disputes between parties. Owing to years of experience and profound knowledge, we suggest our clients&rsquo; possible ways to resolve their conflicts after investing and analyzing their matter correctly. The issues that matter to our clients are equally important to us. We assist our clients in ascertaining which dispute resolution process best suits their needs. Also, we provide proper guidance regarding the appropriate clause to be<br />\nused while investing in a project or business. Whatever we do, we always do by keeping in mind the interest of our clients.</p>\n\n<p>&nbsp;</p>', 1, 1, 1, 1, 1, '2020-09-06 11:16:20', 1, '2020-10-14 11:50:53', NULL),
(8, 'Handling disputes for international contracts in India', NULL, NULL, 'Handling disputes for international contracts in india', 'Handling disputes for international contracts in india', 'Handling disputes for international contracts in india', 'handling-disputes-for-international-contracts-in-india', NULL, NULL, 'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.', '<p>In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.</p>', 1, 1, 1, 1, 1, '2020-09-06 11:16:34', 1, '2021-07-07 13:15:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting_master`
--

CREATE TABLE `setting_master` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `site_logo` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `other_text` text DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` text DEFAULT NULL,
  `meta_description` text DEFAULT NULL,
  `all_rights_reserved` varchar(255) DEFAULT NULL,
  `facebook_url` varchar(255) DEFAULT NULL,
  `twitter_url` varchar(255) DEFAULT NULL,
  `googleplus_url` varchar(255) DEFAULT NULL,
  `instagram_url` varchar(255) DEFAULT NULL,
  `youtube_url` varchar(255) DEFAULT NULL,
  `address_map` text DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting_master`
--

INSERT INTO `setting_master` (`id`, `site_name`, `site_logo`, `address`, `other_text`, `mobile`, `phone`, `email`, `website`, `meta_title`, `meta_keyword`, `meta_description`, `all_rights_reserved`, `facebook_url`, `twitter_url`, `googleplus_url`, `instagram_url`, `youtube_url`, `address_map`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Exim Legal Consultants', NULL, '207, Chander Bhawan, 67-68, Nehru Place, Delhi', '{\"timings\":{\"monday\":\"10AM\\u20135PM\",\"tueday\":\"10AM\\u20135PM\",\"wednusday\":\"10AM\\u20135PM\",\"thrsday\":\"10AM\\u20135PM\",\"friday\":\"10AM\\u20135PM\",\"saturday\":\"10AM\\u20135PM\",\"sunday\":\"Closed\"}}', '+91-9810674894', NULL, 'info@eximlegal.com', 'http://eximlegal.com', 'Exim Legal Consultants, the Best legal service provider for NRI disputes.', 'Intellectual Property Law, Business Agreements, Corporate Law', 'We provide end to end service to our clients.', 'All Rights Reserved. <b>Exim Legal Consultants</b>', 'https://www.facebook.com', 'https://www.twitter.com', NULL, 'https://www.linkedin.com', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider_banner`
--

CREATE TABLE `slider_banner` (
  `id` int(11) NOT NULL,
  `text1` varchar(255) DEFAULT NULL,
  `text2` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `_order` int(11) NOT NULL DEFAULT 1,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `is_featured` tinyint(4) NOT NULL DEFAULT 0,
  `login_required` tinyint(4) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_banner`
--

INSERT INTO `slider_banner` (`id`, `text1`, `text2`, `image`, `_order`, `status`, `is_featured`, `login_required`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Corporate', 'Donec convallis laoreet orci, id ornare elit aliquet  a lectus mauris. \nDonec convallis laoreet orci, id ornare elit aliquet  a lectus mauris.', '2__wpzsyc_banner2.jpg', 1, 1, 0, 0, 1, 1, NULL, '2020-08-07 18:54:19', '2020-10-14 04:51:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriber_master`
--

CREATE TABLE `subscriber_master` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `subscribe_ip` varchar(20) NOT NULL,
  `subscribe_date` date NOT NULL,
  `unsubscribe_ip` varchar(20) DEFAULT NULL,
  `unsubscribe_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscriber_master`
--

INSERT INTO `subscriber_master` (`id`, `name`, `email`, `subscribe_ip`, `subscribe_date`, `unsubscribe_ip`, `unsubscribe_date`) VALUES
(1, NULL, 'giribhagirath169@gmail.com', '::1', '2021-07-09', NULL, NULL),
(2, NULL, 'info@bhagirathgiri.com', '::1', '2021-07-09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `testimonial_master`
--

CREATE TABLE `testimonial_master` (
  `id` int(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `url_slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `position` int(5) DEFAULT NULL,
  `keywords` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `short_intro` text DEFAULT NULL,
  `full_detail` text DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `_order` int(11) DEFAULT 1,
  `frontpage` tinyint(4) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `testimonial_master`
--

INSERT INTO `testimonial_master` (`id`, `name`, `code`, `url_slug`, `image`, `position`, `keywords`, `description`, `short_intro`, `full_detail`, `status`, `_order`, `frontpage`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Bhagirath', NULL, 'bhagirath', '2__pcth1i_testimonial3.jpg', NULL, NULL, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.&nbsp;</p>', 1, 1, 1, 1, 1, '2021-07-08 21:27:59', '2021-07-08 23:00:59', NULL),
(3, 'Anjali', NULL, 'anjali', '3__lykmc6_testimonial3.jpg', NULL, 'Testing the data', 'Testing the data', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.&nbsp;</p>', 1, 1, 1, 1, 1, '2021-07-08 22:30:34', '2021-07-08 23:00:54', NULL),
(4, 'Riya', NULL, 'riya', '4__g9sof6_testimonial3.jpg', NULL, NULL, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>', 1, 1, 1, 1, NULL, '2021-07-08 22:59:29', '2021-07-11 03:40:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `last_seen` datetime DEFAULT NULL,
  `forgot_otp` varchar(10) DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `login_attempts` tinyint(4) DEFAULT NULL,
  `id_type` enum('1','2') DEFAULT NULL COMMENT '1: Aadhar, 2: Passport',
  `id_number` text DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `username`, `password`, `mobile`, `remember_token`, `status`, `last_seen`, `forgot_otp`, `last_login`, `login_attempts`, `id_type`, `id_number`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Eximlegal', 'info@eximlegal.com', 'emadmin', '$2y$10$enpmTDtOGCZrcmYHyji6UuNV9//eRF0sA6TtphKq7MMDN5Yp4pA6q', NULL, 'JH96zR45zJMcjFOWpcjknh7hGThVD5QBpwkhBMyjwrAlKbP24gwRbJD9tjgy', 1, NULL, '', '2020-01-06 07:49:07', 0, NULL, NULL, 1, 1, NULL, '2021-07-22 05:49:59', '2021-07-20 23:09:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `web_users`
--

CREATE TABLE `web_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `user_social_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_reg_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_type` enum('1','2','','') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1:Aadhar, 2:Passport',
  `id_number` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1:Active, 0:Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `web_users`
--

INSERT INTO `web_users` (`id`, `name`, `email`, `mobile_number`, `address`, `city`, `state`, `country`, `zip_code`, `password`, `email_verified_at`, `user_social_id`, `user_reg_type`, `remember_token`, `id_type`, `id_number`, `user_status`, `created_at`, `updated_at`) VALUES
(593, 'Bhagirath Giri', 'giribhagirath169@gmail.com', '8141855269', NULL, NULL, NULL, NULL, NULL, '202cb962ac59075b964b07152d234b70', NULL, NULL, NULL, NULL, '1', '12345678789', 1, '2021-08-31 00:24:31', '2021-08-31 00:24:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment_master`
--
ALTER TABLE `appointment_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `download_forms`
--
ALTER TABLE `download_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `important_links`
--
ALTER TABLE `important_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_failed_attempts`
--
ALTER TABLE `login_failed_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `our_teams`
--
ALTER TABLE `our_teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_master`
--
ALTER TABLE `service_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_master`
--
ALTER TABLE `setting_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_banner`
--
ALTER TABLE `slider_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriber_master`
--
ALTER TABLE `subscriber_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonial_master`
--
ALTER TABLE `testimonial_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `web_users`
--
ALTER TABLE `web_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment_master`
--
ALTER TABLE `appointment_master`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `download_forms`
--
ALTER TABLE `download_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `important_links`
--
ALTER TABLE `important_links`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `login_failed_attempts`
--
ALTER TABLE `login_failed_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `our_teams`
--
ALTER TABLE `our_teams`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `service_master`
--
ALTER TABLE `service_master`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `setting_master`
--
ALTER TABLE `setting_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slider_banner`
--
ALTER TABLE `slider_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `subscriber_master`
--
ALTER TABLE `subscriber_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `testimonial_master`
--
ALTER TABLE `testimonial_master`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `web_users`
--
ALTER TABLE `web_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=595;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
